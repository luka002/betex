package com.luka.goldenbettingtips;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoldenBettingTipsApplication /*implements CommandLineRunner*/ {

	public static void main(String[] args) {
		SpringApplication.run(GoldenBettingTipsApplication.class, args);
	}

}
