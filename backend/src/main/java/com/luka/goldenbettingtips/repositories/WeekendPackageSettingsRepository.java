package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.WeekendPackageSettings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeekendPackageSettingsRepository extends JpaRepository<WeekendPackageSettings, Long> {
}
