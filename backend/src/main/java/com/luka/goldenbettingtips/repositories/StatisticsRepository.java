package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatisticsRepository extends JpaRepository<Statistics, Long> {
}
