package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.VisitorCounter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface VisitorCounterRepository extends JpaRepository<VisitorCounter, LocalDate> {

    List<VisitorCounter> findAllByOrderByDateDesc();

    Page<VisitorCounter> findAllByOrderByDateDesc(Pageable page);

}
