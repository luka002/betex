package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.ResetPasswordToken;
import com.luka.goldenbettingtips.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResetPasswordTokenRepository extends JpaRepository<ResetPasswordToken, Long> {

    ResetPasswordToken findByUser(User user);

    ResetPasswordToken findByToken(String token);

    void deleteByToken(String token);
}
