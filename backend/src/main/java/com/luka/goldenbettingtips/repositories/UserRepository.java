package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.models.entities.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findByEmail(String email);

    Page<User> findByUsernameContainingIgnoreCaseOrSocialNameContainingIgnoreCase(
            String username, String socialName, Pageable page
    );

    Long countByUsernameContainingIgnoreCaseOrSocialNameContainingIgnoreCase(
            String username, String socialName
    );

    List<User> findByUserRole(UserRole userRole);

}
