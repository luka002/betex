package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.Tip;
import com.luka.goldenbettingtips.models.entities.TipState;
import com.luka.goldenbettingtips.models.entities.TipType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TipRepository extends JpaRepository<Tip, Long> {

    @Query("SELECT t FROM Tip t " +
            "WHERE t.tipType = ?1 AND (t.tipState = ?2 OR t.tipState = ?3) " +
            "ORDER BY t.time ASC")
    List<Tip> findByTypeAndStatesOrderByTimeAsc(TipType type, TipState state1, TipState state2);

    List<Tip> findByTipTypeAndTipStateOrderByTimeDesc(TipType tipType, TipState state);

    List<Tip> findByTipTypeAndTipStateAndTimeAfterAndTimeBeforeOrderByTimeDesc(
            TipType tipType, TipState state, LocalDateTime after, LocalDateTime before
    );

    List<Tip> findByTipTypeAndTipStateAndTimeAfterOrderByTimeDesc(
            TipType tipType, TipState state, LocalDateTime after
    );
}
