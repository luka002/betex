package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.TicketUser;
import com.luka.goldenbettingtips.models.entities.TicketUserKey;
import com.luka.goldenbettingtips.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TicketUserRepository extends JpaRepository<TicketUser, TicketUserKey> {

    List<TicketUser> findByUser(User user);

    @Query("SELECT u.user FROM TicketUser u WHERE u.ticket.id = ?1")
    List<User> findUsersByTicketId(String id);

    void deleteByUser(User user);

}
