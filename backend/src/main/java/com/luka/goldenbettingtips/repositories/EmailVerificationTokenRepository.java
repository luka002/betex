package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.EmailVerificationToken;
import com.luka.goldenbettingtips.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailVerificationTokenRepository extends JpaRepository<EmailVerificationToken, Long> {

    EmailVerificationToken findByToken(String token);

    void deleteByToken(String token);

}
