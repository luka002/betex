package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.TipType;
import com.luka.goldenbettingtips.models.enums.Type;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipTypeRepository extends JpaRepository<TipType, Long> {

    TipType findByType(Type type);

}
