package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.TipState;
import com.luka.goldenbettingtips.models.enums.State;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipStateRepository extends JpaRepository<TipState, Long> {

    TipState findByState(State state);

}
