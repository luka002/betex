package com.luka.goldenbettingtips.repositories;

import com.luka.goldenbettingtips.models.entities.UserRole;
import com.luka.goldenbettingtips.models.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    UserRole findByRole(Role role);

}
