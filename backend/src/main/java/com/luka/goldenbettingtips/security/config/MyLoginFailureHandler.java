package com.luka.goldenbettingtips.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class MyLoginFailureHandler implements AuthenticationFailureHandler {

    private final String WRONG_CREDENTIALS_ERROR = "Invalid username and/or password.";
    private final String USER_NOT_VERIFIED = "You have not yet verified your email address " +
            "<strong>%s</strong>. If you do not see email please check your <strong>spam" +
            " folder</strong> or contact us on our email address betextips@gmail.com.";
    private final String USERNAME = "username";

    private final UserDetailsServiceImpl userDetailsService;
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public MyLoginFailureHandler(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {
        String errorMessage = WRONG_CREDENTIALS_ERROR;

        String username = request.getParameter(USERNAME);
        User user = userDetailsService.findByUsername(username);

        if (userIsRegisteredButNotEnabled(user, exception)) {
            errorMessage = String.format(USER_NOT_VERIFIED, user.getEmail());
        }

        Map<String, String> data = new HashMap<>();
        data.put("message", errorMessage);

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.getOutputStream()
                .println(mapper.writeValueAsString(data));
    }

    private boolean userIsRegisteredButNotEnabled(User user, AuthenticationException exception) {
        return
            user != null &&
            exception instanceof DisabledException &&
            !user.isEnabled();
    }

}
