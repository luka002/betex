package com.luka.goldenbettingtips.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class SessionInvalidStrategy implements InvalidSessionStrategy {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void onInvalidSessionDetected(HttpServletRequest httpServletRequest,
                                         HttpServletResponse httpServletResponse)
                                            throws IOException, ServletException {
        if (httpServletRequest.getSession().getAttribute("Already redirected") != null) {
            return;
        }
        httpServletRequest.getSession().setAttribute("Already redirected", "true");

        Map<String, String> data = new HashMap<>();
        data.put("message", "Session expired");

        httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        httpServletResponse.getOutputStream()
                .println(mapper.writeValueAsString(data));
    }

}
