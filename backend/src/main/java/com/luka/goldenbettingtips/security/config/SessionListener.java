package com.luka.goldenbettingtips.security.config;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@Component
public class SessionListener implements HttpSessionListener {

    public SessionListener() {
        super();
    }

    public void sessionCreated(final HttpSessionEvent event) {
        event.getSession().setMaxInactiveInterval(60*60*12);
    }

    public void sessionDestroyed(final HttpSessionEvent event) {
    }
}
