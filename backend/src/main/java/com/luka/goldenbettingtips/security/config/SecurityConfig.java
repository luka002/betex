package com.luka.goldenbettingtips.security.config;

import com.luka.goldenbettingtips.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.session.ConcurrentSessionFilter;
import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Configuration
@EnableWebSecurity
@EnableScheduling
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final String[] PUBLIC_MATCHERS = {
            "/login**",
            "/api/user/register",
            "/api/user/getCurrentUser",
            "/api/user/forgotPassword",
            "/api/user/resetPassword",
            "/api/user/activateUserAccount",
            "/api/user/isUsernameUnique",
            "/api/user/isEmailUnique",
            "/api/tip/activeFreeTips",
            "/api/tip/historyFreeTips",
            "/api/tip/historyPremiumTips",
            "/api/reCaptcha**",
            "/api/statistics/visit",
            "/api/settings",
            "/api/social/auth/google/signup",
            "/api/social/auth/google/signin",
            "/api/social/auth/facebook/signup",
            "/api/social/auth/facebook/signin",
    };

    private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    private final UserDetailsServiceImpl userDetailsServiceImpl;
    private final SessionInvalidStrategy sessionInvalidStrategy;
    private final MyLogoutSuccessHandler myLogoutSuccessHandler;
    private final MyLoginFailureHandler myLoginFailureHandler;
    private final MyLoginSuccessHandler mySuccessHandler;
    private final PasswordEncoder passwordEncoder;

    public SecurityConfig(RestAuthenticationEntryPoint restAuthenticationEntryPoint,
                          UserDetailsServiceImpl userDetailsServiceImpl,
                          SessionInvalidStrategy sessionInvalidStrategy,
                          MyLogoutSuccessHandler myLogoutSuccessHandler,
                          MyLoginFailureHandler myLoginFailureHandler,
                          MyLoginSuccessHandler mySuccessHandler,
                          PasswordEncoder passwordEncoder) {
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
        this.userDetailsServiceImpl = userDetailsServiceImpl;
        this.sessionInvalidStrategy = sessionInvalidStrategy;
        this.myLogoutSuccessHandler = myLogoutSuccessHandler;
        this.myLoginFailureHandler = myLoginFailureHandler;
        this.mySuccessHandler = mySuccessHandler;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .cors()
                .and()
            .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint)
                    .and()
            .authorizeRequests()
                .antMatchers(PUBLIC_MATCHERS).permitAll()
                .anyRequest().authenticated()
                    .and()
            .formLogin()
                .successHandler(mySuccessHandler)
                .failureHandler(myLoginFailureHandler)
                    .and()
            .logout()
                .logoutSuccessHandler(myLogoutSuccessHandler)
                    .and()
            .csrf()
                .disable()
            .sessionManagement()
                .invalidSessionStrategy(sessionInvalidStrategy)
                .maximumSessions(6)
                .sessionRegistry(sessionRegistry());
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource(@Value("${frontend.url}") String[] frontendUrls) {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList(frontendUrls));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("Content-Type"));
        configuration.setMaxAge(3600L);
        configuration.setAllowCredentials(Boolean.TRUE);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsServiceImpl)
            .passwordEncoder(passwordEncoder);
    }

    @Bean
    SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
