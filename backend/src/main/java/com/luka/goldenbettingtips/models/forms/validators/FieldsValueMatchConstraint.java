package com.luka.goldenbettingtips.models.forms.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = FieldsValueMatchValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldsValueMatchConstraint {

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String message() default "The fields must match";
    String firstField();
    String secondField();

    @Documented
    @Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
    @interface List {
        FieldsValueMatchConstraint[] value();
    }

}
