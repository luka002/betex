package com.luka.goldenbettingtips.models.forms;

import com.luka.goldenbettingtips.models.entities.UserRole;
import com.luka.goldenbettingtips.models.forms.validators.*;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Getter @Setter
@ValidatePasswordConstraint(password = "password",
                            confirmPassword = "confirmPassword",
                            doValidatePassword = "doValidatePassword")
@UniqueUsernameOrFromIdConstraint(username = "username", id = "id")
@UniqueEmailOrFromIdConstraint(email = "email", id = "id")
public class EditUser implements AdminFormValues {

    @NotNull
    private Long id;

    @NotBlank
    @Size(min = 2, max = 30)
    //Can only contain alphanumerical characters, hyphens and underscores.
    @Pattern(regexp = "^[a-zA-Z\\d-_]+$")
    private String username;

    private String password;
    private String confirmPassword;
    @NotNull
    private Boolean doValidatePassword;

    @NotBlank
    @Email
    private String email;

    @NotNull
    private UserRole userRole;

    @NotNull
    private Boolean enabled;

    private LocalDateTime lastPaymentTime;
    private LocalDateTime premiumExpiryTime;
    private String socialName;

}
