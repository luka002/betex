package com.luka.goldenbettingtips.models.forms.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ValidatePasswordValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidatePasswordConstraint {

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String message() default "Password error";
    String password();
    String confirmPassword();
    String doValidatePassword();

}
