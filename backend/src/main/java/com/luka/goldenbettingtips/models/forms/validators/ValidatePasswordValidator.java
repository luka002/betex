package com.luka.goldenbettingtips.models.forms.validators;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidatePasswordValidator implements ConstraintValidator<ValidatePasswordConstraint, Object> {

    private String password;
    private String confirmPassword;
    private String doValidatePassword;

    @Override
    public void initialize(ValidatePasswordConstraint constraintAnnotation) {
        this.password = constraintAnnotation.password();
        this.confirmPassword = constraintAnnotation.confirmPassword();
        this.doValidatePassword = constraintAnnotation.doValidatePassword();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        String passwordValue;
        String confirmPasswordValue;
        boolean doValidatePasswordValue;

        try {
            passwordValue =
                    (String) new BeanWrapperImpl(value).getPropertyValue(password);
            confirmPasswordValue =
                    (String) new BeanWrapperImpl(value).getPropertyValue(confirmPassword);
            doValidatePasswordValue =
                    (boolean) new BeanWrapperImpl(value).getPropertyValue(doValidatePassword);
        } catch (NullPointerException e) {
            return false;
        }

        if (!doValidatePasswordValue) {
            return true;
        }

        return passwordValue != null
                && passwordValue.length() >= 8
                && passwordValue.equals(confirmPasswordValue);
    }
}
