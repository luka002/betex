package com.luka.goldenbettingtips.models.forms.validators;

import com.luka.goldenbettingtips.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmailConstraint, String> {

    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public UniqueEmailValidator(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
        return !userDetailsService.isEmailTaken(email);
    }
}
