package com.luka.goldenbettingtips.models.forms.validators;

import com.luka.goldenbettingtips.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsernameConstraint, String> {

    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public UniqueUsernameValidator(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        return !userDetailsService.isUsernameTaken(username);
    }
}
