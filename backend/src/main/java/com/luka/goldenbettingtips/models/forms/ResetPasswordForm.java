package com.luka.goldenbettingtips.models.forms;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ResetPasswordForm extends BasePasswordForm {

    private String token;

}
