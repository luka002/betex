package com.luka.goldenbettingtips.models.enums;

public enum Role {
    REGULAR, PREMIUM, ADMIN
}
