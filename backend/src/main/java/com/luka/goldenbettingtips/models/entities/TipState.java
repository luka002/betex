package com.luka.goldenbettingtips.models.entities;

import com.luka.goldenbettingtips.models.enums.State;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(name = "tip_state", schema = "public")
public class TipState {

    @Id
    private long id;

    @Enumerated(EnumType.STRING)
    private State state;

}
