package com.luka.goldenbettingtips.models.entities;

import com.luka.goldenbettingtips.models.enums.Type;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(name = "tip_type", schema = "public")
public class TipType {

    @Id
    private long id;

    @Enumerated(EnumType.STRING)
    private Type type;

}
