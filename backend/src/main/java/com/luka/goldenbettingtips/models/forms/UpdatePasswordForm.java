package com.luka.goldenbettingtips.models.forms;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter @Setter
public class UpdatePasswordForm extends BasePasswordForm {

    @NotBlank
    @Size(min = 8)
    private String currentPassword;

}
