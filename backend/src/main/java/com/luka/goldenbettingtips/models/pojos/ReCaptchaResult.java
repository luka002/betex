package com.luka.goldenbettingtips.models.pojos;

import lombok.Data;

import java.util.Map;

@Data
public class ReCaptchaResult {

    private boolean success;
    private String hostname;
    private String challenge_ts;
    private String apk_package_name;
    private double score;
    private String action;
    private Map<String, String> error;

}
