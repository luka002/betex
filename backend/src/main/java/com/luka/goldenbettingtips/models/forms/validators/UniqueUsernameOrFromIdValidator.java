package com.luka.goldenbettingtips.models.forms.validators;

import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.services.UserDetailsServiceImpl;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameOrFromIdValidator implements ConstraintValidator<UniqueUsernameOrFromIdConstraint, Object> {

    private String id;
    private String username;

    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public UniqueUsernameOrFromIdValidator(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void initialize(UniqueUsernameOrFromIdConstraint constraintAnnotation) {
        this.id = constraintAnnotation.id();
        this.username = constraintAnnotation.username();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        long idValue;
        String usernameValue;

        try {
            idValue = (long) new BeanWrapperImpl(value).getPropertyValue(id);
            usernameValue = (String) new BeanWrapperImpl(value).getPropertyValue(username);

            if (userDetailsService.isUsernameTaken(usernameValue)) {
                User user = userDetailsService.findOne(idValue);
                return user.getUsername().equals(usernameValue);
            }
        } catch (NullPointerException e) {
            return false;
        }

        return true;
    }
}
