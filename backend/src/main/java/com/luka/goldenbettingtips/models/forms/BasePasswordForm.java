package com.luka.goldenbettingtips.models.forms;

import com.luka.goldenbettingtips.models.forms.validators.ValidatePasswordConstraint;
import lombok.Data;

@Data
@ValidatePasswordConstraint(password = "password",
                            confirmPassword = "confirmPassword",
                            doValidatePassword = "doValidatePassword")
public class BasePasswordForm {

    private String password;
    private String confirmPassword;
    private Boolean doValidatePassword = true;

}
