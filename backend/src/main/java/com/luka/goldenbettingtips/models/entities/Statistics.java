package com.luka.goldenbettingtips.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter @Setter
public class Statistics {

    @Id
    private long id;
    private long weekendPackagesBoughtCount;
    private double weekendPackagesTotalAmountEarned;

}
