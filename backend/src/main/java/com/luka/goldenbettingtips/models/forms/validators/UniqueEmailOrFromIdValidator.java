package com.luka.goldenbettingtips.models.forms.validators;

import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.services.UserDetailsServiceImpl;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailOrFromIdValidator implements ConstraintValidator<UniqueEmailOrFromIdConstraint, Object> {

    private String id;
    private String email;

    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public UniqueEmailOrFromIdValidator(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void initialize(UniqueEmailOrFromIdConstraint constraintAnnotation) {
        this.id = constraintAnnotation.id();
        this.email = constraintAnnotation.email();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        long idValue;
        String emailValue;

        try {
            idValue = (long) new BeanWrapperImpl(value).getPropertyValue(id);
            emailValue = (String) new BeanWrapperImpl(value).getPropertyValue(email);

            if (userDetailsService.isEmailTaken(emailValue)) {
                User user = userDetailsService.findOne(idValue);
                return user.getEmail().equals(emailValue);
            }
        } catch (NullPointerException e) {
            return false;
        }

        return true;
    }
}
