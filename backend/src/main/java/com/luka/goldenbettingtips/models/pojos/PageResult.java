package com.luka.goldenbettingtips.models.pojos;

import com.luka.goldenbettingtips.models.entities.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class PageResult {

    private int currentPage;
    private int previousPagesCount;
    private int nextPagesCount;
    private int lastPage;
    private List<User> users;

    public PageResult(int currentPage, int previousPagesCount, int nextPagesCount, int lastPage, List<User> users) {
        this.currentPage = currentPage;
        this.previousPagesCount = previousPagesCount;
        this.nextPagesCount = nextPagesCount;
        this.lastPage = lastPage;
        this.users = users;
    }

}
