package com.luka.goldenbettingtips.models.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter @Setter
@NoArgsConstructor
public class TicketUserKey implements Serializable {

    @Column(name = "ticket_id")
    private String ticketId;

    @Column(name = "user_id")
    private Long userId;

    public TicketUserKey(String ticketId, Long userId) {
        this.ticketId = ticketId;
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TicketUserKey)) return false;
        TicketUserKey that = (TicketUserKey) o;
        return ticketId.equals(that.ticketId) &&
                userId.equals(that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticketId, userId);
    }

}
