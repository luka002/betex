package com.luka.goldenbettingtips.models.enums;

public enum State {
    ACTIVE, HISTORY, EXPIRED, FINISHED
}
