package com.luka.goldenbettingtips.models.forms;

import com.luka.goldenbettingtips.models.forms.validators.FieldsValueMatchConstraint;
import com.luka.goldenbettingtips.models.forms.validators.UniqueEmailConstraint;
import com.luka.goldenbettingtips.models.forms.validators.UniqueUsernameConstraint;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter @Setter
@FieldsValueMatchConstraint(firstField = "password", secondField = "confirmPassword")
public class RegistrationForm {

    @NotBlank
    @Size(min = 2, max = 30)
    @UniqueUsernameConstraint
    //Can only contain alphanumerical characters, hyphens and underscores.
    @Pattern(regexp = "^[a-zA-Z\\d-_]+$")
    private String username;

    @NotBlank
    @Size(min = 8)
    private String password;

    @NotBlank
    private String confirmPassword;

    @NotBlank
    @Email
    @UniqueEmailConstraint
    private String email;

}
