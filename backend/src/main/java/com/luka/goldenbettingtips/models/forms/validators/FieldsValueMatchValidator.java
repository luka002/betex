package com.luka.goldenbettingtips.models.forms.validators;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldsValueMatchValidator implements ConstraintValidator<FieldsValueMatchConstraint, Object> {

    private String firstField;
    private String secondField;

    @Override
    public void initialize(FieldsValueMatchConstraint constraintAnnotation) {
        this.firstField = constraintAnnotation.firstField();
        this.secondField = constraintAnnotation.secondField();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Object firstFieldValue = new BeanWrapperImpl(value).getPropertyValue(firstField);
        Object secondFieldValue = new BeanWrapperImpl(value).getPropertyValue(secondField);

        if (firstFieldValue != null) {
            return firstFieldValue.equals(secondFieldValue);
        } else {
            return secondFieldValue == null;
        }
    }
}
