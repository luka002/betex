package com.luka.goldenbettingtips.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Getter @Setter
public class Ticket {

    @Id
    private String id;

    private BigDecimal price;
    private BigDecimal odds;
    private LocalDateTime created;
    private boolean active;

    @OneToMany
    @JoinColumn(name = "ticket_id")
    private List<Tip> tips;

    @JsonIgnore
    @OneToMany(mappedBy = "ticket")
    Set<TicketUser> ticketUsers;

}
