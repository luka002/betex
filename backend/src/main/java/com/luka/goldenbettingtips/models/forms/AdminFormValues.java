package com.luka.goldenbettingtips.models.forms;

import com.luka.goldenbettingtips.models.entities.UserRole;

import java.time.LocalDateTime;

public interface AdminFormValues {

    String getUsername();
    String getPassword();
    String getEmail();
    UserRole getUserRole();
    Boolean getEnabled();
    LocalDateTime getLastPaymentTime();
    LocalDateTime getPremiumExpiryTime();
    String getSocialName();

}
