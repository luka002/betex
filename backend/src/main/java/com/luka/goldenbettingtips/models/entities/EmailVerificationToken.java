package com.luka.goldenbettingtips.models.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Entity
@NoArgsConstructor
@Getter @Setter
public class EmailVerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="email_verification_token_id_seq")
    @SequenceGenerator(name="email_verification_token_id_seq", sequenceName="email_verification_token_id_seq", allocationSize=1)
    private long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    private String token;
    private LocalDateTime expiryTime;

    public EmailVerificationToken(User user, String token) {
        this.user = user;
        this.token = token;
        this.expiryTime = LocalDateTime.now(ZoneOffset.UTC).plusDays(1);
    }

}
