package com.luka.goldenbettingtips.models.forms.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueEmailOrFromIdValidator.class)
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueEmailOrFromIdConstraint {

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String message() default "Password error";
    String id();
    String email();

}
