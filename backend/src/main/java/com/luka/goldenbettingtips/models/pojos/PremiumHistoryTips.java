package com.luka.goldenbettingtips.models.pojos;

import com.luka.goldenbettingtips.models.entities.Tip;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter @Setter
public class PremiumHistoryTips {

    private List<Tip> tips;
    private LocalDateTime timeStart;
    private LocalDateTime timeEnd;
    private int weekNumber;

    public PremiumHistoryTips(List<Tip> tips,
                              LocalDateTime timeStart,
                              LocalDateTime timeEnd,
                              int weekNumber) {
        this.tips = tips;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.weekNumber = weekNumber;
    }
}
