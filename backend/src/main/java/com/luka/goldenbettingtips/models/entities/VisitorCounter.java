package com.luka.goldenbettingtips.models.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "visitor_counter", schema = "public")
public class VisitorCounter {

    @Id
    private LocalDate date;
    private int totalCount;

}
