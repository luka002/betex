package com.luka.goldenbettingtips.models.entities;

import com.luka.goldenbettingtips.models.enums.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(name = "user_role", schema = "public")
public class UserRole {

    @Id
    private long id;

    @Enumerated(EnumType.STRING)
    private Role role;

}
