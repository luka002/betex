package com.luka.goldenbettingtips.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
@Table(name = "tip", schema = "public")
public class Tip {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="tip_id_seq")
    @SequenceGenerator(name="tip_id_seq", sequenceName="tip_id_seq", allocationSize=1)
    private long id;

    private String teamHome;
    private String teamAway;
    private String result;
    private boolean status;
    private LocalDateTime time;
    private String odds;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id")
    private TipType tipType;

    private String bet;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "state_id")
    private TipState tipState;

    private String league;

    @Column(name = "ticket_id")
    private String ticketId;

}
