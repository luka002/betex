package com.luka.goldenbettingtips.models.forms.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueUsernameOrFromIdValidator.class)
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueUsernameOrFromIdConstraint {

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String message() default "Password error";
    String id();
    String username();

}
