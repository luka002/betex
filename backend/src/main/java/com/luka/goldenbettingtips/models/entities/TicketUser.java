package com.luka.goldenbettingtips.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
public class TicketUser {

    @EmbeddedId
    private TicketUserKey id;

    @ManyToOne
    @MapsId("ticket_id")
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    @ManyToOne
    @MapsId("user_id")
    @JoinColumn(name = "user_id")
    private User user;

    private LocalDateTime created;

}
