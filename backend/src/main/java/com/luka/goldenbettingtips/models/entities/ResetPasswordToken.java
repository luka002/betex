package com.luka.goldenbettingtips.models.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Entity
@Table(name = "reset_password_token", schema = "public")
@NoArgsConstructor
@Getter @Setter
public class ResetPasswordToken {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="reset_password_token_id_seq")
    @SequenceGenerator(name="reset_password_token_id_seq", sequenceName="reset_password_token_id_seq", allocationSize=1)
    private long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    private String token;
    private LocalDateTime creationTime;
    private LocalDateTime expiryTime;

    public ResetPasswordToken(User user, String token) {
        this.user = user;
        updateToken(token);
    }

    public void updateToken(String token) {
        this.token = token;
        this.expiryTime = LocalDateTime.now(ZoneOffset.UTC).plusDays(1);
        this.creationTime = LocalDateTime.now(ZoneOffset.UTC);
    }

    public boolean isLessThanTwoMinutesOld() {
        return creationTime.plusMinutes(2).isAfter(LocalDateTime.now(ZoneOffset.UTC));
    }

}
