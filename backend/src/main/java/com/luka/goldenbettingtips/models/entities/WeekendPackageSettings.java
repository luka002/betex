package com.luka.goldenbettingtips.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Getter @Setter
@Table(name = "weekend_package_settings", schema = "public")
public class WeekendPackageSettings {

    @Id
    private long id;
    private boolean buyingEnabled;
    private String buyingEnabledMessage;
    private String buyingDisabledMessage;
    private double weekendPrice;

}

