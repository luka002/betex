package com.luka.goldenbettingtips.models.forms;

import com.luka.goldenbettingtips.models.entities.UserRole;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter @Setter
public class AddUserAsAdminForm extends RegistrationForm implements AdminFormValues {

    @NotNull
    private UserRole userRole;

    @NotNull
    private Boolean enabled;

    private LocalDateTime lastPaymentTime;
    private LocalDateTime premiumExpiryTime;
    private String socialName;

}
