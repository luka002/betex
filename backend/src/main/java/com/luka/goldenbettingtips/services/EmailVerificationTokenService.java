package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.EmailVerificationToken;
import com.luka.goldenbettingtips.repositories.EmailVerificationTokenRepository;
import com.luka.goldenbettingtips.models.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailVerificationTokenService {

    private final EmailVerificationTokenRepository emailVerificationTokenRepository;

    @Autowired
    public EmailVerificationTokenService(EmailVerificationTokenRepository emailVerificationTokenRepository) {
        this.emailVerificationTokenRepository = emailVerificationTokenRepository;
    }

    public EmailVerificationToken findByToken(String verificationToken) {
        return emailVerificationTokenRepository.findByToken(verificationToken);
    }

    public void save(EmailVerificationToken token) {
        emailVerificationTokenRepository.save(token);
    }

    public void deleteToken(String token) {
        emailVerificationTokenRepository.deleteByToken(token);
    }

    public List<EmailVerificationToken> getAllTokens() {
        return emailVerificationTokenRepository.findAll();
    }

}
