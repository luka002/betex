package com.luka.goldenbettingtips.services;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class SocialAuthService {

    private GoogleIdTokenVerifier verifier;
    private String usernameSuffix;
    private String passwordSuffix;

    public SocialAuthService(@Value("${google.social.auth.id}") String clientId,
                             @Value("${social.auth.username.suffix}") String usernameSuffix,
                             @Value("${social.auth.password.suffix}") String passwordSuffix) {
        verifier = new GoogleIdTokenVerifier
                .Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
                .setAudience(Collections.singleton(clientId))
                .build();
        this.usernameSuffix = usernameSuffix;
        this.passwordSuffix = passwordSuffix;
    }

    public GoogleIdTokenVerifier getVerifier() {
        return verifier;
    }

    public String getUsernameSuffix() {
        return usernameSuffix;
    }

    public String getPasswordSuffix() {
        return passwordSuffix;
    }

}
