package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.Tip;
import com.luka.goldenbettingtips.models.entities.TipState;
import com.luka.goldenbettingtips.models.entities.TipType;
import com.luka.goldenbettingtips.models.pojos.PremiumHistoryTips;
import com.luka.goldenbettingtips.repositories.TipRepository;
import com.luka.goldenbettingtips.models.enums.State;
import com.luka.goldenbettingtips.models.enums.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@Service
public class TipService {

    private final TimeService timeService;
    private final TipRepository tipRepository;

    private TipType freeTipType;
    private TipType premiumTipType;
    private TipState activeTipState;
    private TipState historyTipState;
    private TipState finishedTipState;

    @Autowired
    public TipService(TimeService timeService,
                      TipRepository tipRepository,
                      TipTypeService tipTypeService,
                      TipStateService tipStateService) {
        this.timeService = timeService;
        this.tipRepository = tipRepository;
        freeTipType = tipTypeService.getTypeByEnum(Type.FREE);
        premiumTipType = tipTypeService.getTypeByEnum(Type.PREMIUM);
        activeTipState = tipStateService.getTipStateByEnum(State.ACTIVE);
        historyTipState = tipStateService.getTipStateByEnum(State.HISTORY);
        finishedTipState = tipStateService.getTipStateByEnum(State.FINISHED);
    }

    public Tip findOne(long id) {
        Optional<Tip> tip = tipRepository.findById(id);
        return tip.orElse(null);
    }

    public Tip saveTip(Tip tip) {
        return tipRepository.save(tip);
    }

    public void deleteTip(long id) {
        tipRepository.deleteById(id);
    }

    public List<Tip> getActiveFreeTips() {
        return tipRepository.findByTypeAndStatesOrderByTimeAsc(
                freeTipType, activeTipState, finishedTipState
        );
    }

    public List<Tip> getHistoryFreeTips() {
        return tipRepository
                .findByTipTypeAndTipStateOrderByTimeDesc(freeTipType, historyTipState);
    }

    public List<Tip> getActivePremiumTips() {
        return tipRepository.findByTypeAndStatesOrderByTimeAsc(
                premiumTipType, activeTipState, finishedTipState
        );
    }

    public List<Tip> getHistoryPremiumTips() {
        return tipRepository.findByTipTypeAndTipStateAndTimeAfterOrderByTimeDesc(
                premiumTipType, historyTipState, timeService.getCurrentTimeUTC().minusMonths(1)
        );
    }

}
