package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.models.entities.VisitorCounter;
import com.luka.goldenbettingtips.repositories.VisitorCounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class VisitorCounterService {

    private final VisitorCounterRepository visitorCounterRepository;
    private final TimeService timeService;

    @Autowired
    public VisitorCounterService(VisitorCounterRepository visitorCounterRepository,
                                 TimeService timeService) {
        this.visitorCounterRepository = visitorCounterRepository;
        this.timeService = timeService;
    }

    public void increaseCounter() {
        LocalDate currentDate = timeService.getCurrentDateUTC();
        if (visitorCounterRepository.existsById(currentDate)) {
            VisitorCounter counter = visitorCounterRepository.getOne(currentDate);
            counter.setTotalCount(counter.getTotalCount() + 1);
            visitorCounterRepository.save(counter);
        } else {
            visitorCounterRepository.save(new VisitorCounter(currentDate, 1));
        }
    }

    public List<VisitorCounter> getVisitorCountByDate() {
        return visitorCounterRepository.findAllByOrderByDateDesc();
    }

    public List<VisitorCounter> getVisitorCountByDate(int days) {
        Page<VisitorCounter> visitorCounter = visitorCounterRepository
                        .findAllByOrderByDateDesc(PageRequest.of(0, days));

        return visitorCounter.getContent();
    }

}
