package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.ResetPasswordToken;
import com.luka.goldenbettingtips.repositories.ResetPasswordTokenRepository;
import com.luka.goldenbettingtips.models.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResetPasswordService {

    private final ResetPasswordTokenRepository resetPasswordTokenRepository;

    @Autowired
    public ResetPasswordService(ResetPasswordTokenRepository resetPasswordTokenRepository) {
        this.resetPasswordTokenRepository = resetPasswordTokenRepository;
    }

    public ResetPasswordToken findByToken(String resetPasswordToken) {
        return resetPasswordTokenRepository.findByToken(resetPasswordToken);
    }

    public ResetPasswordToken findByUser(User user) {
        return resetPasswordTokenRepository.findByUser(user);
    }

    public void save(ResetPasswordToken resetPasswordToken) {
        resetPasswordTokenRepository.save(resetPasswordToken);
    }

    public void delete(String token) {
        resetPasswordTokenRepository.deleteByToken(token);
    }

    public List<ResetPasswordToken> getAllTokens() {
        return resetPasswordTokenRepository.findAll();
    }

    public void delete(ResetPasswordToken token) {
        resetPasswordTokenRepository.delete(token);
    }

}
