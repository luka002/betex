package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.TipType;
import com.luka.goldenbettingtips.models.enums.Type;
import com.luka.goldenbettingtips.repositories.TipTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TipTypeService {

    private final TipTypeRepository tipTypeRepository;

    @Autowired
    public TipTypeService(TipTypeRepository tipTypeRepository) {
        this.tipTypeRepository = tipTypeRepository;
    }

    public TipType getTypeByEnum(Type type) {
        return tipTypeRepository.findByType(type);
    }

}
