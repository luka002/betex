package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.WeekendPackageSettings;
import com.luka.goldenbettingtips.repositories.WeekendPackageSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WeekendPackageSettingsService {

    private final WeekendPackageSettingsRepository weekendPackageSettingsRepository;

    @Autowired
    public WeekendPackageSettingsService(WeekendPackageSettingsRepository weekendPackageSettingsRepository) {
        this.weekendPackageSettingsRepository = weekendPackageSettingsRepository;
    }

    public WeekendPackageSettings getSettings() {
        Optional<WeekendPackageSettings> settings = weekendPackageSettingsRepository.findById((long)1);
        return settings.orElse(null);
    }

    public WeekendPackageSettings updateSettings(WeekendPackageSettings settings) {
        return weekendPackageSettingsRepository.save(settings);
    }

}
