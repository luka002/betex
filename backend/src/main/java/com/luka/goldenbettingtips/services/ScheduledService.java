package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.EmailVerificationToken;
import com.luka.goldenbettingtips.models.entities.ResetPasswordToken;
import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.models.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
public class ScheduledService {

    private final EmailVerificationTokenService emailVerificationTokenService;
    private final ResetPasswordService resetPasswordService;
    private final UserDetailsServiceImpl userDetailsService;
    private final UserRoleService userRoleService;
    private final SessionRegistry sessionRegistry;
    private final TimeService timeService;

    @Autowired
    public ScheduledService(EmailVerificationTokenService emailVerificationTokenService,
                            ResetPasswordService resetPasswordService,
                            UserDetailsServiceImpl userDetailsService,
                            UserRoleService userRoleService,
                            SessionRegistry sessionRegistry,
                            TimeService timeService) {
        this.emailVerificationTokenService = emailVerificationTokenService;
        this.resetPasswordService = resetPasswordService;
        this.userDetailsService = userDetailsService;
        this.userRoleService = userRoleService;
        this.sessionRegistry = sessionRegistry;
        this.timeService = timeService;
    }

    @Scheduled(cron = "0 0 0 */1 * *", zone = "UTC")
    public void setWeekendPremiumUsersToRegular() {
        //Sets premium user to regular if membership expired
        List<User> users = userDetailsService.getPremiumUsers();
        for (User user : users) {
            if (user.getPremiumExpiryTime() != null &&
                    user.getPremiumExpiryTime().isBefore(timeService.getCurrentTimeUTC())) {
                user.setUserRole(userRoleService.getRoleByEnum(Role.REGULAR));
                userDetailsService.saveUser(user);
            }
        }

        //Expires sessions from premium users
        List<Object> loggedUsers = sessionRegistry.getAllPrincipals();
        for (Object principal : loggedUsers) {
            if(principal instanceof User) {
                User loggedUser = (User) principal;
                if(loggedUser.getUserRole().getRole() == Role.PREMIUM) {
                    List<SessionInformation> sessionsInfo = sessionRegistry.getAllSessions(principal, false);
                    if(null != sessionsInfo && sessionsInfo.size() > 0) {
                        for (SessionInformation sessionInformation : sessionsInfo) {
                            sessionInformation.expireNow();
                        }
                    }
                }
            }
        }
    }

    @Scheduled(cron = "30 0 * * * *", zone = "UTC")
    public void deleteExpiredResetPasswordTokens() {
        List<ResetPasswordToken> tokens = resetPasswordService.getAllTokens();

        for (ResetPasswordToken token : tokens) {
            if (token.getExpiryTime().isBefore(timeService.getCurrentTimeUTC())) {
                resetPasswordService.delete(token);
            }
        }
    }

    @Scheduled(cron = "45 0 * * * *", zone = "UTC")
    @Transactional
    public void deleteExpiredEmailVerificationTokensAndItsUsers() {
        List<EmailVerificationToken> tokens = emailVerificationTokenService.getAllTokens();

        for (EmailVerificationToken token : tokens) {
            if (token.getExpiryTime().isBefore(timeService.getCurrentTimeUTC())) {
                User user = token.getUser();
                emailVerificationTokenService.deleteToken(token.getToken());
                userDetailsService.deleteUser(user.getId());
            }
        }
    }

}
