package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.Ticket;
import com.luka.goldenbettingtips.models.entities.TicketUser;
import com.luka.goldenbettingtips.models.entities.TicketUserKey;
import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.repositories.TicketUserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketUserService {

    private final TicketUserRepository ticketUserRepository;
    private final TimeService timeService;

    public TicketUserService(TicketUserRepository ticketUserRepository,
                             TimeService timeService) {
        this.ticketUserRepository = ticketUserRepository;
        this.timeService = timeService;
    }

    public List<TicketUser> findByUser(User user) {
        return ticketUserRepository.findByUser(user);
    }

    public boolean existsById(TicketUserKey id) {
        return ticketUserRepository.existsById(id);
    }

    public void userBoughtTicket(User user, String ticketId) {
        TicketUserKey id = new TicketUserKey();
        id.setUserId(user.getId());
        id.setTicketId(ticketId);

        TicketUser ticketUser = new TicketUser();
        ticketUser.setId(id);
        ticketUser.setCreated(timeService.getCurrentTimeUTC());

        ticketUserRepository.save(ticketUser);
    }

    public List<User> fetchUsers(String id) {
        return ticketUserRepository.findUsersByTicketId(id);
    }

    public void deleteByUser(User user) {
        ticketUserRepository.deleteByUser(user);
    }

}
