package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.Ticket;
import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayPalService {

    @Value("${paypal.client.id}")
    private String clientId;
    @Value("${paypal.client.secret}")
    private String clientSecret;
    @Value("${paypal.environment}")
    private String environment;
    @Value("${paypal.currency}")
    private String currency;
    @Value("${paypal.intent}")
    private String intent;
    @Value("${paypal.ticket.description}")
    private String ticketDescription;
    @Value("${paypal.premium.description}")
    private String premiumDescription;
    @Value("${paypal.success.url}")
    private String successUrl;
    @Value("${paypal.cancel.url}")
    private String cancelUrl;

    private final WeekendPackageSettingsService weekendSettingsService;

    @Autowired
    public PayPalService(WeekendPackageSettingsService weekendSettingsService) {
        this.weekendSettingsService = weekendSettingsService;
    }

    public Map<String, String> createBecomePremiumPayment() {
        return createPayment(null);
    }

    public Map<String, String> createTicketPayment(Ticket ticket) {
        return createPayment(ticket);
    }

    private Map<String, String> createPayment(Ticket ticket) {
        Map<String, String> response = new HashMap<>();
        Payment payment = getPayment(ticket);

        try {
            String redirectUrl = "";
            APIContext apiContext = new APIContext(clientId, clientSecret, environment);
            Payment createdPayment = payment.create(apiContext);

            if (createdPayment != null) {
                List<Links> links = createdPayment.getLinks();

                for(Links link : links) {
                    if (link.getRel().equals("approval_url")) {
                        redirectUrl = link.getHref();
                        break;
                    }
                }

                response.put("status", "success");
                response.put("redirect_url", redirectUrl);
            }
        } catch (PayPalRESTException e) {
        }

        return response;
    }

    private Payment getPayment(Ticket ticket) {
        String formattedPrice;
        if (ticket != null) {
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            formattedPrice = decimalFormat.format(ticket.getPrice())
                    .replace(",", ".");
        } else {
            double weekendPrice = weekendSettingsService.getSettings().getWeekendPrice();
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            formattedPrice = decimalFormat.format(weekendPrice)
                    .replace(",", ".");
        }

        Amount amount = new Amount();
        amount.setCurrency(currency);
        amount.setTotal(formattedPrice);

        Transaction transaction = new Transaction();
        if (ticket != null) {
            transaction.setDescription(ticketDescription + " for " + formattedPrice + "€");
        } else {
            transaction.setDescription(premiumDescription + " for " + formattedPrice + "€");
        }
        transaction.setAmount(amount);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl(cancelUrl);
        redirectUrls.setReturnUrl(successUrl + (ticket != null ? ("?ticketId=" + ticket.getId()) : ""));

        Payment payment = new Payment();
        payment.setIntent(intent);
        payment.setPayer(payer);
        payment.setTransactions(transactions);
        payment.setRedirectUrls(redirectUrls);

        return payment;
    }

    public Map<String, Object> executePayment(String paymentId, String payerId) {
        Map<String, Object> response = new HashMap<>();

        Payment payment = new Payment();
        payment.setId(paymentId);

        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(payerId);

        try {
            APIContext apiContext = new APIContext(clientId, clientSecret, environment);
            Payment createdPayment = payment.execute(apiContext, paymentExecution);

            if (createdPayment.getState().equals("approved")) {
                response.put("status", "success");
            }
        } catch (PayPalRESTException e) {
        }

        return response;
    }

}
