package com.luka.goldenbettingtips.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TextTemplates {

    @Value("${frontend.url}")
    private String[] frontendUrl;
    @Value("${activate.account.url}")
    private String activateAccountUrl;
    @Value("${reset.password.url}")
    private String resetPasswordUrl;

    public String noUserWithEmail(String email) {
        return "Can't find email " + email + ", sorry.";
    }

    public String googleAccount() {
        return "You can not change password if you registered with google.";
    }

    public String twoMinutesNotPassed() {
        return "Reset password link has already been sent to your email. " +
                "Please wait 2 minutes if you want another link.";
    }

    public String resetSubject() {
        return "Reset password";
    }


    public String resetMailSent(String email) {
        return "Reset password link has been sent to your email " + email;
    }

    public String accountActivationSubject() {
        return "Account activation";
    }

    public String activateAccountHtml(String token) {
        return emailHtmlTemplate(
                token,
                "Please activate your account bellow",
                "ACTIVATE YOUR ACCOUNT",
                this.activateAccountUrl,
                "Activate account"
        );
    }

    public String resetPasswordHtml(String token) {
        return emailHtmlTemplate(
                token,
                "Please reset your password bellow",
                "RESET YOUR PASSWORD",
                this.resetPasswordUrl,
                "Reset password"
        );
    }

    private String emailHtmlTemplate(String token, String message, String btnText,
                                     String path, String title) {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>" + title + "</title>\n" +
                "</head>\n" +
                "<body style=\"text-align: center; margin: 20px\">\n" +
                "   <p style=\"color: #3d3d3d;\n" +
                "              font-family: Lato, sans-serif;\n" +
                "              font-size: 64px; " +
                "              margin: 10px\">" +
                "       BETex" +
                "   </p>\n" +
                "   <p style=\"margin: auto; font-family: 'Helvetica Neue', " +
                "             'Helvetica', Helvetica, Arial, sans-serif; " +
                "              width: 400px; " +
                "              border: 1px gray solid; " +
                "              padding: 15px; " +
                "              border-radius: 5px; " +
                "              color: gray;\">" +
                "       <strong>" + message + "</strong>. " +
                "       If you received this by mistake or weren't expecting it, " +
                "       please disregard this email." +
                "   </p>\n" +
                "   <a href=\"" + path + "?token=" + token + "\" " +
                "      style=\"margin-top: 8px; " +
                "              width: 393px; " +
                "              font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; " +
                "              color: #ffffff; " +
                "              font-size: 18px; " +
                "              padding: 20px; " +
                "              font-weight: bold; " +
                "              background: #00E056; " +
                "              border-radius: 5px; " +
                "              text-decoration: none; " +
                "              display: inline-block;\">" +
                        btnText +
                "   </a>\n" +
                "</body>\n" +
                "</html>";
    }

}
