package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.UserRole;
import com.luka.goldenbettingtips.models.enums.Role;
import com.luka.goldenbettingtips.repositories.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {

    private final UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleService(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    public UserRole getRoleByEnum(Role role) {
        return userRoleRepository.findByRole(role);
    }

}
