package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.TipState;
import com.luka.goldenbettingtips.models.enums.State;
import com.luka.goldenbettingtips.repositories.TipStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TipStateService {

    private final TipStateRepository tipStateRepository;

    @Autowired
    public TipStateService(TipStateRepository tipStateRepository) {
        this.tipStateRepository = tipStateRepository;
    }

    public TipState getTipStateByEnum(State state) {
        return tipStateRepository.findByState(state);
    }

}
