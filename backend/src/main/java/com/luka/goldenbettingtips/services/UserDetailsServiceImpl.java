package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.pojos.PageResult;
import com.luka.goldenbettingtips.models.enums.Role;
import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final TimeService timeService;
    private final UserRepository userRepository;
    private final UserRoleService userRoleService;
    private final StatisticsService statisticsService;

    private final int USERS_PER_PAGE = 7;
    private final int MAX_PAGE_OFFSET = 2;
    private final int FIRST_PAGE = 1;
    private final String SORT_BY_USERNAME = "username";

    @Autowired
    public UserDetailsServiceImpl(TimeService timeService,
                                  UserRepository userRepository,
                                  UserRoleService userRoleService,
                                  StatisticsService statisticsService) {
        this.timeService = timeService;
        this.userRepository = userRepository;
        this.userRoleService = userRoleService;
        this.statisticsService = statisticsService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found.");
        }
        return user;
    }

    //region Basic CRUD operations ================================================================
    public User findOne(long id) {
        Optional<User> user = userRepository.findById(id);
        return user.orElse(null);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public List<User> getPremiumUsers() {
        return userRepository.findByUserRole(userRoleService.getRoleByEnum(Role.PREMIUM));
    }
    //endregion ===================================================================================

    //region User purchased weekly package ========================================================
    public void userBoughtMonthlyPackage(User user) {
        user.setUserRole(userRoleService.getRoleByEnum(Role.PREMIUM));
        user.setLastPaymentTime(timeService.getCurrentTimeUTC());
        user.setPremiumExpiryTime(timeService.getCurrentTimeUTC().plusMonths(1));
//                        .with(TemporalAdjusters.next(DayOfWeek.MONDAY))
//                        .withHour(23).withMinute(59).withSecond(59).withNano(0)

        //Updates user session authorities
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(Role.PREMIUM.name()));
        Authentication newAuth = new UsernamePasswordAuthenticationToken(
                auth.getPrincipal(), auth.getCredentials(), authorities
        );
        SecurityContextHolder.getContext().setAuthentication(newAuth);

        userRepository.save(user);
        statisticsService.updateStatistics();
    }
    //endregion ===================================================================================

    //region Pagination ===========================================================================
    public PageResult getPageWithUsers(int page, String username) {
        long userCount = userRepository
                .countByUsernameContainingIgnoreCaseOrSocialNameContainingIgnoreCase(username, username);
        int lastPage = (int) ((userCount - 1) / USERS_PER_PAGE) + 1;
        int previousPagesCount = getPageCount(FIRST_PAGE, page);
        int nextPagesCount = getPageCount(page, lastPage);

        if (page < 1 || page > lastPage) return null;

        Page<User> users = userRepository.findByUsernameContainingIgnoreCaseOrSocialNameContainingIgnoreCase(
                username,
                username,
                PageRequest.of(
                        (page - 1),
                        USERS_PER_PAGE,
                        Sort.Direction.ASC,
                        SORT_BY_USERNAME
                )
        );

        return new PageResult(
                page, previousPagesCount, nextPagesCount, lastPage, users.getContent()
        );
    }

    private int getPageCount(int lowerPage, int higherPage) {
        if (higherPage - lowerPage == 0) {
            return 0;
        } else if (higherPage - lowerPage >= MAX_PAGE_OFFSET) {
            return MAX_PAGE_OFFSET;
        }
        return higherPage - lowerPage;
    }
    //endregion ===================================================================================

    //region Checking =============================================================================
    public boolean isUsernameTaken(String username) {
        return userRepository.findByUsername(username) != null;
    }

    public boolean isEmailTaken(String email) {
        return userRepository.findByEmail(email) != null;
    }
    //endregion ===================================================================================

}
