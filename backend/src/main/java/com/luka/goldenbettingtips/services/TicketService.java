package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.*;
import com.luka.goldenbettingtips.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TicketService {

    private final TicketRepository ticketRepository;
    private final TicketUserService ticketUserService;
    private final TimeService timeService;

    @Autowired
    public TicketService(TicketRepository ticketRepository,
                         TicketUserService ticketUserService,
                         TimeService timeService) {
        this.ticketRepository = ticketRepository;
        this.ticketUserService = ticketUserService;
        this.timeService = timeService;
    }

    public List<Ticket> getActiveTickets() {
        return ticketRepository.findByActiveTrue();
    }

    public Ticket addTicket(Ticket ticket) {
        return ticketRepository.save(ticket);
    }

    public void deleteTicket(String id) {
        ticketRepository.deleteById(id);
    }

    public Ticket findById(String id) {
        Optional<Ticket> ticket = ticketRepository.findById(id);
        return ticket.orElse(null);
    }

    public boolean isTicketBuyableByUser(String ticketId, User user) {
        return
            !ticketUserService.existsById(new TicketUserKey(ticketId, user.getId())) &&
            ticketRepository.existsById(ticketId) &&
            findById(ticketId).isActive();
    }

    public boolean isTicketStillBuyable(Ticket ticket) {
        for (Tip tip : ticket.getTips()) {
            if (tip.getTime().minusMinutes(15).isBefore(timeService.getCurrentTimeUTC())) {
                return false;
            }
        }
        return true;
    }

}
