package com.luka.goldenbettingtips.services;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class TimeService {

    public LocalDateTime getCurrentTimeUTC() {
        return LocalDateTime.now(ZoneOffset.UTC);
    }

    public LocalDate getCurrentDateUTC() {
        return LocalDate.now(ZoneOffset.UTC);
    }

}
