package com.luka.goldenbettingtips.services;

import com.luka.goldenbettingtips.models.entities.Statistics;
import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.repositories.StatisticsRepository;
import com.luka.goldenbettingtips.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StatisticsService {

    private final StatisticsRepository statisticsRepository;
    private final WeekendPackageSettingsService weekendSettingsService;
    private final UserRepository userRepository;

    @Autowired
    public StatisticsService(StatisticsRepository statisticsRepository,
                             WeekendPackageSettingsService weekendSettingsService,
                             UserRepository userRepository) {
        this.statisticsRepository = statisticsRepository;
        this.weekendSettingsService = weekendSettingsService;
        this.userRepository = userRepository;
    }

    public Statistics getStatistics() {
        Optional<Statistics> statistics = statisticsRepository.findById((long)1);
        return statistics.orElse(null);
    }

    public void updateStatistics() {
        Statistics statistics = getStatistics();
        long packagesBought = statistics.getWeekendPackagesBoughtCount();
        double amountEarned = statistics.getWeekendPackagesTotalAmountEarned();
        double currentPrice = weekendSettingsService.getSettings().getWeekendPrice();

        statistics.setWeekendPackagesBoughtCount(packagesBought + 1);
        statistics.setWeekendPackagesTotalAmountEarned(amountEarned + currentPrice);
        statisticsRepository.save(statistics);
    }

    public long getUserCount() {
        return userRepository.count();
    }

}
