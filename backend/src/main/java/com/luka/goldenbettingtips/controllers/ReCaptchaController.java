package com.luka.goldenbettingtips.controllers;

import com.luka.goldenbettingtips.models.pojos.ReCaptchaResult;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/reCaptcha")
public class ReCaptchaController {

    private final String secret = "6Lcl8XkUAAAAADUGcdkaTk8ntNczmdoOOYkJUXKU";
    private final String verifyUrl = "https://www.google.com/recaptcha/api/siteverify";
    private final RestTemplate restTemplate = new RestTemplateBuilder().build();

    @PostMapping
    public void reCaptcha(@RequestParam("action") String action,
                          @RequestParam("token") String token) {

        String url = verifyUrl + "?secret=" + secret + "&response=" + token;
        ReCaptchaResult reCaptchaResult = restTemplate.getForObject(url, ReCaptchaResult.class);
        System.out.println(reCaptchaResult);
        System.out.println(reCaptchaResult.getScore());
        System.out.println(reCaptchaResult.isSuccess());
    }

}
