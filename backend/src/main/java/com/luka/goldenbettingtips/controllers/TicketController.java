package com.luka.goldenbettingtips.controllers;

import com.luka.goldenbettingtips.models.entities.*;
import com.luka.goldenbettingtips.models.enums.Role;
import com.luka.goldenbettingtips.services.TicketService;
import com.luka.goldenbettingtips.services.TicketUserService;
import com.luka.goldenbettingtips.services.TimeService;
import com.luka.goldenbettingtips.services.TipService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/ticket")
public class TicketController {

    private final TicketUserService ticketUserService;
    private final TicketService ticketService;
    private final TimeService timeService;
    private final TipService tipService;

    @Autowired
    public TicketController(TicketUserService ticketUserService,
                            TicketService ticketService,
                            TimeService timeService,
                            TipService tipService) {
        this.ticketUserService = ticketUserService;
        this.ticketService = ticketService;
        this.timeService = timeService;
        this.tipService = tipService;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ADMIN', 'PREMIUM', 'REGULAR')")
    public List<Ticket> getActiveTickets(@AuthenticationPrincipal User user) {
        List<Ticket> tickets = ticketService.getActiveTickets();
        if (user.getUserRole().getRole() == Role.ADMIN || user.getUserRole().getRole() == Role.PREMIUM) {
            return tickets;
        }
        List<Ticket> availableTickets = new ArrayList<>();

        for (Ticket ticket : tickets) {
            if (ticketUserService.existsById(new TicketUserKey(ticket.getId(), user.getId()))) {
                availableTickets.add(ticket);
            } else if (ticketService.isTicketStillBuyable(ticket)) {
                ticket.setTips(null);
                availableTickets.add(ticket);
            }
        }
        return availableTickets;
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity addTicket(@RequestBody Ticket ticket) {
        if (ticket.getTips().isEmpty() || ticket.getPrice() == null
                || ticket.getPrice().compareTo(BigDecimal.ZERO) <= 0
                || ticket.getId() == null || ticket.getOdds() == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        for (Tip tip : ticket.getTips()) {
            if (tip.getTime().minusMinutes(15).isBefore(timeService.getCurrentTimeUTC()) ||
                    tipService.findOne(tip.getId()).getTicketId() != null) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        }
        ticket.setCreated(timeService.getCurrentTimeUTC());
        ticket.setActive(true);
        ticketService.addTicket(ticket);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void deleteTicket(@PathVariable String id) {
        Ticket ticket = ticketService.findById(id);
        if (ticket == null) return;
        ticket.setActive(false);
        ticket.getTips().forEach(tip -> {
            tip.setTicketId(null);
            tipService.saveTip(tip);
        });
        ticketService.addTicket(ticket);
    }

    @GetMapping("/info/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public List<User> ticketBuyers(@PathVariable String id) {
        return ticketUserService.fetchUsers(id);
    }

}
