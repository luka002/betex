package com.luka.goldenbettingtips.controllers;

import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.services.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/paypal")
public class PayPalController {

    private final PayPalService payPalService;
    private final TicketService ticketService;
    private final TicketUserService ticketUserService;
    private final UserDetailsServiceImpl userDetailsService;
    private final WeekendPackageSettingsService settingsService;

    public PayPalController(PayPalService payPalService,
                            TicketService ticketService,
                            TicketUserService ticketUserService,
                            UserDetailsServiceImpl userDetailsService,
                            WeekendPackageSettingsService settingsService) {
        this.payPalService = payPalService;
        this.ticketService = ticketService;
        this.ticketUserService = ticketUserService;
        this.userDetailsService = userDetailsService;
        this.settingsService = settingsService;
    }

    @PostMapping("/become/premium")
    @PreAuthorize("hasAuthority('REGULAR')")
    public Map<String, String> createPayment(@RequestParam("method") String method) {
        if (!settingsService.getSettings().isBuyingEnabled()) {
            return null;
        }
        return payPalService.createBecomePremiumPayment();
    }

    @PostMapping("/buy/ticket")
    @PreAuthorize("hasAuthority('REGULAR')")
    public Map<String, String> buyTicket(@RequestParam("method") String method,
                                         @RequestParam("ticketId") String ticketId,
                                         @AuthenticationPrincipal User user) {
        if (!ticketService.isTicketBuyableByUser(ticketId, user)) return null;
        return payPalService.createTicketPayment(ticketService.findById(ticketId));
    }

    @PostMapping("/execute/ticket/payment")
    @PreAuthorize("hasAuthority('REGULAR')")
    public Map<String, Object> executePayment(@RequestParam("paymentId") String paymentId,
                                              @RequestParam("payerId") String payerId,
                                              @RequestParam("ticketId") String ticketId,
                                              @AuthenticationPrincipal User user) {
        if (!ticketService.isTicketBuyableByUser(ticketId, user)) return null;

        Map<String, Object> payment = payPalService.executePayment(paymentId, payerId);

        if (payment.get("status") != null && payment.get("status").equals("success")) {
            ticketUserService.userBoughtTicket(user, ticketId);
        }
        return payment;
    }

    @PostMapping("/execute/premium/payment")
    @PreAuthorize("hasAuthority('REGULAR')")
    public Map<String, Object> executePayment(@RequestParam("paymentId") String paymentId,
                                              @RequestParam("payerId") String payerId,
                                              @AuthenticationPrincipal User user) {
        if (!settingsService.getSettings().isBuyingEnabled()) {
            return null;
        }
        Map<String, Object> payment = payPalService.executePayment(paymentId, payerId);

        if (payment.get("status") != null && payment.get("status").equals("success")) {
            userDetailsService.userBoughtMonthlyPackage(user);
        }
        return payment;
    }


}
