package com.luka.goldenbettingtips.controllers;

import com.luka.goldenbettingtips.models.entities.Tip;
import com.luka.goldenbettingtips.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tip")
public class TipRestController {
    private final TipService tipService;
    private final TipTypeService tipTypeService;
    private final TipStateService tipStateService;

    @Autowired
    public TipRestController(TipService tipService,
                             TipTypeService tipTypeService,
                             TipStateService tipStateService) {
        this.tipService = tipService;
        this.tipTypeService = tipTypeService;
        this.tipStateService = tipStateService;
    }

    //region Basic CRUD operations ================================================================
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Tip getTip(@PathVariable("id") long id) {
        return tipService.findOne(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Tip createTip(@RequestBody Tip tip) {
        tip.setTipType(tipTypeService.getTypeByEnum(tip.getTipType().getType()));
        tip.setTipState(tipStateService.getTipStateByEnum(tip.getTipState().getState()));
        return tipService.saveTip(tip);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Tip updateTip(@RequestBody Tip tip) {
        Tip _tip = tipService.findOne(tip.getId());
        if (_tip != null) {
            tip.setTicketId(tipService.findOne(tip.getId()).getTicketId());
        }
        tip.setTipType(tipTypeService.getTypeByEnum(tip.getTipType().getType()));
        tip.setTipState(tipStateService.getTipStateByEnum(tip.getTipState().getState()));
        return tipService.saveTip(tip);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteTip(@PathVariable long id) {
        tipService.deleteTip(id);
    }
    //endregion ===================================================================================

    //region Fetching free and premium tips =======================================================
    @GetMapping("/activeFreeTips")
    public List<Tip> getActiveFreeTips() {
        return tipService.getActiveFreeTips();
    }

    @GetMapping("/historyFreeTips")
    public List<Tip> getHistoryFreeTips() {
        return tipService.getHistoryFreeTips();
    }

    @GetMapping("/activePremiumTips")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'PREMIUM')")
    public List<Tip> getActivePremiumTips() {
        return tipService.getActivePremiumTips();
    }

    @GetMapping("/historyPremiumTips")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<Tip> getHistoryPremiumTips() {
        return tipService.getHistoryPremiumTips();
    }
    //endregion ===================================================================================

}
