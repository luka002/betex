package com.luka.goldenbettingtips.controllers;

import com.luka.goldenbettingtips.models.entities.Statistics;
import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.models.entities.VisitorCounter;
import com.luka.goldenbettingtips.services.StatisticsService;
import com.luka.goldenbettingtips.services.VisitorCounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/statistics")
public class StatisticsController {

    private final StatisticsService statisticsService;
    private final VisitorCounterService visitorCounterService;

    @Autowired
    public StatisticsController(StatisticsService statisticsService,
                                VisitorCounterService visitorCounterService) {
        this.statisticsService = statisticsService;
        this.visitorCounterService = visitorCounterService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Statistics getStatistics() {
        return statisticsService.getStatistics();
    }

    @GetMapping("/visit")
    public ResponseEntity visitCounter(@AuthenticationPrincipal User user, HttpServletRequest request) {
//        String remoteAddr = "";
//
//        if (request != null) {
//            remoteAddr = request.getHeader("X-FORWARDED-FOR");
//            if (remoteAddr == null || "".equals(remoteAddr)) {
//                remoteAddr = request.getRemoteAddr();
//            }
//        }
//
//        System.out.println(remoteAddr);
        if (user != null && (user.getUsername().equals("Luka") || user.getUsername().equals("Gustavo"))) {
            return new ResponseEntity(HttpStatus.OK);
        }
        visitorCounterService.increaseCounter();
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/visitor/count")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<VisitorCounter> getVisitorCount() {
        return visitorCounterService.getVisitorCountByDate();
    }

    @GetMapping("/visitor/count/{days}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<VisitorCounter> getVisitorCount(@PathVariable int days) {
        return visitorCounterService.getVisitorCountByDate(days);
    }

    @GetMapping("/getUserCount")
    @PreAuthorize("hasAuthority('ADMIN')")
    public long getUserCount() {
        return statisticsService.getUserCount();
    }

}
