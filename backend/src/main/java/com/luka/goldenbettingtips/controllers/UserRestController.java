package com.luka.goldenbettingtips.controllers;

import com.luka.goldenbettingtips.models.entities.EmailVerificationToken;
import com.luka.goldenbettingtips.models.forms.*;
import com.luka.goldenbettingtips.models.pojos.PageResult;
import com.luka.goldenbettingtips.models.entities.ResetPasswordToken;
import com.luka.goldenbettingtips.models.enums.Role;
import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.services.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/user")
public class UserRestController {

    private final EmailVerificationTokenService emailVerificationTokenService;
    private final UserDetailsServiceImpl userDetailsServiceImpl;
    private final ResetPasswordService resetPasswordService;
    private final TicketUserService ticketUserService;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleService userRoleService;
    private final MailService mailService;
    private final TextTemplates text;

    public UserRestController(EmailVerificationTokenService emailVerificationTokenService,
                              UserDetailsServiceImpl userDetailsServiceImpl,
                              ResetPasswordService resetPasswordService,
                              TicketUserService ticketUserService,
                              PasswordEncoder passwordEncoder,
                              UserRoleService userRoleService,
                              MailService mailService,
                              TextTemplates text) {
        this.emailVerificationTokenService = emailVerificationTokenService;
        this.userDetailsServiceImpl = userDetailsServiceImpl;
        this.resetPasswordService = resetPasswordService;
        this.ticketUserService = ticketUserService;
        this.passwordEncoder = passwordEncoder;
        this.userRoleService = userRoleService;
        this.mailService = mailService;
        this.text = text;
    }

    //region Basic CRUD operations ================================================================
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public User getUserById(@PathVariable("id") long id) {
        return userDetailsServiceImpl.findOne(id);
    }

    @GetMapping("/getCurrentUser")
    public User getCurrentUser(@AuthenticationPrincipal User user) {
        return user;
    }

    @GetMapping("/getUserRole")
    public Map<String, String> getUserRole(@AuthenticationPrincipal User user) {
        return Collections.singletonMap("role", user.getUserRole().getRole().name());
    }

    @GetMapping("/getUserRoleAndUsername")
    public Map<String, String> getUserRoleAndUsername(@AuthenticationPrincipal User user) {
        Map<String, String> roleAndUsername = new HashMap<>();
        String username = user.getSocialName() == null ? user.getUsername() : user.getSocialName();
        roleAndUsername.put("role", user.getUserRole().getRole().name());
        roleAndUsername.put("username", username);

        return roleAndUsername;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<User> getUsers() {
        return userDetailsServiceImpl.getAllUsers();
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity createUserAsAdmin(@Valid @RequestBody AddUserAsAdminForm form,
                                            BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        User user = new User();
        fillUserFromForm(user, form, true);
        userDetailsServiceImpl.saveUser(user);

        return new ResponseEntity(HttpStatus.OK);
    }

    private void fillUserFromForm(User user, AdminFormValues form, boolean usePassword) {
        user.setUsername(form.getUsername());
        if (usePassword) {
            user.setPassword(passwordEncoder.encode(form.getPassword()));
        }
        user.setEmail(form.getEmail());
        user.setUserRole(userRoleService.getRoleByEnum(form.getUserRole().getRole()));
        user.setEnabled(form.getEnabled());
        user.setLastPaymentTime(form.getLastPaymentTime());
        user.setPremiumExpiryTime(form.getPremiumExpiryTime());
        user.setSocialName(form.getSocialName());
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity updateUser(@Valid @RequestBody EditUser form,
                                     BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        User user = userDetailsServiceImpl.findOne(form.getId());
        if (user == null || user.getUsername().equals("admin")) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        fillUserFromForm(user, form, form.getDoValidatePassword());
        userDetailsServiceImpl.saveUser(user);

        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    @Transactional
    public ResponseEntity deleteUser(@PathVariable long id) {
        User user = userDetailsServiceImpl.findOne(id);
        if (user == null || user.getUsername().equals("admin")) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        ticketUserService.deleteByUser(user);
        userDetailsServiceImpl.deleteUser(id);
        return new ResponseEntity(HttpStatus.OK);
    }
    //endregion ===================================================================================

    //region Password manipulation ================================================================
    @PostMapping(value = "/forgotPassword")
    public Map<String, String> forgotPassword(@RequestParam String email) {
        Map<String, String> response = new HashMap<>();
        User user = userDetailsServiceImpl.findByEmail(email);

        if (user == null) {
            response.put("message", text.noUserWithEmail(email));
            response.put("success", "false");
            return response;
        }

        if (user.getSocialName() != null) {
            response.put("message", text.googleAccount());
            response.put("success", "false");
            return response;
        }

        ResetPasswordToken token = resetPasswordService.findByUser(user);
        String newToken = UUID.randomUUID().toString();

        if (token != null && token.isLessThanTwoMinutesOld()) {
            response.put("message", text.twoMinutesNotPassed());
            response.put("success", "false");
            return response;
        } else if (token == null){
            token = new ResetPasswordToken(user, newToken);
        } else {
            token.updateToken(newToken);
        }

        resetPasswordService.save(token);
        mailService.sendMail(email, text.resetSubject(), text.resetPasswordHtml(newToken));

        response.put("message", text.resetMailSent(email));
        response.put("success", "true");
        return response;
    }

    @PostMapping("/resetPassword")
    @Transactional
    public ResponseEntity resetPassword(@Valid @RequestBody ResetPasswordForm form,
                                        BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        String token = form.getToken();
        ResetPasswordToken rpToken = resetPasswordService.findByToken(token);

        if (rpToken == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        User user = rpToken.getUser();
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        userDetailsServiceImpl.saveUser(user);
        resetPasswordService.delete(token);

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/updatePassword")
    public ResponseEntity updatePassword(@Valid @RequestBody UpdatePasswordForm form,
                                         BindingResult result,
                                         @AuthenticationPrincipal User user) {
        if (result.hasErrors()
                || !passwordEncoder.matches(form.getCurrentPassword(), user.getPassword())
                || user.getUsername().equals("admin")) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        user.setPassword(passwordEncoder.encode(form.getPassword()));
        userDetailsServiceImpl.saveUser(user);

        return new ResponseEntity(HttpStatus.OK);
    }
    //endregion ===================================================================================

    //region Registration process =================================================================
    @PostMapping("/register")
    public ResponseEntity registerUser(@Valid @RequestBody RegistrationForm form,
                                       BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        User user = new User();
        user.setUsername(form.getUsername());
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        user.setEmail(form.getEmail());
        user.setUserRole(userRoleService.getRoleByEnum(Role.REGULAR));
        user.setEnabled(false);
        userDetailsServiceImpl.saveUser(user);

        String token = UUID.randomUUID().toString();
        EmailVerificationToken evToken = new EmailVerificationToken(user, token);
        emailVerificationTokenService.save(evToken);

        mailService.sendMail(
                user.getEmail(),
                text.accountActivationSubject(),
                text.activateAccountHtml(token)
        );

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/activateUserAccount")
    @Transactional
    public ResponseEntity activateUserAccount(@RequestParam("token") String token) {
        EmailVerificationToken evToken = emailVerificationTokenService.findByToken(token);

        if (evToken == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        User user = evToken.getUser();
        user.setEnabled(true);
        userDetailsServiceImpl.saveUser(user);
        emailVerificationTokenService.deleteToken(token);

        return new ResponseEntity(HttpStatus.OK);
    }
    //endregion ===================================================================================

    //region Pagination ===========================================================================
    @GetMapping("/getUsersWithUsername")
    @PreAuthorize("hasAuthority('ADMIN')")
    public PageResult getUsersWithUsername(@RequestParam("page") int page,
                                           @RequestParam("username") String username) {
        return userDetailsServiceImpl.getPageWithUsers(page, username);
    }
    //endregion ===================================================================================

    //region Checking =============================================================================
    @GetMapping("/isUserLoggedIn")
    @PreAuthorize("hasAnyAuthority('REGULAR', 'ADMIN', 'PREMIUM')")
    public ResponseEntity isUserLoggedIn() {
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/isUsernameUnique")
    public boolean isUsernameUnique(@RequestParam("username") String username) {
        return !userDetailsServiceImpl.isUsernameTaken(username);
    }

    @GetMapping("/isEmailUnique")
    public boolean isEmailUnique(@RequestParam("email") String email) {
        return !userDetailsServiceImpl.isEmailTaken(email);
    }

    @GetMapping("/isEmailUniqueOrFromUser")
    public boolean isEmailUniqueOrFormUser(@RequestParam("email") String email,
                                           @RequestParam("id") Long id) {
        if (userDetailsServiceImpl.isEmailTaken(email)) {
            User user = userDetailsServiceImpl.findOne(id);
            if (user == null) {
                return true;
            }
            return user.getEmail().equals(email);
        }

        return true;
    }
    //endregion ===================================================================================

}
