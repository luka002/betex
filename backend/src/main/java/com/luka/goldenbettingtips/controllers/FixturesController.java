package com.luka.goldenbettingtips.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/fixtures")
public class FixturesController {
    RestTemplate restTemplate;
    @Value("${algorithm.url}")
    String algorithmUrl;

    @Autowired
    public FixturesController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/{startDate}/{endDate}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> getFixturesByDate(@PathVariable String startDate,
                                                    @PathVariable String endDate) {
        return restTemplate.getForEntity(algorithmUrl + "/fixtures/" + startDate + "/" + endDate, Object.class);
    }

    @PostMapping("/{startDate}/{endDate}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> saveFixturesByDate(@PathVariable String startDate,
                                                     @PathVariable String endDate) {
        return restTemplate.postForEntity(algorithmUrl + "/fixtures/" + startDate + "/" + endDate, null, Object.class);
    }

    @PutMapping("/{startDate}/{endDate}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> updateFixturesByDate(@PathVariable String startDate,
                                                       @PathVariable String endDate) {
        restTemplate.put(algorithmUrl + "/fixtures/" + startDate + "/" + endDate, null);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/forebet/{startDate}/{endDate}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> updateForebetByDate(@PathVariable String startDate,
                                                      @PathVariable String endDate) {
        restTemplate.put(algorithmUrl + "/fixtures/forebet/" + startDate + "/" + endDate, null);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/odds/{startDate}/{endDate}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> updateOddsByDate(@PathVariable String startDate,
                                                   @PathVariable String endDate) {
        restTemplate.put(algorithmUrl + "/fixtures/odds/" + startDate + "/" + endDate, null);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/analyze")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> analyze(@RequestBody Object body) {
        return restTemplate.postForEntity(algorithmUrl + "/fixtures/analyze", body, Object.class);
    }

    @PostMapping("/predict")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> predict(@RequestBody Object body) {
        return restTemplate.postForEntity(algorithmUrl + "/fixtures/predict", body, Object.class);
    }
}
