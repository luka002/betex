package com.luka.goldenbettingtips.controllers;

import com.luka.goldenbettingtips.models.entities.WeekendPackageSettings;
import com.luka.goldenbettingtips.services.WeekendPackageSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/settings")
public class SettingsController {

    private final WeekendPackageSettingsService weekendSettingsService;

    @Autowired
    public SettingsController(WeekendPackageSettingsService weekendSettingsService) {
        this.weekendSettingsService = weekendSettingsService;
    }

    @GetMapping
    public WeekendPackageSettings getSettings() {
        return weekendSettingsService.getSettings();
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity updateSettings(@RequestBody WeekendPackageSettings settings) {
        settings.setId(1);
        WeekendPackageSettings newSettings = weekendSettingsService.updateSettings(settings);

        if (newSettings == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

}
