package com.luka.goldenbettingtips.controllers;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.luka.goldenbettingtips.models.entities.User;
import com.luka.goldenbettingtips.models.enums.Role;
import com.luka.goldenbettingtips.services.SocialAuthService;
import com.luka.goldenbettingtips.services.UserDetailsServiceImpl;
import com.luka.goldenbettingtips.services.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/social/auth")
public class SocialAuthController {

    private final SocialAuthService socialAuthService;
    private final UserDetailsServiceImpl userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleService userRoleService;

    @Autowired
    public SocialAuthController(SocialAuthService socialAuthService,
                                UserDetailsServiceImpl userDetailsService,
                                PasswordEncoder passwordEncoder,
                                UserRoleService userRoleService) {
        this.socialAuthService = socialAuthService;
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.userRoleService = userRoleService;
    }

    //region Google signin and signup =================================================================================
    @PostMapping("/google/signup")
    public ResponseEntity googleSignup(@RequestParam("idToken") String idTokenString) {
        GoogleIdToken idToken;
        try {
            idToken = socialAuthService.getVerifier().verify(idTokenString);
        } catch (GeneralSecurityException | IOException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        if (createGoogleUser(idToken) == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/google/signin")
    public ResponseEntity googleSingin(@RequestParam("idToken") String idTokenString) {
        String username, password;
        try {
            GoogleIdToken idToken = socialAuthService.getVerifier().verify(idTokenString);
            username = idToken.getPayload().getSubject() + socialAuthService.getUsernameSuffix();
            password = idToken.getPayload().getSubject() + socialAuthService.getPasswordSuffix();
            if (!userDetailsService.isUsernameTaken(username)) {
                if (createGoogleUser(idToken) == null) {
                    return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }
            }
        } catch (GeneralSecurityException | IOException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        signInUser(username, password);
        return new ResponseEntity(HttpStatus.OK);
    }
    //endregion========================================================================================================

    //region Facebook signup and signin ===============================================================================
    @PostMapping("/facebook/signup")
    public ResponseEntity facebookSignup(@RequestParam("authToken") String authToken) {
        if (createFacebookUser(authToken) == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/facebook/signin")
    public ResponseEntity facebookSingin(@RequestParam("authToken") String authToken) {
        org.springframework.social.facebook.api.User fbUser = getFacebookUser(authToken);
        String password = fbUser.getId() + socialAuthService.getPasswordSuffix();
        String username = fbUser.getId() + socialAuthService.getUsernameSuffix();

        if (!userDetailsService.isUsernameTaken(username)) {
            if (createFacebookUser(authToken) == null) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        }

        signInUser(username, password);
        return new ResponseEntity(HttpStatus.OK);
    }
    //endregion =======================================================================================================

    //region Helper methods ===========================================================================================
    private User createGoogleUser(GoogleIdToken idToken) {
        GoogleIdToken.Payload payload = idToken.getPayload();
        String userId = payload.getSubject();
        String email = payload.getEmail();
        String name = (String) payload.get("name");

        if (!payload.getEmailVerified() ||
                userDetailsService.isUsernameTaken(userId + socialAuthService.getUsernameSuffix()) ||
                userDetailsService.isEmailTaken(email)) {
            return null;
        }

        String username = userId + socialAuthService.getUsernameSuffix();
        String password = userId + socialAuthService.getPasswordSuffix();
        User user = generateUser(username, password, email, name);
        return userDetailsService.saveUser(user);
    }

    private User createFacebookUser(String authToken) {
        org.springframework.social.facebook.api.User fbUser = getFacebookUser(authToken);
        String username = fbUser.getId() + socialAuthService.getUsernameSuffix();
        String password = fbUser.getId() + socialAuthService.getPasswordSuffix();

        if (userDetailsService.isUsernameTaken(username) ||
                userDetailsService.isEmailTaken(fbUser.getEmail())) {
            return null;
        }

        User user = generateUser(username, password, fbUser.getEmail(), fbUser.getName());
        return userDetailsService.saveUser(user);
    }

    private User generateUser(String username, String password, String email, String name) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.setEmail(email);
        user.setUserRole(userRoleService.getRoleByEnum(Role.REGULAR));
        user.setEnabled(true);
        user.setSocialName(name);
        return user;
    }


    private org.springframework.social.facebook.api.User getFacebookUser(String authToken) {
        Facebook facebook = new FacebookTemplate(authToken);
        String [] fields = { "id", "email",  "name" };
        return facebook.fetchObject(
                "me", org.springframework.social.facebook.api.User.class, fields
        );
    }


    private void signInUser(String username, String password) {
        User user = userDetailsService.findByUsername(username);
        Set<GrantedAuthority> authorities = new HashSet<>(user.getAuthorities());
        Authentication newAuth = new UsernamePasswordAuthenticationToken(user, password, authorities);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }
    //endregion =======================================================================================================

}
