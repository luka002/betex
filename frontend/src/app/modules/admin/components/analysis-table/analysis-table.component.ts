import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AnalysisFilter, AnalysisResult} from '../../../core/models';
import {MatTableDataSource} from '@angular/material/table';
import {DaterangepickerDirective} from 'ngx-daterangepicker-material';
import {FixturesService} from '../../../core/services';

@Component({
  selector: 'betex-analysis-table',
  templateUrl: './analysis-table.component.html',
  styleUrls: ['./analysis-table.component.scss']
})
export class AnalysisTableComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(DaterangepickerDirective, {static: false}) pickerDirective: DaterangepickerDirective;

  analysisDataSource = new MatTableDataSource<AnalysisResult>();
  displayedColumns: string[] = [
    'spread', 'profit', 'totalGames', 'gamesWon', 'gamesLost', 'winRate', 'roi', 'averageOdds'
  ];
  isLoading = false;
  emptyState = true;

  constructor(private fixturesService: FixturesService) {}

  ngOnInit() {
    this.analysisDataSource.paginator = this.paginator;
    this.analysisDataSource.sort = this.sort;
  }

  applyFilter(filter: AnalysisFilter) {
    this.isLoading = true;
    this.fixturesService
      .analyzeFixtures(filter)
      .subscribe(result => {
        if (result) {
          this.analysisDataSource.data = result;
          this.isLoading = false;
          this.emptyState = false;
        } else {
          this.analysisDataSource.data = null;
          this.isLoading = false;
          this.emptyState = true;
        }
      }, () => {
        this.analysisDataSource.data = null;
        this.isLoading = false;
        this.emptyState = true;
      });
  }

}
