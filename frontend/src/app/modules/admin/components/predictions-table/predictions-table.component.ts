import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {DaterangepickerDirective} from 'ngx-daterangepicker-material';
import {MatTableDataSource} from '@angular/material/table';
import {AnalysisFilter, PredictionResult} from '../../../core/models';
import {FixturesService} from '../../../core/services';

@Component({
  selector: 'betex-predictions-table',
  templateUrl: './predictions-table.component.html',
  styleUrls: ['./predictions-table.component.scss']
})
export class PredictionsTableComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(DaterangepickerDirective, {static: false}) pickerDirective: DaterangepickerDirective;

  @Output() saveFixture = new EventEmitter<PredictionResult>();

  predictionDataSource = new MatTableDataSource<PredictionResult>();
  predictions: PredictionResult[];
  displayedColumns: string[] = [
    'spread', 'Date', 'eventDateTime', 'homeTeamName', 'score', 'awayTeamName', 'leagueName',
    'leagueCountry', 'status', 'forebetResult', 'forebetTotalGoals', 'odds', 'add'
  ];
  isLoading = false;
  emptyState = true;

  constructor(private fixturesService: FixturesService) {}

  ngOnInit() {
    this.predictionDataSource.paginator = this.paginator;
    this.predictionDataSource.sort = this.sort;
  }

  addFixtureToTips(fixture: PredictionResult) {
    this.saveFixture.emit(fixture);
  }

  applyFilter(filter: AnalysisFilter) {
    this.isLoading = true;
    this.fixturesService
      .predictFixtures(filter)
      .subscribe(result => {
        if (result) {
          this.predictions = result;
          this.predictionDataSource.data = result
            .map(fixture => ({...fixture, odds: fixture.bets.over25, betType: 'Over 2.5'}));
          this.isLoading = false;
          this.emptyState = false;
        } else {
          this.predictionDataSource.data = null;
          this.isLoading = false;
          this.emptyState = true;
        }
      }, () => {
        this.predictionDataSource.data = null;
        this.isLoading = false;
        this.emptyState = true;
      });
  }

  oddsChanged(odds: string) {
    if (!this.predictions) return;
    this.predictionDataSource.data = this.predictions.map(fixture => {
      if (odds === 'over15') return {...fixture, odds: fixture.bets.over15, betType: 'Over 1.5'};
      if (odds === 'under15') return {...fixture, odds: fixture.bets.under15, betType: 'Under 1.5'};
      if (odds === 'over25') return {...fixture, odds: fixture.bets.over25, betType: 'Over 2.5'};
      if (odds === 'under25') return {...fixture, odds: fixture.bets.under25, betType: 'Under 2.5'};
      if (odds === 'over35') return {...fixture, odds: fixture.bets.over35, betType: 'Over 3.5'};
      if (odds === 'under35') return {...fixture, odds: fixture.bets.under35, betType: 'Under 3.5'};
      if (odds === 'yesBts') return {...fixture, odds: fixture.bets.yesBts, betType: 'BTS'};
      if (odds === 'noBts') return {...fixture, odds: fixture.bets.noBts, betType: 'No BTS'};
    });
  }

}
