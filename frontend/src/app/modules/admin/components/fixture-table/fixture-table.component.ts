import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Fixture} from '../../../core/models';
import {MatSelectChange} from '@angular/material/select';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {DaterangepickerDirective} from 'ngx-daterangepicker-material';
import moment, {Moment} from 'moment';
import {catchError, map, switchMap} from 'rxjs/operators';
import {FixturesService} from '../../../core/services';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'betex-fixture-table',
  templateUrl: './fixture-table.component.html',
  styleUrls: ['./fixture-table.component.scss']
})
export class FixtureTableComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(DaterangepickerDirective, {static: false}) pickerDirective: DaterangepickerDirective;

  selected: {startDate: Moment, endDate: Moment} = {startDate: moment().add(0, 'days'), endDate: moment().add(0, 'days')};
  ranges: any = {
    'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  };
  fixturesDataSource = new MatTableDataSource<Fixture>();
  fixtures: Fixture[] = [];
  displayedColumns: string[] = [
    'Date', 'eventDateTime', 'homeTeamName', 'score', 'awayTeamName', 'leagueName',
    'leagueCountry', 'status', 'forebetResult', 'forebetTotalGoals', 'odds'
  ];
  odds = 'over25';
  total: number;
  notStarted: number;
  finished: number;
  other: number;
  forebet: number;
  modalRef: BsModalRef;
  isLoadingFixtures: boolean;

  constructor(private modalService: BsModalService,
              private fixturesService: FixturesService) {}

  ngOnInit() {
    this.fixturesDataSource.paginator = this.paginator;
    this.fixturesDataSource.sort = this.sort;
  }

  getFixtures(date: {startDate: Moment, endDate: Moment}): Observable<any> {
    this.isLoadingFixtures = true;
    return this.fixturesService
      .getFixturesByDate(this.getDateStrings(date))
      .pipe(
        map(fixtures => {
          this.fixtures = fixtures;
          this.refreshDetails();
          this.fixturesDataSource.data = this.fixtures.map(fixture => ({...fixture, odds: fixture.bets.over25}));
          this.odds = 'over25';
          this.isLoadingFixtures = false;
        }),
        catchError(() => {
          this.isLoadingFixtures = false;
          return of({});
        })
      );
  }

  apply() {
    this.getFixtures(this.selected).subscribe();
  }

  saveFixtures() {
    this.isLoadingFixtures = true;
    this.fixturesService
      .saveFixturesByDate(this.getDateStrings(this.selected))
      .pipe(switchMap(res => this.getFixtures(this.selected)))
      .subscribe(() => this.isLoadingFixtures = false, () => this.isLoadingFixtures = false);
  }

  updateFixtures() {
    this.isLoadingFixtures = true;
    this.fixturesService
      .updateFixturesByDate(this.getDateStrings(this.selected))
      .pipe(switchMap(res => this.getFixtures(this.selected)))
      .subscribe(() => this.isLoadingFixtures = false, () => this.isLoadingFixtures = false);
  }

  updateForebet() {
    this.isLoadingFixtures = true;
    this.fixturesService
      .updateForebetByDate(this.getDateStrings(this.selected))
      .pipe(switchMap(res => this.getFixtures(this.selected)))
      .subscribe(() => this.isLoadingFixtures = false, () => this.isLoadingFixtures = false);
  }

  updateOdds() {
    this.isLoadingFixtures = true;
    this.fixturesService
      .updateOddsByDate(this.getDateStrings(this.selected))
      .pipe(switchMap(res => this.getFixtures(this.selected)))
      .subscribe(() => this.isLoadingFixtures = false, () => this.isLoadingFixtures = false);
  }

  open(event) {
    this.pickerDirective.open(event);
  }

  getDateStrings(date: {startDate: Moment, endDate: Moment}): {start: string, end: string} {
    const start = date.startDate.format('YYYY-MM-DD');
    const end = date.endDate.format('YYYY-MM-DD');
    return {start: start, end: end};
  }

  refreshDetails() {
    this.total = this.fixtures.length;
    this.notStarted = this.numberByStatus('Not Started');
    this.finished = this.numberByStatus('Match Finished');
    this.other = this.otherStatusCount();
    this.forebet = this.fixtures.reduce((prev, current) => {
      return current.forebetResult ? (prev + 1) : prev;
    }, 0);
  }

  numberByStatus(status: string) {
    return this.fixtures.reduce((prev, current) => {
      return current.status === status ? (prev + 1) : prev;
    }, 0);
  }

  otherStatusCount() {
    return this.fixtures.reduce((prev, current) => {
      if (current.status !== 'Not Started' && current.status !== 'Match Finished') {
        return prev + 1;
      }
      return prev;
    }, 0);
  }

  oddsChanged(select: MatSelectChange) {
    this.fixturesDataSource.data = this.fixtures.map(fixture => {
      if (select.value === 'over15') return {...fixture, odds: fixture.bets.over15};
      if (select.value === 'under15') return {...fixture, odds: fixture.bets.under15};
      if (select.value === 'over25') return {...fixture, odds: fixture.bets.over25};
      if (select.value === 'under25') return {...fixture, odds: fixture.bets.under25};
      if (select.value === 'over35') return {...fixture, odds: fixture.bets.over35};
      if (select.value === 'under35') return {...fixture, odds: fixture.bets.under35};
      if (select.value === 'yesBts') return {...fixture, odds: fixture.bets.yesBts};
      if (select.value === 'noBts') return {...fixture, odds: fixture.bets.noBts};
    });
  }

  openModal(template) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

}
