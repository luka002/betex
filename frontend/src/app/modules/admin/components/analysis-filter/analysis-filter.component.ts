import {Component, EventEmitter, Input, Output} from '@angular/core';
import moment, {Moment} from 'moment';
import {AnalysisFilter, BetType, Forebet, Spread} from '../../../core/models';
import {MatSelectChange} from '@angular/material/select';

@Component({
  selector: 'betex-analysis-filter',
  templateUrl: './analysis-filter.component.html',
  styleUrls: ['./analysis-filter.component.scss']
})
export class AnalysisFilterComponent {
  @Input() isAnalysis: boolean;
  @Output() applyFilter = new EventEmitter<AnalysisFilter>();
  @Output() oddsChanged = new EventEmitter<string>();

  ranges: any = {
    'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  };
  selected: {startDate: Moment, endDate: Moment} = {startDate: moment().add(0, 'days'), endDate: moment().add(0, 'days')};
  analysisType = 'over25';
  lastH2H = '5';
  lastHome = '13';
  lastAway = '13';
  minOdds = 1.20;
  maxOdds = 1.85;
  betType = 'over25';
  minBetOdds = 1;
  maxBetOdds = 5;
  customSpread = false;
  minSpread = -0.10;
  maxSpread = 0.10;
  useForebet = true;
  minForebet = 3;
  maxForebet = 12;
  gameOdds = 'over25';

  apply() {
    const filter = new AnalysisFilter();
    filter.analysisType = this.analysisType;
    filter.startDate = this.selected.startDate.format('YYYY-MM-DD');
    filter.endDate = this.selected.endDate.format('YYYY-MM-DD');
    filter.lastH2H = +this.lastH2H;
    filter.lastHome = +this.lastHome;
    filter.lastAway = +this.lastAway;
    filter.minOdds = this.minOdds;
    filter.maxOdds = this.maxOdds;
    filter.isAnalysis = this.isAnalysis;
    filter.betType = new BetType(this.betType, this.minBetOdds, this.maxBetOdds);
    if (this.customSpread) {
      filter.spread = new Spread(this.minSpread, this.maxSpread);
    } else {
      filter.spread = null;
    }
    if (this.useForebet) {
      filter.forebet = new Forebet(this.minForebet, this.maxForebet);
    } else {
      filter.forebet = null;
    }

    this.applyFilter.emit(filter);
    this.gameOdds = 'over25';
  }

  onOddsChanged(select: MatSelectChange) {
    this.oddsChanged.emit(select.value);
  }

}
