import {NgModule} from '@angular/core';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {SharedModule} from '../shared/shared.module';
import {AdminRoutingModule} from './admin-routing.module';
import {
  TipComponent,
  FixtureTableComponent,
  AnalysisTableComponent,
  AnalysisFilterComponent,
  PredictionsTableComponent,
  ManageUsersTableComponent,
  ManageUsersPaginationComponent
} from './components';
import {
  AddUserComponent,
  EditTipComponent,
  EditUserComponent,
  HistoryComponent,
  ManageUsersComponent,
  NewTipComponent,
  SettingsComponent,
  FixturesComponent
} from './pages';
import {
  EditTipResolver,
  EditUserResolver,
  HistoryResolver,
  ManageUsersResolver,
  StatisticsResolver
} from './resolvers';

@NgModule({
  declarations: [
    TipComponent,
    AddUserComponent,
    EditTipComponent,
    EditUserComponent,
    HistoryComponent,
    ManageUsersComponent,
    NewTipComponent,
    SettingsComponent,
    ManageUsersTableComponent,
    ManageUsersPaginationComponent,
    FixturesComponent,
    FixtureTableComponent,
    AnalysisTableComponent,
    AnalysisFilterComponent,
    PredictionsTableComponent
  ],
  imports: [
    SharedModule,
    AdminRoutingModule,
    MatTabsModule,
    NgxDaterangepickerMd.forRoot(),
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    MatTableModule,
    MatSelectModule,
    MatProgressSpinnerModule
  ],
  providers: [
    EditTipResolver,
    EditUserResolver,
    HistoryResolver,
    ManageUsersResolver,
    StatisticsResolver
  ]
})
export class AdminModule { }
