import {Component, TemplateRef} from '@angular/core';
import {PredictionResult, Tip} from '../../../core/models';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ProgressBarService, TipService} from '../../../core/services';
import moment from 'moment';

@Component({
  selector: 'betex-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.scss']
})
export class FixturesComponent {
  tip = new Tip();
  loading = false;
  saveSuccess = false;
  modalRef: BsModalRef;

  constructor(private modalService: BsModalService,
              private tipService: TipService,
              private progressBarService: ProgressBarService) {}

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  saveFixture(fixture: PredictionResult) {
    this.tip.teamHome = fixture.homeTeamName;
    this.tip.teamAway = fixture.awayTeamName;
    this.tip.time = moment(fixture.eventDateTime).utc().format(moment.HTML5_FMT.DATETIME_LOCAL);
    this.tip.league = fixture.leagueName;
    this.tip.odds = fixture.odds.toFixed(2);
    this.tip.bet = fixture.betType;
  }

  saveTip(tip: Tip) {
    this.saveSuccess = false;
    this.loading = true;
    this.progressBarService.requestStart();

    this.tipService.saveTip(tip).subscribe(
      (_tip) => {
        this.saveSuccess = _tip != null;
        this.loading = false;
        this.tip = new Tip();
        this.progressBarService.requestDone();
        this.modalRef.hide();
      },
      () => {
        this.saveSuccess = false;
        this.loading = false;
        this.progressBarService.requestDone();
      }
    );
  }
}
