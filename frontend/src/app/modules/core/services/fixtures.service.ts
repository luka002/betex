import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {httpOptions, url} from '../properties/properties';
import {Observable} from 'rxjs';
import {AnalysisFilter, AnalysisResult, PredictionResult} from '../models';

@Injectable({
  providedIn: 'root'
})
export class FixturesService {

  constructor(private http: HttpClient) {}

  getFixturesByDate(date: {start: string, end: string}): Observable<any> {
    return this.http.get(url.getFixturesByDate(date.start, date.end), httpOptions.json);
  }

  saveFixturesByDate(date: {start: string, end: string}): Observable<any> {
    return this.http.post(url.saveFixturesByDate(date.start, date.end), null, httpOptions.json);
  }

  updateFixturesByDate(date: {start: string, end: string}): Observable<any> {
    return this.http.put(url.updateFixturesByDate(date.start, date.end), null, httpOptions.json);
  }

  updateForebetByDate(date: {start: string, end: string}): Observable<any> {
    return this.http.put(url.updateForebetByDate(date.start, date.end), null, httpOptions.json);
  }

  updateOddsByDate(date: {start: string, end: string}): Observable<any> {
    return this.http.put(url.updateOddsByDate(date.start, date.end), null, httpOptions.json);
  }

  analyzeFixtures(analysisFilter: AnalysisFilter): Observable<AnalysisResult[]> {
    return this.http.post<AnalysisResult[]>(url.analyzeFixtures, analysisFilter, httpOptions.json);
  }

  predictFixtures(analysisFilter: AnalysisFilter): Observable<PredictionResult[]> {
    return this.http.post<PredictionResult[]>(url.analyzeFixtures, analysisFilter, httpOptions.json);
  }

}
