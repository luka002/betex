export class Fixture {
  fixtureId: string;
  homeTeamName: string;
  awayTeamName: string;
  score: string;
  eventDateTime: Date;
  leagueName: string;
  leagueCountry: string;
  analyzedBeforeStart: boolean;
  status: string;
  forebetResult: string;
  forebetTotalGoals: number;
  bets: {
    yesBts: number,
    noBts: number,
    over15: number,
    under15: number,
    over25: number,
    under25: number,
    over35: number,
    under35: number
  };
  odds: number;
  betType: string;
}
