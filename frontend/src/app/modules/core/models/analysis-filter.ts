export class AnalysisFilter {
  analysisType: string;
  startDate: string;
  endDate: string;
  lastH2H: number;
  lastHome: number;
  lastAway: number;
  minOdds: number;
  maxOdds: number;
  betType: BetType;
  isAnalysis: boolean;
  spread: Spread;
  forebet: Forebet;
}

export class BetType {
  constructor(public betType: string, public minBetOdds: number, public maxBetOdds: number) {}
}

export class Spread {
  constructor(public bottom: number, public top: number) {}
}

export class Forebet {
  constructor(public min: number, public max: number) {}
}
