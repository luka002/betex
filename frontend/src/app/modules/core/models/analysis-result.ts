export class AnalysisResult {
  spread: string;
  profit: number;
  totalGames: number;
  gamesWon: number;
  gamesLost: number;
  winRate: string;
  roi: string;
  averageOdds: number;
}
