// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backendRoot: 'http://localhost:8080',
  // backendRoot: 'http://192.168.0.24:8080',
  googleLoginProviderID: '715271956253-eflv77la8kfibanj4sjjt0mic01ivhub.apps.googleusercontent.com',
  facebookLoginProviderID: '288171098797831'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
