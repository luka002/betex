const {getDb} = require('./db-connection');

function getCollection() {
  const db = getDb();
  return db.collection('fixtures');
}

exports.saveFixture = (fixture) => {
  return getCollection().insertOne(fixture);
};

exports.updateForebet = (fixture) => {
  return getCollection().updateOne(
    {fixtureId: fixture.fixtureId},
    {$set: {forebetResult: fixture.forebetResult}}
  )
};

exports.updateBetsByFixtureId = (fixtureId, bets) => {
  return getCollection().updateOne(
    {fixtureId: fixtureId},
    {$set: {bets: bets}}
  )
};

exports.findByFixtureId = (fixtureId) => {
  return getCollection().findOne({fixtureId: fixtureId})
};

exports.findByFilter = (filter) => {
  return getCollection().find(filter).toArray();
};

exports.updateFixtureResult = (fixture) => {
  return getCollection().updateOne(
    {fixtureId: fixture.fixtureId},
    {$set: {
      status: fixture.status,
      homeTeamGoals: fixture.homeTeamGoals,
      awayTeamGoals: fixture.awayTeamGoals,
      goals: fixture.goals,
      isBts: fixture.isBts
    }}
  )
};
