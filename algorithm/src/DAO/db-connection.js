const MongoClient = require('mongodb').MongoClient;

let _db;
const uri = "mongodb+srv://luka_002:1595Lg15@betex-algorithm-wvzw4.mongodb.net/betex?retryWrites=true&w=majority";

const mongoConnect = (callback) => {
  const client = new MongoClient(uri, {useUnifiedTopology: true, useNewUrlParser: true});
  return client.connect(err => {
    if (!err) {
      _db = client.db();
      // _db.collection('fixtures').deleteMany({}).then();
      callback()
    } else {
      throw err;
    }
  });
};

const getDb = () => {
  if (_db) {
    return _db;
  }
  throw 'No database found!';
};

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;
