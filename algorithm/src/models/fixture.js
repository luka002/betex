const moment = require('moment');
const leagues = require('../constants/leagues');
const leaguesPro = require('../constants/leagues-pro');

module.exports = class Fixture {
  constructor(fixture) {
    this.fixtureId = fixture['fixture_id'];
    this.homeTeamId = fixture.homeTeam['team_id'];
    this.homeTeamName = fixture.homeTeam['team_name'];
    this.awayTeamId = fixture.awayTeam['team_id'];
    this.awayTeamName = fixture.awayTeam['team_name'];
    this.eventDateTime = fixture['event_date'];
    this.eventDate = fixture['event_date'].substring(0, 10);
    this.leagueId = fixture['league_id'];
    this.leagueName = fixture.league.name;
    this.leagueCountry = fixture.league.country;
    this.analyzedBeforeStart = fixture.statusShort === 'NS';
    this.status = fixture.statusShort;
    if (fixture.statusShort === 'FT') {
      this.homeTeamGoals = fixture.goalsHomeTeam;
      this.awayTeamGoals = fixture.goalsAwayTeam;
      this.goals = fixture.goalsHomeTeam + fixture.goalsAwayTeam;
      this.isBts = fixture.goalsHomeTeam > 0 && fixture.goalsAwayTeam > 0;
    }
  }

  isLeagueValid() {
    return !!leagues.find(league => {
      return league.name === this.leagueName && league.country === this.leagueCountry;
    });
  }

  isProLeagueValid() {
    return !!leaguesPro.find(league => {
      return league.name === this.leagueName && league.country === this.leagueCountry;
    });
  }

  isGameAnalyzable(game) {
    const gameDate = moment(game['event_date']);
    return gameDate.isBefore(this.eventDateTime) &&
      (this.homeTeamId === game.homeTeam['team_id'] || this.awayTeamId === game.awayTeam['team_id']) &&
      (this.leagueName === game.league.name && this.leagueCountry === game.league.country);
  }
};
