exports.getFixturesByDate = {
  "api": {
    "results": 1,
    "fixtures": [
      {
        "fixture_id": 157232,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-10T20:00:00+00:00",
        "event_timestamp": 1578686400,
        "firstHalfStart": 1578686400,
        "secondHalfStart": 1578690000,
        "round": "Regular Season - 22",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane, Sheffield",
        "referee": "Michael Oliver, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-0",
          "extratime": null,
          "penalty": null
        }
      }
    ]
  }
};

exports.getOddsByFixtureId = {
  "api":{
    "results":1,
    "odds":[
      {
        "fixture":{
          "league_id":524,
          "fixture_id":157232,
          "updateAt":1578679225
        },
        "bookmakers":[
          {
            "bookmaker_id":6,
            "bookmaker_name":"Bwin",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.00"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.40"
                  },
                  {
                    "value":"Away",
                    "odd":"4.00"
                  }
                ]
              },
              {
                "label_id":2,
                "label_name":"Home\/Away",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.40"
                  },
                  {
                    "value":"Away",
                    "odd":"2.70"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.30"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.40"
                  },
                  {
                    "value":"Away",
                    "odd":"3.90"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.40"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.28"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.10"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"6.25"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.09"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.75"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"7.50"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"10.50"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.02"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"16.50"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.90"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.36"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"7.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.42"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.65"
                  }
                ]
              },
              {
                "label_id":26,
                "label_name":"Goals Over\/Under - Second Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"9.50"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.03"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.15"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.62"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.60"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.16"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.26"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"3.50"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"14.50"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"41.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"8.25"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"4.75"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.20"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"5.00"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"26.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.83"
                  },
                  {
                    "value":"No",
                    "odd":"1.87"
                  }
                ]
              },
              {
                "label_id":29,
                "label_name":"Win to Nil - Home",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.20"
                  },
                  {
                    "value":"No",
                    "odd":"1.30"
                  }
                ]
              },
              {
                "label_id":30,
                "label_name":"Win to Nil - Away",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"5.75"
                  },
                  {
                    "value":"No",
                    "odd":"1.10"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.60"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.78"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.70"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.25"
                  },
                  {
                    "value":"Away +1",
                    "odd":"8.50"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.25"
                  },
                  {
                    "value":"Home +2",
                    "odd":"1.05"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"9.75"
                  },
                  {
                    "value":"Away +2",
                    "odd":"17.50"
                  },
                  {
                    "value":"Home -2",
                    "odd":"7.50"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"5.75"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.25"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"7.25"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.75"
                  },
                  {
                    "value":"2:1",
                    "odd":"9.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"16.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"16.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"23.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"34.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"31.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"51.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"101.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"7.50"
                  },
                  {
                    "value":"1:1",
                    "odd":"7.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"15.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"51.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"11.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"19.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"41.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"67.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"13.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"31.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"101.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"36.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"126.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"151.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.25"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.25"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.78"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.60"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.05"
                  },
                  {
                    "value":"Away",
                    "odd":"4.25"
                  }
                ]
              },
              {
                "label_id":14,
                "label_name":"Team To Score First",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.67"
                  },
                  {
                    "value":"Draw",
                    "odd":"7.50"
                  },
                  {
                    "value":"Away",
                    "odd":"2.45"
                  }
                ]
              },
              {
                "label_id":15,
                "label_name":"Team To Score Last",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.67"
                  },
                  {
                    "value":"Away",
                    "odd":"2.45"
                  },
                  {
                    "value":"No goal",
                    "odd":"7.50"
                  }
                ]
              },
              {
                "label_id":32,
                "label_name":"Win Both Halves",
                "values":[
                  {
                    "value":"Home",
                    "odd":"5.50"
                  },
                  {
                    "value":"Draw",
                    "odd":"1.17"
                  },
                  {
                    "value":"Away",
                    "odd":"14.00"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"2.05"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.70"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"3.25"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.30"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.15"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.62"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.40"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"4.50"
                  },
                  {
                    "value":"No",
                    "odd":"1.17"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.90"
                  },
                  {
                    "value":"Even",
                    "odd":"1.80"
                  }
                ]
              },
              {
                "label_id":22,
                "label_name":"Odd\/Even - First Half",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.10"
                  },
                  {
                    "value":"Even",
                    "odd":"1.65"
                  }
                ]
              },
              {
                "label_id":38,
                "label_name":"Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.50"
                  },
                  {
                    "value":3,
                    "odd":"4.20"
                  },
                  {
                    "value":4,
                    "odd":"6.00"
                  },
                  {
                    "value":1,
                    "odd":"4.50"
                  },
                  {
                    "value":0,
                    "odd":"7.50"
                  },
                  {
                    "value":5,
                    "odd":"11.50"
                  },
                  {
                    "value":6,
                    "odd":"23.00"
                  },
                  {
                    "value":7,
                    "odd":"67.00"
                  },
                  {
                    "value":"more 8",
                    "odd":"126.00"
                  }
                ]
              },
              {
                "label_id":40,
                "label_name":"Home Team Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.60"
                  },
                  {
                    "value":1,
                    "odd":"2.75"
                  },
                  {
                    "value":0,
                    "odd":"3.80"
                  },
                  {
                    "value":"more 3",
                    "odd":"4.20"
                  }
                ]
              },
              {
                "label_id":41,
                "label_name":"Away Team Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"4.75"
                  },
                  {
                    "value":1,
                    "odd":"2.50"
                  },
                  {
                    "value":0,
                    "odd":"2.45"
                  },
                  {
                    "value":"more 3",
                    "odd":"7.75"
                  }
                ]
              },
              {
                "label_id":42,
                "label_name":"Second Half Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.70"
                  },
                  {
                    "value":1,
                    "odd":"2.70"
                  },
                  {
                    "value":0,
                    "odd":"3.50"
                  },
                  {
                    "value":"more 3",
                    "odd":"4.60"
                  }
                ]
              },
              {
                "label_id":24,
                "label_name":"Results\/Both Teams Score",
                "values":[
                  {
                    "value":"Draw\/Yes",
                    "odd":"4.50"
                  },
                  {
                    "value":"Draw\/No",
                    "odd":"7.50"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over 3.5",
                    "odd":"5.75"
                  },
                  {
                    "value":"Away\/Over 3.5",
                    "odd":"13.00"
                  },
                  {
                    "value":"Home\/Under 3.5",
                    "odd":"2.60"
                  },
                  {
                    "value":"Away\/Under 3.5",
                    "odd":"4.50"
                  },
                  {
                    "value":"Home\/Over 2.5",
                    "odd":"3.10"
                  },
                  {
                    "value":"Away\/Over 2.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Home\/Under 2.5",
                    "odd":"4.20"
                  },
                  {
                    "value":"Away\/Under 2.5",
                    "odd":"7.00"
                  }
                ]
              },
              {
                "label_id":43,
                "label_name":"Home Team Score a Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.22"
                  },
                  {
                    "value":"No",
                    "odd":"3.80"
                  }
                ]
              },
              {
                "label_id":44,
                "label_name":"Away Team Score a Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.48"
                  },
                  {
                    "value":"No",
                    "odd":"2.45"
                  }
                ]
              },
              {
                "label_id":55,
                "label_name":"Corners 1x2",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.40"
                  },
                  {
                    "value":"Draw",
                    "odd":"7.75"
                  },
                  {
                    "value":"Away",
                    "odd":"3.80"
                  }
                ]
              },
              {
                "label_id":45,
                "label_name":"Corners Over Under",
                "values":[
                  {
                    "value":"Over 9.5",
                    "odd":"1.44"
                  },
                  {
                    "value":"Under 9.5",
                    "odd":"2.60"
                  },
                  {
                    "value":"Over 10.5",
                    "odd":"1.75"
                  },
                  {
                    "value":"Under 10.5",
                    "odd":"1.95"
                  },
                  {
                    "value":"Over 11.5",
                    "odd":"2.25"
                  },
                  {
                    "value":"Under 11.5",
                    "odd":"1.57"
                  },
                  {
                    "value":"Over 8.5",
                    "odd":"1.22"
                  },
                  {
                    "value":"Under 8.5",
                    "odd":"3.80"
                  },
                  {
                    "value":"Over 12.5",
                    "odd":"3.00"
                  },
                  {
                    "value":"Under 12.5",
                    "odd":"1.34"
                  }
                ]
              },
              {
                "label_id":46,
                "label_name":"Exact Goals Number - First Half",
                "values":[
                  {
                    "value":2,
                    "odd":"4.25"
                  },
                  {
                    "value":1,
                    "odd":"2.60"
                  },
                  {
                    "value":0,
                    "odd":"2.65"
                  },
                  {
                    "value":"more 3",
                    "odd":"7.00"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":1,
            "bookmaker_name":"10Bet",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.95"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.15"
                  },
                  {
                    "value":"Away",
                    "odd":"3.80"
                  }
                ]
              },
              {
                "label_id":2,
                "label_name":"Home\/Away",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.43"
                  },
                  {
                    "value":"Away",
                    "odd":"2.65"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.45"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.40"
                  },
                  {
                    "value":"Away",
                    "odd":"3.45"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.42"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.69"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"1.70"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.05"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"1.98"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.76"
                  },
                  {
                    "value":"Home +0.5",
                    "odd":"1.25"
                  },
                  {
                    "value":"Away +0.5",
                    "odd":"3.65"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.32"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.55"
                  },
                  {
                    "value":"Home -1",
                    "odd":"2.82"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.39"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"3.80"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.23"
                  },
                  {
                    "value":"Home +1.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Away +1.5",
                    "odd":"7.75"
                  },
                  {
                    "value":"Home -2.5",
                    "odd":"7.75"
                  },
                  {
                    "value":"Away -2.5",
                    "odd":"1.05"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.60"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.25"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.31"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"7.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.74"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.03"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"9.25"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"14.25"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.77"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"1.97"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.29"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.56"
                  },
                  {
                    "value":"Over 6.5",
                    "odd":"24.50"
                  },
                  {
                    "value":"Over 3.0",
                    "odd":"2.81"
                  },
                  {
                    "value":"Under 3.0",
                    "odd":"1.39"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.51"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.42"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"17.75"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"3.00"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.34"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"7.50"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.42"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.70"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"2.49"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"1.49"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.57"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.27"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"1.94"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.79"
                  }
                ]
              },
              {
                "label_id":26,
                "label_name":"Goals Over\/Under - Second Half",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"2.20"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.61"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"14.25"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"32.50"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"7.25"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"5.10"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.05"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"5.25"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"28.50"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"16.25"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"5.80"
                  }
                ]
              },
              {
                "label_id":27,
                "label_name":"Clean Sheet - Home",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"2.60"
                  },
                  {
                    "value":"No",
                    "odd":"1.45"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.80"
                  },
                  {
                    "value":"No",
                    "odd":"1.95"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.95"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.74"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.85"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.26"
                  },
                  {
                    "value":"Away +1",
                    "odd":"8.25"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.85"
                  },
                  {
                    "value":"Home +2",
                    "odd":"1.06"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"11.00"
                  },
                  {
                    "value":"Away +2",
                    "odd":"18.50"
                  },
                  {
                    "value":"Home -2",
                    "odd":"8.50"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"6.10"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.24"
                  },
                  {
                    "value":"Home -3",
                    "odd":"18.75"
                  },
                  {
                    "value":"Draw -3",
                    "odd":"11.50"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.06"
                  },
                  {
                    "value":"Draw +3",
                    "odd":"22.00"
                  },
                  {
                    "value":"Away +3",
                    "odd":"32.75"
                  },
                  {
                    "value":"Home -4",
                    "odd":"31.50"
                  },
                  {
                    "value":"Draw -4",
                    "odd":"22.00"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"6.70"
                  },
                  {
                    "value":"2:0",
                    "odd":"8.75"
                  },
                  {
                    "value":"2:1",
                    "odd":"8.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"18.75"
                  },
                  {
                    "value":"3:1",
                    "odd":"17.75"
                  },
                  {
                    "value":"3:2",
                    "odd":"29.25"
                  },
                  {
                    "value":"4:0",
                    "odd":"39.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"37.75"
                  },
                  {
                    "value":"4:2",
                    "odd":"54.25"
                  },
                  {
                    "value":"4:3",
                    "odd":"80.25"
                  },
                  {
                    "value":"5:0",
                    "odd":"71.50"
                  },
                  {
                    "value":"5:1",
                    "odd":"70.50"
                  },
                  {
                    "value":"5:2",
                    "odd":"83.50"
                  },
                  {
                    "value":"0:0",
                    "odd":"8.00"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"13.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"52.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"11.75"
                  },
                  {
                    "value":"0:2",
                    "odd":"20.25"
                  },
                  {
                    "value":"0:3",
                    "odd":"41.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"75.75"
                  },
                  {
                    "value":"1:2",
                    "odd":"12.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"31.25"
                  },
                  {
                    "value":"1:4",
                    "odd":"66.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"39.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"73.75"
                  },
                  {
                    "value":"3:4",
                    "odd":"88.50"
                  }
                ]
              },
              {
                "label_id":11,
                "label_name":"Highest Scoring Half",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"3.25"
                  },
                  {
                    "value":"1st Half",
                    "odd":"3.00"
                  },
                  {
                    "value":"2nd Half",
                    "odd":"2.00"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"3.40"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.25"
                  },
                  {
                    "value":"2:1",
                    "odd":"18.75"
                  },
                  {
                    "value":"3:0",
                    "odd":"30.50"
                  },
                  {
                    "value":"3:1",
                    "odd":"47.75"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.35"
                  },
                  {
                    "value":"1:1",
                    "odd":"7.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"47.75"
                  },
                  {
                    "value":"0:1",
                    "odd":"4.90"
                  },
                  {
                    "value":"0:2",
                    "odd":"17.75"
                  },
                  {
                    "value":"0:3",
                    "odd":"55.50"
                  },
                  {
                    "value":"1:2",
                    "odd":"24.75"
                  },
                  {
                    "value":"1:3",
                    "odd":"62.25"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.23"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.31"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.74"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.60"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.10"
                  },
                  {
                    "value":"Away",
                    "odd":"4.10"
                  }
                ]
              },
              {
                "label_id":14,
                "label_name":"Team To Score First",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.69"
                  },
                  {
                    "value":"Draw",
                    "odd":"9.50"
                  },
                  {
                    "value":"Away",
                    "odd":"2.40"
                  }
                ]
              },
              {
                "label_id":15,
                "label_name":"Team To Score Last",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.77"
                  },
                  {
                    "value":"Away",
                    "odd":"2.40"
                  },
                  {
                    "value":"No goal",
                    "odd":"9.50"
                  }
                ]
              },
              {
                "label_id":18,
                "label_name":"Handicap Result - First Half",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"7.75"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.43"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.55"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.21"
                  },
                  {
                    "value":"Away +1",
                    "odd":"14.00"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.15"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"15.25"
                  },
                  {
                    "value":"Away +2",
                    "odd":"29.75"
                  },
                  {
                    "value":"Home -2",
                    "odd":"21.00"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"9.25"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.06"
                  },
                  {
                    "value":"Home -3",
                    "odd":"32.75"
                  },
                  {
                    "value":"Draw -3",
                    "odd":"22.25"
                  }
                ]
              },
              {
                "label_id":19,
                "label_name":"Asian Handicap First Half",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.53"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.36"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"2.05"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"1.70"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.61"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.44"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"3.30"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.29"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.18"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.62"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.42"
                  }
                ]
              },
              {
                "label_id":36,
                "label_name":"Win To Nil",
                "values":[
                  {
                    "value":"Home",
                    "odd":"3.30"
                  },
                  {
                    "value":"Away",
                    "odd":"5.85"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.87"
                  },
                  {
                    "value":"Even",
                    "odd":"1.87"
                  }
                ]
              },
              {
                "label_id":22,
                "label_name":"Odd\/Even - First Half",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.10"
                  },
                  {
                    "value":"Even",
                    "odd":"1.69"
                  }
                ]
              },
              {
                "label_id":23,
                "label_name":"Home Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.95"
                  },
                  {
                    "value":"Even",
                    "odd":"1.80"
                  }
                ]
              },
              {
                "label_id":38,
                "label_name":"Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.10"
                  },
                  {
                    "value":3,
                    "odd":"3.75"
                  },
                  {
                    "value":4,
                    "odd":"5.55"
                  },
                  {
                    "value":1,
                    "odd":"4.25"
                  },
                  {
                    "value":0,
                    "odd":"9.50"
                  },
                  {
                    "value":5,
                    "odd":"11.00"
                  },
                  {
                    "value":"more 6",
                    "odd":"13.25"
                  }
                ]
              },
              {
                "label_id":40,
                "label_name":"Home Team Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.30"
                  },
                  {
                    "value":3,
                    "odd":"6.55"
                  },
                  {
                    "value":1,
                    "odd":"2.50"
                  },
                  {
                    "value":0,
                    "odd":"3.75"
                  },
                  {
                    "value":"more 4",
                    "odd":"11.50"
                  }
                ]
              },
              {
                "label_id":24,
                "label_name":"Results\/Both Teams Score",
                "values":[
                  {
                    "value":"Home\/Yes",
                    "odd":"3.85"
                  },
                  {
                    "value":"Draw\/Yes",
                    "odd":"3.80"
                  },
                  {
                    "value":"Away\/Yes",
                    "odd":"6.75"
                  },
                  {
                    "value":"Home\/No",
                    "odd":"3.10"
                  },
                  {
                    "value":"Draw\/No",
                    "odd":"8.75"
                  },
                  {
                    "value":"Away\/No",
                    "odd":"5.45"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over 3.5",
                    "odd":"6.55"
                  },
                  {
                    "value":"Draw\/Over 3.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away\/Over 3.5",
                    "odd":"14.00"
                  },
                  {
                    "value":"Home\/Under 3.5",
                    "odd":"2.65"
                  },
                  {
                    "value":"Draw\/Under 3.5",
                    "odd":"3.80"
                  },
                  {
                    "value":"Away\/Under 3.5",
                    "odd":"4.60"
                  },
                  {
                    "value":"Home\/Over 2.5",
                    "odd":"3.35"
                  },
                  {
                    "value":"Draw\/Over 2.5",
                    "odd":"12.25"
                  },
                  {
                    "value":"Away\/Over 2.5",
                    "odd":"6.70"
                  },
                  {
                    "value":"Home\/Under 2.5",
                    "odd":"4.45"
                  },
                  {
                    "value":"Draw\/Under 2.5",
                    "odd":"3.90"
                  },
                  {
                    "value":"Away\/Under 2.5",
                    "odd":"7.50"
                  }
                ]
              },
              {
                "label_id":55,
                "label_name":"Corners 1x2",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.42"
                  },
                  {
                    "value":"Draw",
                    "odd":"9.00"
                  },
                  {
                    "value":"Away",
                    "odd":"3.65"
                  }
                ]
              },
              {
                "label_id":45,
                "label_name":"Corners Over Under",
                "values":[
                  {
                    "value":"Over 10.5",
                    "odd":"1.69"
                  },
                  {
                    "value":"Under 10.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Over 11",
                    "odd":"1.91"
                  },
                  {
                    "value":"Under 11",
                    "odd":"1.80"
                  }
                ]
              },
              {
                "label_id":46,
                "label_name":"Exact Goals Number - First Half",
                "values":[
                  {
                    "value":2,
                    "odd":"3.85"
                  },
                  {
                    "value":1,
                    "odd":"2.15"
                  },
                  {
                    "value":0,
                    "odd":"2.75"
                  },
                  {
                    "value":"more 3",
                    "odd":"7.50"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":8,
            "bookmaker_name":"Bet365",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.00"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.30"
                  },
                  {
                    "value":"Away",
                    "odd":"3.90"
                  }
                ]
              },
              {
                "label_id":2,
                "label_name":"Home\/Away",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.40"
                  },
                  {
                    "value":"Away",
                    "odd":"2.75"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.30"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.40"
                  },
                  {
                    "value":"Away",
                    "odd":"3.75"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.45"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.67"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"1.72"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.07"
                  },
                  {
                    "value":"Home +0.25",
                    "odd":"1.35"
                  },
                  {
                    "value":"Away +0.25",
                    "odd":"3.10"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.08"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.85"
                  },
                  {
                    "value":"Home +0.5",
                    "odd":"1.27"
                  },
                  {
                    "value":"Away +0.5",
                    "odd":"3.55"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.30"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.60"
                  },
                  {
                    "value":"Home +0.75",
                    "odd":"1.18"
                  },
                  {
                    "value":"Away +0.75",
                    "odd":"4.65"
                  },
                  {
                    "value":"Home -1",
                    "odd":"3.00"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.37"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"3.70"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.26"
                  },
                  {
                    "value":"Home -1.25",
                    "odd":"3.45"
                  },
                  {
                    "value":"Away -1.25",
                    "odd":"1.30"
                  },
                  {
                    "value":"Home -1.75",
                    "odd":"4.80"
                  },
                  {
                    "value":"Away -1.75",
                    "odd":"1.17"
                  },
                  {
                    "value":"Home -2",
                    "odd":"7.00"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.10"
                  },
                  {
                    "value":"Home -2.5",
                    "odd":"7.00"
                  },
                  {
                    "value":"Away -2.5",
                    "odd":"1.10"
                  },
                  {
                    "value":"Home -2.25",
                    "odd":"7.00"
                  },
                  {
                    "value":"Away -2.25",
                    "odd":"1.10"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.50"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.40"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.11"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.80"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"9.50"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"15.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.03"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.85"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"2.05"
                  },
                  {
                    "value":"Over 3.25",
                    "odd":"3.30"
                  },
                  {
                    "value":"Under 3.25",
                    "odd":"1.32"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.35"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.57"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"1.21"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"4.25"
                  },
                  {
                    "value":"Over 4.25",
                    "odd":"6.80"
                  },
                  {
                    "value":"Under 4.25",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"1.42"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"2.75"
                  },
                  {
                    "value":"Over 3.75",
                    "odd":"4.40"
                  },
                  {
                    "value":"Under 3.75",
                    "odd":"1.20"
                  },
                  {
                    "value":"Over 6.5",
                    "odd":"26.00"
                  },
                  {
                    "value":"Under 6.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 3.0",
                    "odd":"2.85"
                  },
                  {
                    "value":"Under 3.0",
                    "odd":"1.40"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.55"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.37"
                  },
                  {
                    "value":"Over 4.0",
                    "odd":"6.40"
                  },
                  {
                    "value":"Under 4.0",
                    "odd":"1.11"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"23.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"3.00"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.36"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.40"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.75"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"7.00"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"2.50"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"1.50"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"4.00"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"1.23"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.62"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.25"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"6.80"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"2.05"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.80"
                  }
                ]
              },
              {
                "label_id":26,
                "label_name":"Goals Over\/Under - Second Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"13.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.10"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.66"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.50"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.18"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.25"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"3.75"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"34.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"8.50"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"5.50"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.40"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"5.00"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"26.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":27,
                "label_name":"Clean Sheet - Home",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"2.62"
                  },
                  {
                    "value":"No",
                    "odd":"1.44"
                  }
                ]
              },
              {
                "label_id":28,
                "label_name":"Clean Sheet - Away",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"4.33"
                  },
                  {
                    "value":"No",
                    "odd":"1.20"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.80"
                  },
                  {
                    "value":"No",
                    "odd":"1.95"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.60"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.72"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"4.00"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.25"
                  },
                  {
                    "value":"Away +1",
                    "odd":"8.00"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.50"
                  },
                  {
                    "value":"Home +2",
                    "odd":"1.05"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away +2",
                    "odd":"23.00"
                  },
                  {
                    "value":"Home -2",
                    "odd":"7.50"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"5.50"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.25"
                  },
                  {
                    "value":"Home -3",
                    "odd":"21.00"
                  },
                  {
                    "value":"Draw -3",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.06"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"8.00"
                  },
                  {
                    "value":"2:0",
                    "odd":"10.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"9.50"
                  },
                  {
                    "value":"3:0",
                    "odd":"19.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"19.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"29.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"41.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"41.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"51.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"101.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"81.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"81.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"126.00"
                  },
                  {
                    "value":"5:3",
                    "odd":"301.00"
                  },
                  {
                    "value":"6:0",
                    "odd":"251.00"
                  },
                  {
                    "value":"6:1",
                    "odd":"251.00"
                  },
                  {
                    "value":"6:2",
                    "odd":"401.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"9.50"
                  },
                  {
                    "value":"1:1",
                    "odd":"7.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"17.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"51.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"301.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"12.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"21.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"41.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"101.00"
                  },
                  {
                    "value":"0:5",
                    "odd":"401.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"15.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"34.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"81.00"
                  },
                  {
                    "value":"1:5",
                    "odd":"251.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"41.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"101.00"
                  },
                  {
                    "value":"2:5",
                    "odd":"351.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"151.00"
                  }
                ]
              },
              {
                "label_id":11,
                "label_name":"Highest Scoring Half",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"3.50"
                  },
                  {
                    "value":"1st Half",
                    "odd":"3.20"
                  },
                  {
                    "value":"2nd Half",
                    "odd":"2.05"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.00"
                  },
                  {
                    "value":"2:0",
                    "odd":"11.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"21.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"41.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"51.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"151.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"101.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"501.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.75"
                  },
                  {
                    "value":"1:1",
                    "odd":"8.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"51.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"5.50"
                  },
                  {
                    "value":"0:2",
                    "odd":"21.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"67.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"351.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"29.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"81.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"501.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"201.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.25"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.33"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.72"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.62"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.10"
                  },
                  {
                    "value":"Away",
                    "odd":"4.33"
                  }
                ]
              },
              {
                "label_id":14,
                "label_name":"Team To Score First",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.61"
                  },
                  {
                    "value":"Draw",
                    "odd":"9.50"
                  },
                  {
                    "value":"Away",
                    "odd":"2.50"
                  }
                ]
              },
              {
                "label_id":15,
                "label_name":"Team To Score Last",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.61"
                  },
                  {
                    "value":"Away",
                    "odd":"2.50"
                  },
                  {
                    "value":"No goal",
                    "odd":"9.50"
                  }
                ]
              },
              {
                "label_id":32,
                "label_name":"Win Both Halves",
                "values":[
                  {
                    "value":"Home",
                    "odd":"6.50"
                  },
                  {
                    "value":"Away",
                    "odd":"15.00"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"11.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.72"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"26.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.33"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.20"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"26.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"3.25"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"10.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.06"
                  }
                ]
              },
              {
                "label_id":18,
                "label_name":"Handicap Result - First Half",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"8.00"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.44"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.40"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.20"
                  },
                  {
                    "value":"Away +1",
                    "odd":"15.00"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"4.75"
                  },
                  {
                    "value":"Home -2",
                    "odd":"29.00"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"10.00"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.06"
                  }
                ]
              },
              {
                "label_id":19,
                "label_name":"Asian Handicap First Half",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.52"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.42"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"2.15"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"1.72"
                  },
                  {
                    "value":"Home +0.25",
                    "odd":"1.32"
                  },
                  {
                    "value":"Away +0.25",
                    "odd":"3.30"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.67"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.45"
                  },
                  {
                    "value":"Home +0.5",
                    "odd":"1.22"
                  },
                  {
                    "value":"Away +0.5",
                    "odd":"4.15"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"3.55"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.27"
                  },
                  {
                    "value":"Home +0.75",
                    "odd":"1.12"
                  },
                  {
                    "value":"Away +0.75",
                    "odd":"6.00"
                  },
                  {
                    "value":"Home -1",
                    "odd":"6.60"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.11"
                  },
                  {
                    "value":"Home -1.25",
                    "odd":"7.00"
                  },
                  {
                    "value":"Away -1.25",
                    "odd":"1.10"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.20"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.66"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.44"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"5.00"
                  },
                  {
                    "value":"No",
                    "odd":"1.16"
                  }
                ]
              },
              {
                "label_id":35,
                "label_name":"Both Teams To Score - Second Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.40"
                  },
                  {
                    "value":"No",
                    "odd":"1.30"
                  }
                ]
              },
              {
                "label_id":36,
                "label_name":"Win To Nil",
                "values":[
                  {
                    "value":"Home",
                    "odd":"3.40"
                  },
                  {
                    "value":"Away",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.95"
                  },
                  {
                    "value":"Even",
                    "odd":"1.90"
                  }
                ]
              },
              {
                "label_id":22,
                "label_name":"Odd\/Even - First Half",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.10"
                  },
                  {
                    "value":"Even",
                    "odd":"1.72"
                  }
                ]
              },
              {
                "label_id":38,
                "label_name":"Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.50"
                  },
                  {
                    "value":3,
                    "odd":"4.33"
                  },
                  {
                    "value":4,
                    "odd":"6.00"
                  },
                  {
                    "value":1,
                    "odd":"4.75"
                  },
                  {
                    "value":0,
                    "odd":"9.50"
                  },
                  {
                    "value":5,
                    "odd":"12.00"
                  },
                  {
                    "value":6,
                    "odd":"23.00"
                  },
                  {
                    "value":"more 7",
                    "odd":"29.00"
                  }
                ]
              },
              {
                "label_id":39,
                "label_name":"To Win Either Half",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.57"
                  },
                  {
                    "value":"Away",
                    "odd":"2.25"
                  }
                ]
              },
              {
                "label_id":40,
                "label_name":"Home Team Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.50"
                  },
                  {
                    "value":1,
                    "odd":"2.75"
                  },
                  {
                    "value":0,
                    "odd":"4.33"
                  },
                  {
                    "value":"more 3",
                    "odd":"4.33"
                  }
                ]
              },
              {
                "label_id":41,
                "label_name":"Away Team Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"4.50"
                  },
                  {
                    "value":1,
                    "odd":"2.40"
                  },
                  {
                    "value":0,
                    "odd":"2.62"
                  },
                  {
                    "value":"more 3",
                    "odd":"10.00"
                  }
                ]
              },
              {
                "label_id":42,
                "label_name":"Second Half Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.60"
                  },
                  {
                    "value":3,
                    "odd":"7.00"
                  },
                  {
                    "value":4,
                    "odd":"17.00"
                  },
                  {
                    "value":1,
                    "odd":"2.75"
                  },
                  {
                    "value":0,
                    "odd":"3.75"
                  },
                  {
                    "value":"more 5",
                    "odd":"29.00"
                  }
                ]
              },
              {
                "label_id":24,
                "label_name":"Results\/Both Teams Score",
                "values":[
                  {
                    "value":"Home\/Yes",
                    "odd":"4.33"
                  },
                  {
                    "value":"Draw\/Yes",
                    "odd":"4.50"
                  },
                  {
                    "value":"Away\/Yes",
                    "odd":"7.50"
                  },
                  {
                    "value":"Home\/No",
                    "odd":"3.40"
                  },
                  {
                    "value":"Draw\/No",
                    "odd":"9.50"
                  },
                  {
                    "value":"Away\/No",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over 2.5",
                    "odd":"3.25"
                  },
                  {
                    "value":"Draw\/Over 2.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away\/Over 2.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Home\/Under 2.5",
                    "odd":"4.33"
                  },
                  {
                    "value":"Draw\/Under 2.5",
                    "odd":"4.00"
                  },
                  {
                    "value":"Away\/Under 2.5",
                    "odd":"7.50"
                  }
                ]
              },
              {
                "label_id":43,
                "label_name":"Home Team Score a Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.20"
                  },
                  {
                    "value":"No",
                    "odd":"4.33"
                  }
                ]
              },
              {
                "label_id":44,
                "label_name":"Away Team Score a Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.44"
                  },
                  {
                    "value":"No",
                    "odd":"2.62"
                  }
                ]
              },
              {
                "label_id":54,
                "label_name":"First 10 min Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"9.00"
                  },
                  {
                    "value":"Draw",
                    "odd":"1.14"
                  },
                  {
                    "value":"Away",
                    "odd":"13.00"
                  }
                ]
              },
              {
                "label_id":55,
                "label_name":"Corners 1x2",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.44"
                  },
                  {
                    "value":"Draw",
                    "odd":"8.00"
                  },
                  {
                    "value":"Away",
                    "odd":"3.40"
                  }
                ]
              },
              {
                "label_id":45,
                "label_name":"Corners Over Under",
                "values":[
                  {
                    "value":"Over 10.5",
                    "odd":"1.80"
                  },
                  {
                    "value":"Under 10.5",
                    "odd":"1.90"
                  },
                  {
                    "value":"Over 11.5",
                    "odd":"2.10"
                  },
                  {
                    "value":"Under 11.5",
                    "odd":"1.66"
                  }
                ]
              },
              {
                "label_id":46,
                "label_name":"Exact Goals Number - First Half",
                "values":[
                  {
                    "value":2,
                    "odd":"4.33"
                  },
                  {
                    "value":3,
                    "odd":"11.00"
                  },
                  {
                    "value":4,
                    "odd":"29.00"
                  },
                  {
                    "value":1,
                    "odd":"2.50"
                  },
                  {
                    "value":0,
                    "odd":"2.75"
                  },
                  {
                    "value":"more 5",
                    "odd":"67.00"
                  }
                ]
              },
              {
                "label_id":47,
                "label_name":"Winning Margin",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"9.50"
                  },
                  {
                    "value":"1 by 1",
                    "odd":"4.00"
                  },
                  {
                    "value":"2 by 1",
                    "odd":"5.50"
                  },
                  {
                    "value":"1 by 2",
                    "odd":"5.50"
                  },
                  {
                    "value":"2 by 2",
                    "odd":"12.00"
                  },
                  {
                    "value":"1 by 3",
                    "odd":"12.00"
                  },
                  {
                    "value":"2 by 3",
                    "odd":"34.00"
                  },
                  {
                    "value":"1 by 4+",
                    "odd":"21.00"
                  },
                  {
                    "value":"2 by 4+",
                    "odd":"67.00"
                  },
                  {
                    "value":"Score Draw",
                    "odd":"4.50"
                  }
                ]
              },
              {
                "label_id":48,
                "label_name":"To Score In Both Halves By Teams",
                "values":[
                  {
                    "value":"Home",
                    "odd":"3.25"
                  },
                  {
                    "value":"Away",
                    "odd":"5.50"
                  }
                ]
              },
              {
                "label_id":49,
                "label_name":"Total Goals\/Both Teams To Score",
                "values":[
                  {
                    "value":"o\/yes 2.5",
                    "odd":"2.30"
                  },
                  {
                    "value":"o\/no 2.5",
                    "odd":"9.50"
                  },
                  {
                    "value":"u\/yes 2.5",
                    "odd":"7.00"
                  },
                  {
                    "value":"u\/no 2.5",
                    "odd":"2.30"
                  }
                ]
              },
              {
                "label_id":50,
                "label_name":"Goal Line",
                "values":[
                  {
                    "value":"Over",
                    "odd":"4.40"
                  },
                  {
                    "value":"Under",
                    "odd":"1.20"
                  }
                ]
              },
              {
                "label_id":72,
                "label_name":"Goal Line (1st Half)",
                "values":[
                  {
                    "value":"Over",
                    "odd":"1.62"
                  },
                  {
                    "value":"Under",
                    "odd":"2.25"
                  }
                ]
              },
              {
                "label_id":52,
                "label_name":"Halftime Result\/Both Teams Score",
                "values":[
                  {
                    "value":"Home\/Yes",
                    "odd":"17.00"
                  },
                  {
                    "value":"Draw\/Yes",
                    "odd":"7.50"
                  },
                  {
                    "value":"Away\/Yes",
                    "odd":"26.00"
                  },
                  {
                    "value":"Home\/No",
                    "odd":"3.00"
                  },
                  {
                    "value":"Draw\/No",
                    "odd":"2.75"
                  },
                  {
                    "value":"Away\/No",
                    "odd":"4.75"
                  }
                ]
              },
              {
                "label_id":56,
                "label_name":"Corners Asian Handicap",
                "values":[
                  {
                    "value":"Home -2",
                    "odd":"1.95"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.85"
                  },
                  {
                    "value":"Home -2.5",
                    "odd":"1.90"
                  },
                  {
                    "value":"Away -2.5",
                    "odd":"1.90"
                  }
                ]
              },
              {
                "label_id":57,
                "label_name":"Home Corners Over\/Under",
                "values":[
                  {
                    "value":"Over 6.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Under 6.5",
                    "odd":"1.72"
                  }
                ]
              },
              {
                "label_id":58,
                "label_name":"Away Corners Over\/Under",
                "values":[
                  {
                    "value":"Over 4.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.72"
                  }
                ]
              },
              {
                "label_id":59,
                "label_name":"Own Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"9.00"
                  },
                  {
                    "value":"No",
                    "odd":"1.07"
                  }
                ]
              },
              {
                "label_id":73,
                "label_name":"Both Teams to Score 1st Half - 2nd Half",
                "values":[
                  {
                    "value":"Yes\/Yes",
                    "odd":"17.00"
                  },
                  {
                    "value":"Yes\/No",
                    "odd":"6.00"
                  },
                  {
                    "value":"No\/Yes",
                    "odd":"3.75"
                  },
                  {
                    "value":"No\/No",
                    "odd":"1.57"
                  }
                ]
              },
              {
                "label_id":74,
                "label_name":"10 Over\/Under",
                "values":[
                  {
                    "value":"Over 0.5",
                    "odd":"4.50"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"1.18"
                  }
                ]
              },
              {
                "label_id":75,
                "label_name":"Last Corner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.57"
                  },
                  {
                    "value":"Away",
                    "odd":"2.25"
                  }
                ]
              },
              {
                "label_id":76,
                "label_name":"First Corner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.57"
                  },
                  {
                    "value":"Away",
                    "odd":"2.25"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":2,
            "bookmaker_name":"Marathonbet",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.12"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.28"
                  },
                  {
                    "value":"Away",
                    "odd":"4.05"
                  }
                ]
              },
              {
                "label_id":2,
                "label_name":"Home\/Away",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.50"
                  },
                  {
                    "value":"Away",
                    "odd":"2.83"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.42"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.44"
                  },
                  {
                    "value":"Away",
                    "odd":"3.95"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home -0.25",
                    "odd":"1.77"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.10"
                  },
                  {
                    "value":"Home +0.25",
                    "odd":"1.35"
                  },
                  {
                    "value":"Away +0.25",
                    "odd":"3.32"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.39"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.59"
                  },
                  {
                    "value":"Home +0.75",
                    "odd":"1.18"
                  },
                  {
                    "value":"Away +0.75",
                    "odd":"5.10"
                  },
                  {
                    "value":"Home -1",
                    "odd":"3.04"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.38"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.08"
                  },
                  {
                    "value":"Away +1",
                    "odd":"8.10"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"3.78"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.25"
                  },
                  {
                    "value":"Home +1.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Away +1.5",
                    "odd":"8.90"
                  },
                  {
                    "value":"Home -1.25",
                    "odd":"3.45"
                  },
                  {
                    "value":"Away -1.25",
                    "odd":"1.30"
                  },
                  {
                    "value":"Home -1.75",
                    "odd":"4.90"
                  },
                  {
                    "value":"Away -1.75",
                    "odd":"1.16"
                  },
                  {
                    "value":"Home +1.25",
                    "odd":"1.06"
                  },
                  {
                    "value":"Away +1.25",
                    "odd":"8.50"
                  },
                  {
                    "value":"Home +1.75",
                    "odd":"1.02"
                  },
                  {
                    "value":"Away +1.75",
                    "odd":"11.75"
                  },
                  {
                    "value":"Home -2",
                    "odd":"7.20"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.07"
                  },
                  {
                    "value":"Home -3",
                    "odd":"13.50"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.00"
                  },
                  {
                    "value":"Home -2.5",
                    "odd":"7.70"
                  },
                  {
                    "value":"Away -2.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Home -2.25",
                    "odd":"7.50"
                  },
                  {
                    "value":"Away -2.25",
                    "odd":"1.07"
                  },
                  {
                    "value":"Home -2.75",
                    "odd":"10.25"
                  },
                  {
                    "value":"Away -2.75",
                    "odd":"1.02"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.50"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.29"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.18"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"6.55"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.11"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.81"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"7.70"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.82"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"2.01"
                  },
                  {
                    "value":"Over 3.25",
                    "odd":"3.20"
                  },
                  {
                    "value":"Under 3.25",
                    "odd":"1.34"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.60"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"1.21"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"4.10"
                  },
                  {
                    "value":"Over 4.25",
                    "odd":"6.25"
                  },
                  {
                    "value":"Under 4.25",
                    "odd":"1.09"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"1.42"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"2.82"
                  },
                  {
                    "value":"Over 3.75",
                    "odd":"4.33"
                  },
                  {
                    "value":"Under 3.75",
                    "odd":"1.19"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.06"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"7.30"
                  },
                  {
                    "value":"Over 3.0",
                    "odd":"2.88"
                  },
                  {
                    "value":"Under 3.0",
                    "odd":"1.42"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.55"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.45"
                  },
                  {
                    "value":"Over 4.0",
                    "odd":"5.95"
                  },
                  {
                    "value":"Under 4.0",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"1.08"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"6.75"
                  },
                  {
                    "value":"Over 5.0",
                    "odd":"11.00"
                  },
                  {
                    "value":"Under 5.0",
                    "odd":"1.00"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"2.95"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.40"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"7.80"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.48"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.65"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"6.45"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"1.11"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"2.01"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.80"
                  }
                ]
              },
              {
                "label_id":26,
                "label_name":"Goals Over\/Under - Second Half",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"2.26"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.64"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"5.05"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.17"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.28"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"3.68"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"3.85"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"1.26"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"1.53"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"2.50"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"41.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"8.40"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"4.50"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.24"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"4.95"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"29.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"15.25"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"6.80"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.84"
                  },
                  {
                    "value":"No",
                    "odd":"1.96"
                  }
                ]
              },
              {
                "label_id":29,
                "label_name":"Win to Nil - Home",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.30"
                  },
                  {
                    "value":"No",
                    "odd":"1.33"
                  }
                ]
              },
              {
                "label_id":30,
                "label_name":"Win to Nil - Away",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"6.00"
                  },
                  {
                    "value":"No",
                    "odd":"1.13"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.29"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.39"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.81"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.72"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.03"
                  },
                  {
                    "value":"Away",
                    "odd":"4.65"
                  }
                ]
              },
              {
                "label_id":14,
                "label_name":"Team To Score First",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.79"
                  },
                  {
                    "value":"Draw",
                    "odd":"7.70"
                  },
                  {
                    "value":"Away",
                    "odd":"2.59"
                  }
                ]
              },
              {
                "label_id":15,
                "label_name":"Team To Score Last",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.80"
                  },
                  {
                    "value":"Away",
                    "odd":"2.57"
                  },
                  {
                    "value":"No goal",
                    "odd":"7.70"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"11.25"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.14"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.71"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.70"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.19"
                  },
                  {
                    "value":"Over 2",
                    "odd":"3.60"
                  },
                  {
                    "value":"Under 2",
                    "odd":"1.29"
                  },
                  {
                    "value":"Over 3",
                    "odd":"10.00"
                  },
                  {
                    "value":"Under 3",
                    "odd":"1.05"
                  },
                  {
                    "value":"Over 1",
                    "odd":"1.44"
                  },
                  {
                    "value":"Under 1",
                    "odd":"2.77"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"3.32"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"9.30"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Over 2",
                    "odd":"7.90"
                  },
                  {
                    "value":"Under 2",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 1",
                    "odd":"2.17"
                  },
                  {
                    "value":"Under 1",
                    "odd":"1.69"
                  }
                ]
              },
              {
                "label_id":19,
                "label_name":"Asian Handicap First Half",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.53"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.50"
                  },
                  {
                    "value":"Home -1",
                    "odd":"6.70"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.11"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.02"
                  },
                  {
                    "value":"Away +1",
                    "odd":"14.25"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"8.40"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Home +1.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Away +1.5",
                    "odd":"15.75"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.16"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.72"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.42"
                  }
                ]
              },
              {
                "label_id":33,
                "label_name":"Double Chance - Second Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.22"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.51"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.52"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"4.70"
                  },
                  {
                    "value":"No",
                    "odd":"1.19"
                  }
                ]
              },
              {
                "label_id":35,
                "label_name":"Both Teams To Score - Second Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.52"
                  },
                  {
                    "value":"No",
                    "odd":"1.30"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.98"
                  },
                  {
                    "value":"Even",
                    "odd":"1.83"
                  }
                ]
              },
              {
                "label_id":22,
                "label_name":"Odd\/Even - First Half",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.22"
                  },
                  {
                    "value":"Even",
                    "odd":"1.66"
                  }
                ]
              },
              {
                "label_id":37,
                "label_name":"Home win both halves",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"6.65"
                  },
                  {
                    "value":"No",
                    "odd":"1.11"
                  }
                ]
              },
              {
                "label_id":53,
                "label_name":"Away win both halves",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"18.00"
                  },
                  {
                    "value":"No",
                    "odd":"1.00"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over 3.5",
                    "odd":"6.30"
                  },
                  {
                    "value":"Draw\/Over 3.5",
                    "odd":"11.25"
                  },
                  {
                    "value":"Away\/Over 3.5",
                    "odd":"12.75"
                  },
                  {
                    "value":"Home\/Under 3.5",
                    "odd":"2.79"
                  },
                  {
                    "value":"Draw\/Under 3.5",
                    "odd":"3.85"
                  },
                  {
                    "value":"Away\/Under 3.5",
                    "odd":"4.80"
                  },
                  {
                    "value":"Home\/Over 4.5",
                    "odd":"10.25"
                  },
                  {
                    "value":"Away\/Over 4.5",
                    "odd":"18.00"
                  },
                  {
                    "value":"Home\/Under 4.5",
                    "odd":"2.38"
                  },
                  {
                    "value":"Draw\/Under 4.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Away\/Under 4.5",
                    "odd":"4.30"
                  },
                  {
                    "value":"Home\/Over 2.5",
                    "odd":"3.38"
                  },
                  {
                    "value":"Draw\/Over 2.5",
                    "odd":"11.25"
                  },
                  {
                    "value":"Away\/Over 2.5",
                    "odd":"6.60"
                  },
                  {
                    "value":"Home\/Under 2.5",
                    "odd":"4.45"
                  },
                  {
                    "value":"Draw\/Under 2.5",
                    "odd":"3.85"
                  },
                  {
                    "value":"Away\/Under 2.5",
                    "odd":"7.40"
                  },
                  {
                    "value":"Home\/Over 5.5",
                    "odd":"18.00"
                  },
                  {
                    "value":"Home\/Under 5.5",
                    "odd":"2.15"
                  },
                  {
                    "value":"Draw\/Under 5.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Away\/Under 5.5",
                    "odd":"3.85"
                  }
                ]
              },
              {
                "label_id":43,
                "label_name":"Home Team Score a Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.24"
                  },
                  {
                    "value":"No",
                    "odd":"4.05"
                  }
                ]
              },
              {
                "label_id":44,
                "label_name":"Away Team Score a Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.49"
                  },
                  {
                    "value":"No",
                    "odd":"2.62"
                  }
                ]
              },
              {
                "label_id":55,
                "label_name":"Corners 1x2",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.40"
                  },
                  {
                    "value":"Draw",
                    "odd":"9.60"
                  },
                  {
                    "value":"Away",
                    "odd":"3.88"
                  }
                ]
              },
              {
                "label_id":45,
                "label_name":"Corners Over Under",
                "values":[
                  {
                    "value":"Over 6.5",
                    "odd":"1.03"
                  },
                  {
                    "value":"Under 6.5",
                    "odd":"10.00"
                  },
                  {
                    "value":"Over 9.5",
                    "odd":"1.43"
                  },
                  {
                    "value":"Under 9.5",
                    "odd":"2.78"
                  },
                  {
                    "value":"Over 10.5",
                    "odd":"1.72"
                  },
                  {
                    "value":"Under 10.5",
                    "odd":"2.12"
                  },
                  {
                    "value":"Over 7.5",
                    "odd":"1.10"
                  },
                  {
                    "value":"Under 7.5",
                    "odd":"6.15"
                  },
                  {
                    "value":"Over 11.5",
                    "odd":"2.17"
                  },
                  {
                    "value":"Under 11.5",
                    "odd":"1.69"
                  },
                  {
                    "value":"Over 8.5",
                    "odd":"1.22"
                  },
                  {
                    "value":"Under 8.5",
                    "odd":"4.10"
                  },
                  {
                    "value":"Over 13.5",
                    "odd":"3.96"
                  },
                  {
                    "value":"Under 13.5",
                    "odd":"1.24"
                  },
                  {
                    "value":"Over 12.5",
                    "odd":"2.88"
                  },
                  {
                    "value":"Under 12.5",
                    "odd":"1.42"
                  },
                  {
                    "value":"Over 15.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Under 15.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Over 14.5",
                    "odd":"5.60"
                  },
                  {
                    "value":"Under 14.5",
                    "odd":"1.13"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":16,
            "bookmaker_name":"Unibet",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.06"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.40"
                  },
                  {
                    "value":"Away",
                    "odd":"4.10"
                  }
                ]
              },
              {
                "label_id":2,
                "label_name":"Home\/Away",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.44"
                  },
                  {
                    "value":"Away",
                    "odd":"2.88"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.35"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.40"
                  },
                  {
                    "value":"Away",
                    "odd":"3.95"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.43"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.88"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"1.72"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.16"
                  },
                  {
                    "value":"Home +0.25",
                    "odd":"1.33"
                  },
                  {
                    "value":"Away +0.25",
                    "odd":"3.45"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.83"
                  },
                  {
                    "value":"Home +0.5",
                    "odd":"1.26"
                  },
                  {
                    "value":"Away +0.5",
                    "odd":"4.00"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.61"
                  },
                  {
                    "value":"Home +0.75",
                    "odd":"1.17"
                  },
                  {
                    "value":"Away +0.75",
                    "odd":"5.40"
                  },
                  {
                    "value":"Home -1",
                    "odd":"3.10"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.38"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.08"
                  },
                  {
                    "value":"Away +1",
                    "odd":"9.00"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"4.10"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.26"
                  },
                  {
                    "value":"Home +1.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Away +1.5",
                    "odd":"10.50"
                  },
                  {
                    "value":"Home -1.25",
                    "odd":"3.60"
                  },
                  {
                    "value":"Away -1.25",
                    "odd":"1.30"
                  },
                  {
                    "value":"Home -1.75",
                    "odd":"5.40"
                  },
                  {
                    "value":"Away -1.75",
                    "odd":"1.17"
                  },
                  {
                    "value":"Home +1.25",
                    "odd":"1.07"
                  },
                  {
                    "value":"Away +1.25",
                    "odd":"10.00"
                  },
                  {
                    "value":"Home +1.75",
                    "odd":"1.04"
                  },
                  {
                    "value":"Away +1.75",
                    "odd":"16.00"
                  },
                  {
                    "value":"Home -2",
                    "odd":"8.50"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.09"
                  },
                  {
                    "value":"Home -3",
                    "odd":"26.00"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.01"
                  },
                  {
                    "value":"Home -2.5",
                    "odd":"10.00"
                  },
                  {
                    "value":"Away -2.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Home -2.25",
                    "odd":"9.00"
                  },
                  {
                    "value":"Away -2.25",
                    "odd":"1.08"
                  },
                  {
                    "value":"Home -2.75",
                    "odd":"14.00"
                  },
                  {
                    "value":"Away -2.75",
                    "odd":"1.04"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.70"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.28"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.32"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.35"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"7.50"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.09"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.02"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.79"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"9.50"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"16.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.77"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"2.06"
                  },
                  {
                    "value":"Over 3.25",
                    "odd":"3.30"
                  },
                  {
                    "value":"Under 3.25",
                    "odd":"1.33"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.35"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.60"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"1.20"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"4.70"
                  },
                  {
                    "value":"Over 4.25",
                    "odd":"7.50"
                  },
                  {
                    "value":"Under 4.25",
                    "odd":"1.11"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"1.38"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"3.00"
                  },
                  {
                    "value":"Over 3.75",
                    "odd":"4.70"
                  },
                  {
                    "value":"Under 3.75",
                    "odd":"1.20"
                  },
                  {
                    "value":"Over 4.75",
                    "odd":"10.50"
                  },
                  {
                    "value":"Under 4.75",
                    "odd":"1.06"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.07"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"9.50"
                  },
                  {
                    "value":"Over 5.25",
                    "odd":"17.00"
                  },
                  {
                    "value":"Under 5.25",
                    "odd":"1.03"
                  },
                  {
                    "value":"Over 3.0",
                    "odd":"2.95"
                  },
                  {
                    "value":"Under 3.0",
                    "odd":"1.41"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.50"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.60"
                  },
                  {
                    "value":"Over 4.0",
                    "odd":"6.75"
                  },
                  {
                    "value":"Under 4.0",
                    "odd":"1.12"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"1.09"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"8.50"
                  },
                  {
                    "value":"Over 5.0",
                    "odd":"17.00"
                  },
                  {
                    "value":"Under 5.0",
                    "odd":"1.03"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"3.00"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.35"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.38"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.85"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"7.50"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"1.09"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"2.48"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"1.50"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"4.00"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"1.23"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.57"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.35"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"7.00"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"1.94"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.83"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"13.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"36.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"8.50"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"4.90"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.25"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"5.10"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"25.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"14.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"6.75"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.79"
                  },
                  {
                    "value":"No",
                    "odd":"1.98"
                  }
                ]
              },
              {
                "label_id":29,
                "label_name":"Win to Nil - Home",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.45"
                  },
                  {
                    "value":"No",
                    "odd":"1.27"
                  }
                ]
              },
              {
                "label_id":30,
                "label_name":"Win to Nil - Away",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"6.75"
                  },
                  {
                    "value":"No",
                    "odd":"1.09"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.85"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.76"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.70"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.22"
                  },
                  {
                    "value":"Away +1",
                    "odd":"9.50"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.60"
                  },
                  {
                    "value":"Home +2",
                    "odd":"1.04"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"12.50"
                  },
                  {
                    "value":"Away +2",
                    "odd":"29.00"
                  },
                  {
                    "value":"Home -2",
                    "odd":"9.00"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"6.00"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.21"
                  },
                  {
                    "value":"Home -3",
                    "odd":"23.00"
                  },
                  {
                    "value":"Draw -3",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.05"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"7.00"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"8.50"
                  },
                  {
                    "value":"3:0",
                    "odd":"17.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"15.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"25.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"36.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"34.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"56.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"121.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"91.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"91.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"141.00"
                  },
                  {
                    "value":"5:3",
                    "odd":"301.00"
                  },
                  {
                    "value":"6:0",
                    "odd":"301.00"
                  },
                  {
                    "value":"6:1",
                    "odd":"276.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"9.50"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.25"
                  },
                  {
                    "value":"2:2",
                    "odd":"13.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"51.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"301.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"10.50"
                  },
                  {
                    "value":"0:2",
                    "odd":"19.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"46.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"131.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"12.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"30.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"91.00"
                  },
                  {
                    "value":"1:5",
                    "odd":"301.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"34.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"101.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"161.00"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.10"
                  },
                  {
                    "value":"2:0",
                    "odd":"11.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"20.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"36.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"67.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"161.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"301.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.80"
                  },
                  {
                    "value":"1:1",
                    "odd":"7.50"
                  },
                  {
                    "value":"2:2",
                    "odd":"61.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"21.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"101.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"29.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"131.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"301.00"
                  }
                ]
              },
              {
                "label_id":62,
                "label_name":"Correct Score - Second Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.40"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.50"
                  },
                  {
                    "value":"2:1",
                    "odd":"14.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"26.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"41.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"101.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"91.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"141.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"301.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"3.80"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.75"
                  },
                  {
                    "value":"2:2",
                    "odd":"36.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"6.25"
                  },
                  {
                    "value":"0:2",
                    "odd":"18.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"67.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"19.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"71.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"131.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.26"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.35"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.83"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.63"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.07"
                  },
                  {
                    "value":"Away",
                    "odd":"4.35"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"12.50"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.08"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.71"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.80"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.18"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.20"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"4.40"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"3.40"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"10.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.45"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.70"
                  }
                ]
              },
              {
                "label_id":18,
                "label_name":"Handicap Result - First Half",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"8.50"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.38"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.50"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.16"
                  },
                  {
                    "value":"Away +1",
                    "odd":"18.00"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.10"
                  },
                  {
                    "value":"Home -2",
                    "odd":"31.00"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"10.00"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.05"
                  }
                ]
              },
              {
                "label_id":19,
                "label_name":"Asian Handicap First Half",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.49"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.55"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"2.08"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"1.71"
                  },
                  {
                    "value":"Home +0.25",
                    "odd":"1.28"
                  },
                  {
                    "value":"Away +0.25",
                    "odd":"3.50"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.70"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.45"
                  },
                  {
                    "value":"Home +0.5",
                    "odd":"1.20"
                  },
                  {
                    "value":"Away +0.5",
                    "odd":"4.40"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"3.60"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.27"
                  },
                  {
                    "value":"Home +0.75",
                    "odd":"1.11"
                  },
                  {
                    "value":"Away +0.75",
                    "odd":"6.50"
                  },
                  {
                    "value":"Home -1",
                    "odd":"6.75"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.10"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"9.00"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Home -1.25",
                    "odd":"8.00"
                  },
                  {
                    "value":"Away -1.25",
                    "odd":"1.08"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.16"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.65"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.41"
                  }
                ]
              },
              {
                "label_id":33,
                "label_name":"Double Chance - Second Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.20"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.48"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.50"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"4.80"
                  },
                  {
                    "value":"No",
                    "odd":"1.16"
                  }
                ]
              },
              {
                "label_id":35,
                "label_name":"Both Teams To Score - Second Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.45"
                  },
                  {
                    "value":"No",
                    "odd":"1.26"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.96"
                  },
                  {
                    "value":"Even",
                    "odd":"1.84"
                  }
                ]
              },
              {
                "label_id":23,
                "label_name":"Home Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.97"
                  },
                  {
                    "value":"Even",
                    "odd":"1.84"
                  }
                ]
              },
              {
                "label_id":60,
                "label_name":"Away Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.15"
                  },
                  {
                    "value":"Even",
                    "odd":"1.71"
                  }
                ]
              },
              {
                "label_id":37,
                "label_name":"Home win both halves",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"6.50"
                  },
                  {
                    "value":"No",
                    "odd":"1.10"
                  }
                ]
              },
              {
                "label_id":53,
                "label_name":"Away win both halves",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"16.00"
                  },
                  {
                    "value":"No",
                    "odd":"1.01"
                  }
                ]
              },
              {
                "label_id":55,
                "label_name":"Corners 1x2",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.32"
                  },
                  {
                    "value":"Draw",
                    "odd":"9.00"
                  },
                  {
                    "value":"Away",
                    "odd":"4.20"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":3,
            "bookmaker_name":"Betfair",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.05"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.30"
                  },
                  {
                    "value":"Away",
                    "odd":"4.00"
                  }
                ]
              },
              {
                "label_id":2,
                "label_name":"Home\/Away",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.40"
                  },
                  {
                    "value":"Away",
                    "odd":"2.75"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.40"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.40"
                  },
                  {
                    "value":"Away",
                    "odd":"3.75"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.80"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.36"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.40"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.10"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.80"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"9.50"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"21.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.02"
                  },
                  {
                    "value":"Over 6.5",
                    "odd":"51.00"
                  },
                  {
                    "value":"Under 6.5",
                    "odd":"1.00"
                  },
                  {
                    "value":"Over 7.5",
                    "odd":"101.00"
                  },
                  {
                    "value":"Under 7.5",
                    "odd":"1.00"
                  },
                  {
                    "value":"Over 8.5",
                    "odd":"101.00"
                  },
                  {
                    "value":"Under 8.5",
                    "odd":"1.00"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"19.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.00"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"3.00"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"67.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.00"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.40"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.88"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"101.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.00"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"14.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"36.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"8.50"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"4.50"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.30"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"5.00"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"23.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"14.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.87"
                  },
                  {
                    "value":"No",
                    "odd":"1.90"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.75"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.73"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.60"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.20"
                  },
                  {
                    "value":"Away +1",
                    "odd":"8.50"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.50"
                  },
                  {
                    "value":"Home +2",
                    "odd":"1.05"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"11.00"
                  },
                  {
                    "value":"Away +2",
                    "odd":"21.00"
                  },
                  {
                    "value":"Home -2",
                    "odd":"9.50"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"6.00"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.20"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"0:11",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:0",
                    "odd":"4.20"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"8.50"
                  },
                  {
                    "value":"3:0",
                    "odd":"15.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"15.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"23.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"81.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"36.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"71.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"151.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"101.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"101.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"8.50"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"14.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"67.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"11.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"15.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"41.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"126.00"
                  },
                  {
                    "value":"0:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"13.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"26.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"91.00"
                  },
                  {
                    "value":"1:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"36.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"101.00"
                  },
                  {
                    "value":"2:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"4:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"4:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"9:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"9:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"10:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"9:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"10:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"11:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:8",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:8",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:8",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:9",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:10",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:9",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:8",
                    "odd":"201.00"
                  },
                  {
                    "value":"4:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:10",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:9",
                    "odd":"201.00"
                  }
                ]
              },
              {
                "label_id":11,
                "label_name":"Highest Scoring Half",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"3.25"
                  },
                  {
                    "value":"1st Half",
                    "odd":"3.30"
                  },
                  {
                    "value":"2nd Half",
                    "odd":"2.20"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.00"
                  },
                  {
                    "value":"2:0",
                    "odd":"12.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"19.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"36.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"91.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"276.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"251.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"476.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.75"
                  },
                  {
                    "value":"1:1",
                    "odd":"7.50"
                  },
                  {
                    "value":"2:2",
                    "odd":"67.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"21.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"101.00"
                  },
                  {
                    "value":"0:5",
                    "odd":"476.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"26.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"176.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"476.00"
                  }
                ]
              },
              {
                "label_id":62,
                "label_name":"Correct Score - Second Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.33"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.50"
                  },
                  {
                    "value":"2:1",
                    "odd":"14.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"26.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"46.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"126.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"101.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"251.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"426.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"3.60"
                  },
                  {
                    "value":"1:1",
                    "odd":"7.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"41.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"15.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"71.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"376.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"18.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"81.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"401.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"176.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.20"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.30"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.73"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.70"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.10"
                  },
                  {
                    "value":"Away",
                    "odd":"4.50"
                  }
                ]
              },
              {
                "label_id":14,
                "label_name":"Team To Score First",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.67"
                  },
                  {
                    "value":"Draw",
                    "odd":"8.50"
                  },
                  {
                    "value":"Away",
                    "odd":"2.50"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"10.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.03"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.10"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.67"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"34.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.00"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"5.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.14"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.20"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"3.75"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"34.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.00"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"34.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.00"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"34.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.00"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"8.50"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.44"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.63"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"34.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.00"
                  }
                ]
              },
              {
                "label_id":18,
                "label_name":"Handicap Result - First Half",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"8.50"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.40"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.40"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.17"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.67"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.40"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"4.50"
                  },
                  {
                    "value":"No",
                    "odd":"1.17"
                  }
                ]
              },
              {
                "label_id":35,
                "label_name":"Both Teams To Score - Second Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.50"
                  },
                  {
                    "value":"No",
                    "odd":"1.25"
                  }
                ]
              },
              {
                "label_id":36,
                "label_name":"Win To Nil",
                "values":[
                  {
                    "value":"Home",
                    "odd":"3.40"
                  },
                  {
                    "value":"Away",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.95"
                  },
                  {
                    "value":"Even",
                    "odd":"1.75"
                  }
                ]
              },
              {
                "label_id":24,
                "label_name":"Results\/Both Teams Score",
                "values":[
                  {
                    "value":"Home\/Yes",
                    "odd":"4.33"
                  },
                  {
                    "value":"Away\/Yes",
                    "odd":"7.50"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over 3.5",
                    "odd":"6.00"
                  },
                  {
                    "value":"Draw\/Over 3.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away\/Over 3.5",
                    "odd":"13.00"
                  },
                  {
                    "value":"Home\/Under 3.5",
                    "odd":"2.63"
                  },
                  {
                    "value":"Draw\/Under 3.5",
                    "odd":"3.75"
                  },
                  {
                    "value":"Away\/Under 3.5",
                    "odd":"4.50"
                  },
                  {
                    "value":"Home\/Over 1.5",
                    "odd":"2.60"
                  },
                  {
                    "value":"Away\/Over 1.5",
                    "odd":"5.00"
                  },
                  {
                    "value":"Draw\/Over 1.5",
                    "odd":"4.50"
                  },
                  {
                    "value":"Draw\/Under 1.5",
                    "odd":"8.50"
                  },
                  {
                    "value":"Home\/Under 1.5",
                    "odd":"7.00"
                  },
                  {
                    "value":"Away\/Under 1.5",
                    "odd":"11.00"
                  },
                  {
                    "value":"Home\/Over 4.5",
                    "odd":"11.00"
                  },
                  {
                    "value":"Draw\/Over 4.5",
                    "odd":"67.00"
                  },
                  {
                    "value":"Away\/Over 4.5",
                    "odd":"20.00"
                  },
                  {
                    "value":"Home\/Under 4.5",
                    "odd":"2.30"
                  },
                  {
                    "value":"Draw\/Under 4.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Away\/Under 4.5",
                    "odd":"4.33"
                  },
                  {
                    "value":"Home\/Over 2.5",
                    "odd":"3.40"
                  },
                  {
                    "value":"Draw\/Over 2.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away\/Over 2.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Home\/Under 2.5",
                    "odd":"4.33"
                  },
                  {
                    "value":"Draw\/Under 2.5",
                    "odd":"3.75"
                  },
                  {
                    "value":"Away\/Under 2.5",
                    "odd":"7.50"
                  }
                ]
              },
              {
                "label_id":45,
                "label_name":"Corners Over Under",
                "values":[
                  {
                    "value":"Over 4.5",
                    "odd":"1.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"21.00"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"13.00"
                  },
                  {
                    "value":"Over 6.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Under 6.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Over 9.5",
                    "odd":"1.45"
                  },
                  {
                    "value":"Under 9.5",
                    "odd":"2.55"
                  },
                  {
                    "value":"Over 10.5",
                    "odd":"1.85"
                  },
                  {
                    "value":"Under 10.5",
                    "odd":"1.85"
                  },
                  {
                    "value":"Over 7.5",
                    "odd":"1.11"
                  },
                  {
                    "value":"Under 7.5",
                    "odd":"5.50"
                  },
                  {
                    "value":"Over 11.5",
                    "odd":"2.25"
                  },
                  {
                    "value":"Under 11.5",
                    "odd":"1.57"
                  },
                  {
                    "value":"Over 8.5",
                    "odd":"1.25"
                  },
                  {
                    "value":"Under 8.5",
                    "odd":"3.60"
                  },
                  {
                    "value":"Over 13.5",
                    "odd":"4.50"
                  },
                  {
                    "value":"Under 13.5",
                    "odd":"1.17"
                  },
                  {
                    "value":"Over 12.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Under 12.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 15.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Under 15.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Over 14.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Under 14.5",
                    "odd":"1.08"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":15,
            "bookmaker_name":"Interwetten",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.05"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.30"
                  },
                  {
                    "value":"Away",
                    "odd":"3.75"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.45"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.55"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.05"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.75"
                  },
                  {
                    "value":"Home +0.5",
                    "odd":"1.27"
                  },
                  {
                    "value":"Away +0.5",
                    "odd":"3.75"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"4.05"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.22"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.40"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.27"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.32"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.10"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"6.90"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"1.97"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.73"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"10.00"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"2.75"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.40"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"6.30"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.40"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.75"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"13.50"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"46.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"7.60"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"4.45"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"2.95"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"4.70"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"30.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"14.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"5.70"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.77"
                  },
                  {
                    "value":"No",
                    "odd":"1.93"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"4.05"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.75"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.65"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.27"
                  },
                  {
                    "value":"Away +1",
                    "odd":"9.40"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.20"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"6.60"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.40"
                  },
                  {
                    "value":"2:1",
                    "odd":"8.10"
                  },
                  {
                    "value":"3:0",
                    "odd":"19.50"
                  },
                  {
                    "value":"3:1",
                    "odd":"17.50"
                  },
                  {
                    "value":"3:2",
                    "odd":"30.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"55.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"49.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"85.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"100.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"100.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"100.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"100.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"10.00"
                  },
                  {
                    "value":"1:1",
                    "odd":"5.80"
                  },
                  {
                    "value":"2:2",
                    "odd":"14.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"75.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"100.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"9.80"
                  },
                  {
                    "value":"0:2",
                    "odd":"20.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"60.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"100.00"
                  },
                  {
                    "value":"0:5",
                    "odd":"100.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"12.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"37.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"100.00"
                  },
                  {
                    "value":"1:5",
                    "odd":"100.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"43.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"100.00"
                  },
                  {
                    "value":"2:5",
                    "odd":"100.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"100.00"
                  }
                ]
              },
              {
                "label_id":11,
                "label_name":"Highest Scoring Half",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"3.10"
                  },
                  {
                    "value":"1st Half",
                    "odd":"2.70"
                  },
                  {
                    "value":"2nd Half",
                    "odd":"2.05"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.00"
                  },
                  {
                    "value":"2:0",
                    "odd":"10.50"
                  },
                  {
                    "value":"2:1",
                    "odd":"19.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"47.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"90.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.75"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.40"
                  },
                  {
                    "value":"2:2",
                    "odd":"75.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"5.80"
                  },
                  {
                    "value":"0:2",
                    "odd":"22.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"100.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"28.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"100.00"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.60"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.05"
                  },
                  {
                    "value":"Away",
                    "odd":"4.10"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.20"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.60"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.40"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"4.10"
                  },
                  {
                    "value":"No",
                    "odd":"1.20"
                  }
                ]
              },
              {
                "label_id":35,
                "label_name":"Both Teams To Score - Second Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.35"
                  },
                  {
                    "value":"No",
                    "odd":"1.28"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.87"
                  },
                  {
                    "value":"Even",
                    "odd":"1.83"
                  }
                ]
              },
              {
                "label_id":38,
                "label_name":"Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.20"
                  },
                  {
                    "value":3,
                    "odd":"3.80"
                  },
                  {
                    "value":4,
                    "odd":"5.70"
                  },
                  {
                    "value":1,
                    "odd":"4.05"
                  },
                  {
                    "value":0,
                    "odd":"10.00"
                  },
                  {
                    "value":"more 5",
                    "odd":"6.90"
                  }
                ]
              },
              {
                "label_id":42,
                "label_name":"Second Half Exact Goals Number",
                "values":[
                  {
                    "value":1,
                    "odd":"2.50"
                  },
                  {
                    "value":0,
                    "odd":"3.30"
                  },
                  {
                    "value":"more 2",
                    "odd":"2.25"
                  }
                ]
              },
              {
                "label_id":24,
                "label_name":"Results\/Both Teams Score",
                "values":[
                  {
                    "value":"Home\/Yes",
                    "odd":"4.15"
                  },
                  {
                    "value":"Draw\/Yes",
                    "odd":"4.20"
                  },
                  {
                    "value":"Away\/Yes",
                    "odd":"7.10"
                  },
                  {
                    "value":"Home\/No",
                    "odd":"3.30"
                  },
                  {
                    "value":"Draw\/No",
                    "odd":"10.00"
                  },
                  {
                    "value":"Away\/No",
                    "odd":"6.30"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over ",
                    "odd":"3.25"
                  },
                  {
                    "value":"Draw\/Over ",
                    "odd":"12.50"
                  },
                  {
                    "value":"Away\/Over ",
                    "odd":"6.30"
                  },
                  {
                    "value":"Home\/Under ",
                    "odd":"4.25"
                  },
                  {
                    "value":"Draw\/Under ",
                    "odd":"3.90"
                  },
                  {
                    "value":"Away\/Under ",
                    "odd":"7.10"
                  }
                ]
              },
              {
                "label_id":55,
                "label_name":"Corners 1x2",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.40"
                  },
                  {
                    "value":"Draw",
                    "odd":"8.50"
                  },
                  {
                    "value":"Away",
                    "odd":"3.25"
                  }
                ]
              },
              {
                "label_id":46,
                "label_name":"Exact Goals Number - First Half",
                "values":[
                  {
                    "value":1,
                    "odd":"2.40"
                  },
                  {
                    "value":0,
                    "odd":"2.75"
                  },
                  {
                    "value":"more 2",
                    "odd":"2.75"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":10,
            "bookmaker_name":"Ladbrokes",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.05"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.30"
                  },
                  {
                    "value":"Away",
                    "odd":"3.70"
                  }
                ]
              },
              {
                "label_id":2,
                "label_name":"Home\/Away",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.50"
                  },
                  {
                    "value":"Away",
                    "odd":"2.50"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.35"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.40"
                  },
                  {
                    "value":"Away",
                    "odd":"3.70"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.60"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.25"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.00"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"7.50"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.05"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.70"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"8.50"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"17.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.03"
                  },
                  {
                    "value":"Over 6.5",
                    "odd":"46.00"
                  },
                  {
                    "value":"Under 6.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 7.5",
                    "odd":"126.00"
                  },
                  {
                    "value":"Over 8.5",
                    "odd":"201.00"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"34.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"126.00"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"9.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.50"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.45"
                  }
                ]
              },
              {
                "label_id":26,
                "label_name":"Goals Over\/Under - Second Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.10"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.67"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"41.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.60"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.17"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.25"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"3.70"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"126.00"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"41.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"7.50"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"4.60"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.40"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"4.60"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"26.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.83"
                  },
                  {
                    "value":"No",
                    "odd":"1.87"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.80"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.75"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.60"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.28"
                  },
                  {
                    "value":"Away +1",
                    "odd":"9.00"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.00"
                  },
                  {
                    "value":"Home +2",
                    "odd":"1.06"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away +2",
                    "odd":"29.00"
                  },
                  {
                    "value":"Home -2",
                    "odd":"9.00"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"5.80"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.22"
                  },
                  {
                    "value":"Home -3",
                    "odd":"26.00"
                  },
                  {
                    "value":"Draw -3",
                    "odd":"13.00"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.06"
                  },
                  {
                    "value":"Home +3",
                    "odd":"1.01"
                  },
                  {
                    "value":"Draw +3",
                    "odd":"34.00"
                  },
                  {
                    "value":"Away +3",
                    "odd":"101.00"
                  },
                  {
                    "value":"Home -4",
                    "odd":"81.00"
                  },
                  {
                    "value":"Away -4",
                    "odd":"1.02"
                  },
                  {
                    "value":"Draw -4",
                    "odd":"34.00"
                  },
                  {
                    "value":"Home -5",
                    "odd":"201.00"
                  },
                  {
                    "value":"Draw -5",
                    "odd":"101.00"
                  },
                  {
                    "value":"Draw +4",
                    "odd":"126.00"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"6.50"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"8.50"
                  },
                  {
                    "value":"3:0",
                    "odd":"17.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"17.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"29.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"46.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"41.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"71.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"151.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"126.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"101.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"151.00"
                  },
                  {
                    "value":"6:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"8.50"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"15.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"67.00"
                  }
                ]
              },
              {
                "label_id":11,
                "label_name":"Highest Scoring Half",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"3.25"
                  },
                  {
                    "value":"1st Half",
                    "odd":"3.40"
                  },
                  {
                    "value":"2nd Half",
                    "odd":"1.95"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.00"
                  },
                  {
                    "value":"2:0",
                    "odd":"12.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"23.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"51.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"91.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.45"
                  },
                  {
                    "value":"1:1",
                    "odd":"8.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"81.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.25"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.33"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.75"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.75"
                  },
                  {
                    "value":"Draw",
                    "odd":"1.95"
                  },
                  {
                    "value":"Away",
                    "odd":"4.20"
                  }
                ]
              },
              {
                "label_id":14,
                "label_name":"Team To Score First",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.73"
                  },
                  {
                    "value":"Draw",
                    "odd":"8.50"
                  },
                  {
                    "value":"Away",
                    "odd":"2.45"
                  }
                ]
              },
              {
                "label_id":32,
                "label_name":"Win Both Halves",
                "values":[
                  {
                    "value":"Home",
                    "odd":"6.50"
                  },
                  {
                    "value":"Away",
                    "odd":"17.00"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"13.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.15"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.61"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"41.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.75"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.17"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.22"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"3.80"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"126.00"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"34.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"126.00"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"9.50"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.44"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.55"
                  }
                ]
              },
              {
                "label_id":18,
                "label_name":"Handicap Result - First Half",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"9.00"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.40"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.60"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.20"
                  },
                  {
                    "value":"Away +1",
                    "odd":"19.00"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.00"
                  },
                  {
                    "value":"Home +2",
                    "odd":"1.03"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"21.00"
                  },
                  {
                    "value":"Away +2",
                    "odd":"101.00"
                  },
                  {
                    "value":"Home -2",
                    "odd":"41.00"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"11.00"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.06"
                  },
                  {
                    "value":"Home -3",
                    "odd":"151.00"
                  },
                  {
                    "value":"Draw -3",
                    "odd":"46.00"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.01"
                  },
                  {
                    "value":"Draw +3",
                    "odd":"101.00"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.14"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.67"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.33"
                  }
                ]
              },
              {
                "label_id":33,
                "label_name":"Double Chance - Second Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.18"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.44"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.44"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"5.00"
                  },
                  {
                    "value":"No",
                    "odd":"1.15"
                  }
                ]
              },
              {
                "label_id":35,
                "label_name":"Both Teams To Score - Second Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.20"
                  },
                  {
                    "value":"No",
                    "odd":"1.30"
                  }
                ]
              },
              {
                "label_id":36,
                "label_name":"Win To Nil",
                "values":[
                  {
                    "value":"Home",
                    "odd":"3.25"
                  },
                  {
                    "value":"Away",
                    "odd":"6.00"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.91"
                  },
                  {
                    "value":"Even",
                    "odd":"1.80"
                  }
                ]
              },
              {
                "label_id":38,
                "label_name":"Exact Goals Number",
                "values":[
                  {
                    "value":2,
                    "odd":"3.40"
                  },
                  {
                    "value":3,
                    "odd":"4.00"
                  },
                  {
                    "value":4,
                    "odd":"6.00"
                  },
                  {
                    "value":1,
                    "odd":"4.20"
                  },
                  {
                    "value":0,
                    "odd":"8.50"
                  },
                  {
                    "value":5,
                    "odd":"11.00"
                  },
                  {
                    "value":6,
                    "odd":"26.00"
                  },
                  {
                    "value":"more 7",
                    "odd":"46.00"
                  }
                ]
              },
              {
                "label_id":39,
                "label_name":"To Win Either Half",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.57"
                  },
                  {
                    "value":"Away",
                    "odd":"2.30"
                  }
                ]
              },
              {
                "label_id":40,
                "label_name":"Home Team Exact Goals Number",
                "values":[
                  {
                    "value":1,
                    "odd":"2.60"
                  },
                  {
                    "value":0,
                    "odd":"3.80"
                  },
                  {
                    "value":"more 2",
                    "odd":"2.15"
                  }
                ]
              },
              {
                "label_id":41,
                "label_name":"Away Team Exact Goals Number",
                "values":[
                  {
                    "value":1,
                    "odd":"2.40"
                  },
                  {
                    "value":0,
                    "odd":"2.55"
                  },
                  {
                    "value":"more 2",
                    "odd":"3.20"
                  }
                ]
              },
              {
                "label_id":24,
                "label_name":"Results\/Both Teams Score",
                "values":[
                  {
                    "value":"Home\/Yes",
                    "odd":"4.25"
                  },
                  {
                    "value":"Draw\/Yes",
                    "odd":"4.33"
                  },
                  {
                    "value":"Away\/Yes",
                    "odd":"7.00"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over 3.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Draw\/Over 3.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away\/Over 3.5",
                    "odd":"13.00"
                  },
                  {
                    "value":"Home\/Under 3.5",
                    "odd":"2.62"
                  },
                  {
                    "value":"Draw\/Under 3.5",
                    "odd":"3.80"
                  },
                  {
                    "value":"Away\/Under 3.5",
                    "odd":"4.33"
                  },
                  {
                    "value":"Home\/Over 1.5",
                    "odd":"2.60"
                  },
                  {
                    "value":"Away\/Over 1.5",
                    "odd":"4.80"
                  },
                  {
                    "value":"Draw\/Over 1.5",
                    "odd":"4.33"
                  },
                  {
                    "value":"Draw\/Under 1.5",
                    "odd":"8.50"
                  },
                  {
                    "value":"Home\/Under 1.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Away\/Under 1.5",
                    "odd":"9.50"
                  },
                  {
                    "value":"Home\/Over 4.5",
                    "odd":"11.00"
                  },
                  {
                    "value":"Draw\/Over 4.5",
                    "odd":"61.00"
                  },
                  {
                    "value":"Away\/Over 4.5",
                    "odd":"23.00"
                  },
                  {
                    "value":"Home\/Under 4.5",
                    "odd":"2.30"
                  },
                  {
                    "value":"Draw\/Under 4.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Away\/Under 4.5",
                    "odd":"3.90"
                  },
                  {
                    "value":"Home\/Over 2.5",
                    "odd":"3.40"
                  },
                  {
                    "value":"Draw\/Over 2.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away\/Over 2.5",
                    "odd":"6.00"
                  },
                  {
                    "value":"Home\/Under 2.5",
                    "odd":"4.00"
                  },
                  {
                    "value":"Draw\/Under 2.5",
                    "odd":"3.80"
                  },
                  {
                    "value":"Away\/Under 2.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Home\/Over 5.5",
                    "odd":"29.00"
                  },
                  {
                    "value":"Draw\/Over 5.5",
                    "odd":"61.00"
                  },
                  {
                    "value":"Away\/Over 5.5",
                    "odd":"67.00"
                  },
                  {
                    "value":"Home\/Under 5.5",
                    "odd":"2.05"
                  },
                  {
                    "value":"Draw\/Under 5.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Away\/Under 5.5",
                    "odd":"3.60"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":13,
            "bookmaker_name":"188Bet",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.85"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.60"
                  },
                  {
                    "value":"Away",
                    "odd":"4.50"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home -0.25",
                    "odd":"1.62"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.44"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"1.86"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"2.07"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.13"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.81"
                  },
                  {
                    "value":"Home -1",
                    "odd":"2.61"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.55"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 2.5",
                    "odd":"2.02"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.88"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.79"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"2.12"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.29"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.67"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.53"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.58"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 1.25",
                    "odd":"2.35"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"1.64"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.62"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"1.98"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.92"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"18.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"51.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"10.00"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"5.00"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"2.87"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"5.00"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"31.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"17.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"7.10"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.83"
                  },
                  {
                    "value":"No",
                    "odd":"1.99"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.40"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.86"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.50"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"6.50"
                  },
                  {
                    "value":"2:0",
                    "odd":"8.90"
                  },
                  {
                    "value":"2:1",
                    "odd":"8.40"
                  },
                  {
                    "value":"3:0",
                    "odd":"18.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"15.50"
                  },
                  {
                    "value":"3:2",
                    "odd":"26.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"46.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"36.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"61.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"131.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"10.00"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.90"
                  },
                  {
                    "value":"2:2",
                    "odd":"16.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"71.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"351.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"11.50"
                  },
                  {
                    "value":"0:2",
                    "odd":"26.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"91.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"351.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"14.50"
                  },
                  {
                    "value":"1:3",
                    "odd":"46.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"211.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"46.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"161.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"271.00"
                  }
                ]
              },
              {
                "label_id":11,
                "label_name":"Highest Scoring Half",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"3.40"
                  },
                  {
                    "value":"1st Half",
                    "odd":"2.88"
                  },
                  {
                    "value":"2nd Half",
                    "odd":"2.08"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"3.50"
                  },
                  {
                    "value":"2:0",
                    "odd":"10.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"19.50"
                  },
                  {
                    "value":"3:0",
                    "odd":"41.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"71.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.49"
                  },
                  {
                    "value":"1:1",
                    "odd":"7.50"
                  },
                  {
                    "value":"2:2",
                    "odd":"71.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"5.90"
                  },
                  {
                    "value":"0:2",
                    "odd":"26.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"31.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"201.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.22"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.31"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"2.00"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.51"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.14"
                  },
                  {
                    "value":"Away",
                    "odd":"4.70"
                  }
                ]
              },
              {
                "label_id":14,
                "label_name":"Team To Score First",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.60"
                  },
                  {
                    "value":"Draw",
                    "odd":"10.00"
                  },
                  {
                    "value":"Away",
                    "odd":"2.53"
                  }
                ]
              },
              {
                "label_id":15,
                "label_name":"Team To Score Last",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.60"
                  },
                  {
                    "value":"Away",
                    "odd":"2.53"
                  }
                ]
              },
              {
                "label_id":32,
                "label_name":"Win Both Halves",
                "values":[
                  {
                    "value":"Home",
                    "odd":"6.00"
                  },
                  {
                    "value":"Away",
                    "odd":"26.00"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"1.97"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.85"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 1",
                    "odd":"2.13"
                  },
                  {
                    "value":"Under 1",
                    "odd":"1.70"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.74"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.08"
                  }
                ]
              },
              {
                "label_id":19,
                "label_name":"Asian Handicap First Half",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.53"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.58"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"2.02"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"1.88"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.53"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.55"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"4.80"
                  },
                  {
                    "value":"No",
                    "odd":"1.16"
                  }
                ]
              },
              {
                "label_id":36,
                "label_name":"Win To Nil",
                "values":[
                  {
                    "value":"Home",
                    "odd":"3.25"
                  },
                  {
                    "value":"Away",
                    "odd":"8.30"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.96"
                  },
                  {
                    "value":"Even",
                    "odd":"1.92"
                  }
                ]
              },
              {
                "label_id":22,
                "label_name":"Odd\/Even - First Half",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.14"
                  },
                  {
                    "value":"Even",
                    "odd":"1.73"
                  }
                ]
              },
              {
                "label_id":38,
                "label_name":"Exact Goals Number",
                "values":[
                  {
                    "value":"more 7",
                    "odd":"21.00"
                  }
                ]
              },
              {
                "label_id":39,
                "label_name":"To Win Either Half",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.47"
                  },
                  {
                    "value":"Away",
                    "odd":"2.70"
                  }
                ]
              },
              {
                "label_id":24,
                "label_name":"Results\/Both Teams Score",
                "values":[
                  {
                    "value":"Home\/Yes",
                    "odd":"3.75"
                  },
                  {
                    "value":"Draw\/Yes",
                    "odd":"4.40"
                  },
                  {
                    "value":"Away\/Yes",
                    "odd":"8.60"
                  },
                  {
                    "value":"Home\/No",
                    "odd":"3.25"
                  },
                  {
                    "value":"Draw\/No",
                    "odd":"10.00"
                  },
                  {
                    "value":"Away\/No",
                    "odd":"8.30"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over 3.5",
                    "odd":"5.40"
                  },
                  {
                    "value":"Draw\/Over 3.5",
                    "odd":"14.00"
                  },
                  {
                    "value":"Away\/Over 3.5",
                    "odd":"17.50"
                  },
                  {
                    "value":"Home\/Under 3.5",
                    "odd":"2.50"
                  },
                  {
                    "value":"Draw\/Under 3.5",
                    "odd":"4.40"
                  },
                  {
                    "value":"Away\/Under 3.5",
                    "odd":"5.40"
                  },
                  {
                    "value":"Home\/Over 1.5",
                    "odd":"2.26"
                  },
                  {
                    "value":"Away\/Over 1.5",
                    "odd":"6.20"
                  },
                  {
                    "value":"Draw\/Over 1.5",
                    "odd":"4.40"
                  },
                  {
                    "value":"Draw\/Under 1.5",
                    "odd":"10.00"
                  },
                  {
                    "value":"Home\/Under 1.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Away\/Under 1.5",
                    "odd":"11.50"
                  },
                  {
                    "value":"Home\/Over 4.5",
                    "odd":"9.40"
                  },
                  {
                    "value":"Draw\/Over 4.5",
                    "odd":"71.00"
                  },
                  {
                    "value":"Away\/Over 4.5",
                    "odd":"26.00"
                  },
                  {
                    "value":"Home\/Under 4.5",
                    "odd":"2.10"
                  },
                  {
                    "value":"Draw\/Under 4.5",
                    "odd":"3.55"
                  },
                  {
                    "value":"Away\/Under 4.5",
                    "odd":"4.85"
                  },
                  {
                    "value":"Home\/Over 2.5",
                    "odd":"2.94"
                  },
                  {
                    "value":"Draw\/Over 2.5",
                    "odd":"14.00"
                  },
                  {
                    "value":"Away\/Over 2.5",
                    "odd":"7.80"
                  },
                  {
                    "value":"Home\/Under 2.5",
                    "odd":"4.15"
                  },
                  {
                    "value":"Draw\/Under 2.5",
                    "odd":"4.40"
                  },
                  {
                    "value":"Away\/Under 2.5",
                    "odd":"8.80"
                  }
                ]
              },
              {
                "label_id":46,
                "label_name":"Exact Goals Number - First Half",
                "values":[
                  {
                    "value":2,
                    "odd":"4.40"
                  },
                  {
                    "value":1,
                    "odd":"2.62"
                  },
                  {
                    "value":0,
                    "odd":"2.49"
                  },
                  {
                    "value":"more 3",
                    "odd":"7.10"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":4,
            "bookmaker_name":"Pinnacle",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.08"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.32"
                  },
                  {
                    "value":"Away",
                    "odd":"4.02"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.48"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.88"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"1.78"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.17"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.08"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.85"
                  },
                  {
                    "value":"Home +0.5",
                    "odd":"1.29"
                  },
                  {
                    "value":"Away +0.5",
                    "odd":"4.02"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.46"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.62"
                  },
                  {
                    "value":"Home -1",
                    "odd":"3.23"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.40"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"4.22"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.27"
                  },
                  {
                    "value":"Home -2.5",
                    "odd":"8.78"
                  },
                  {
                    "value":"Away -2.5",
                    "odd":"1.10"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.88"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.36"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.44"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"8.50"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.11"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.81"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.85"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"2.07"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.45"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.62"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"1.44"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"3.00"
                  },
                  {
                    "value":"Over 3.0",
                    "odd":"2.97"
                  },
                  {
                    "value":"Under 3.0",
                    "odd":"1.45"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.57"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.57"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.39"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.45"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.90"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"2.62"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"1.53"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.63"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"2.05"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.85"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.78"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.22"
                  },
                  {
                    "value":"Away",
                    "odd":"4.45"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"2.20"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.74"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 1",
                    "odd":"2.23"
                  },
                  {
                    "value":"Under 1",
                    "odd":"1.72"
                  }
                ]
              },
              {
                "label_id":19,
                "label_name":"Asian Handicap First Half",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.58"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.54"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"2.19"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"1.76"
                  },
                  {
                    "value":"Home +0.25",
                    "odd":"1.35"
                  },
                  {
                    "value":"Away +0.25",
                    "odd":"3.51"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.78"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.50"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"3.79"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.31"
                  }
                ]
              },
              {
                "label_id":45,
                "label_name":"Corners Over Under",
                "values":[
                  {
                    "value":"Over 9.5",
                    "odd":"1.46"
                  },
                  {
                    "value":"Under 9.5",
                    "odd":"2.71"
                  },
                  {
                    "value":"Over 10.5",
                    "odd":"1.74"
                  },
                  {
                    "value":"Under 10.5",
                    "odd":"2.11"
                  },
                  {
                    "value":"Over 11.5",
                    "odd":"2.29"
                  },
                  {
                    "value":"Under 11.5",
                    "odd":"1.62"
                  },
                  {
                    "value":"Over 10",
                    "odd":"1.54"
                  },
                  {
                    "value":"Under 10",
                    "odd":"2.48"
                  },
                  {
                    "value":"Over 11",
                    "odd":"2.00"
                  },
                  {
                    "value":"Under 11",
                    "odd":"1.83"
                  },
                  {
                    "value":"Over 12",
                    "odd":"2.82"
                  },
                  {
                    "value":"Under 12",
                    "odd":"1.43"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":5,
            "bookmaker_name":"Sport Betting Odds",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.09"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.30"
                  },
                  {
                    "value":"Away",
                    "odd":"3.85"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.49"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.85"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"1.80"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.17"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.09"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.86"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.49"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.62"
                  },
                  {
                    "value":"Home -1",
                    "odd":"3.08"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.43"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 2.5",
                    "odd":"2.09"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.83"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.84"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"2.08"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.64"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.57"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.53"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 0.75",
                    "odd":"1.62"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"2.03"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.87"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"36.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"9.00"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"4.80"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"2.92"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"5.00"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"29.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"7.20"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"6.60"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.75"
                  },
                  {
                    "value":"2:1",
                    "odd":"7.80"
                  },
                  {
                    "value":"3:0",
                    "odd":"22.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"17.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"28.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"65.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"50.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"80.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"205.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"9.00"
                  },
                  {
                    "value":"1:1",
                    "odd":"5.80"
                  },
                  {
                    "value":"2:2",
                    "odd":"15.50"
                  },
                  {
                    "value":"3:3",
                    "odd":"90.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"220.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"10.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"23.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"75.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"215.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"13.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"42.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"185.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"50.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"205.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"220.00"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"3.75"
                  },
                  {
                    "value":"2:0",
                    "odd":"14.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"18.50"
                  },
                  {
                    "value":"3:0",
                    "odd":"80.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"105.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"210.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.01"
                  },
                  {
                    "value":"1:1",
                    "odd":"5.60"
                  },
                  {
                    "value":"2:2",
                    "odd":"60.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"225.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"5.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"25.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"165.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"28.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"210.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"225.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.28"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.36"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.78"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.83"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.06"
                  },
                  {
                    "value":"Away",
                    "odd":"4.20"
                  }
                ]
              },
              {
                "label_id":19,
                "label_name":"Asian Handicap First Half",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.62"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.38"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"2.13"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"1.78"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.00"
                  },
                  {
                    "value":"Even",
                    "odd":"1.90"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":11,
            "bookmaker_name":"1xBet",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.14"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.28"
                  },
                  {
                    "value":"Away",
                    "odd":"4.02"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.44"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.44"
                  },
                  {
                    "value":"Away",
                    "odd":"3.94"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.50"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.83"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"1.80"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.13"
                  },
                  {
                    "value":"Home +0.25",
                    "odd":"1.34"
                  },
                  {
                    "value":"Away +0.25",
                    "odd":"3.28"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.58"
                  },
                  {
                    "value":"Home +0.75",
                    "odd":"1.17"
                  },
                  {
                    "value":"Away +0.75",
                    "odd":"5.05"
                  },
                  {
                    "value":"Home -1",
                    "odd":"3.04"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.38"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.08"
                  },
                  {
                    "value":"Away +1",
                    "odd":"8.10"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"3.78"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.25"
                  },
                  {
                    "value":"Home +1.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Away +1.5",
                    "odd":"8.90"
                  },
                  {
                    "value":"Home -1.25",
                    "odd":"3.46"
                  },
                  {
                    "value":"Away -1.25",
                    "odd":"1.31"
                  },
                  {
                    "value":"Home -1.75",
                    "odd":"5.00"
                  },
                  {
                    "value":"Away -1.75",
                    "odd":"1.17"
                  },
                  {
                    "value":"Home +1.25",
                    "odd":"1.07"
                  },
                  {
                    "value":"Away +1.25",
                    "odd":"8.50"
                  },
                  {
                    "value":"Home +1.75",
                    "odd":"1.02"
                  },
                  {
                    "value":"Away +1.75",
                    "odd":"14.00"
                  },
                  {
                    "value":"Home -2",
                    "odd":"7.20"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.07"
                  },
                  {
                    "value":"Home -3",
                    "odd":"16.00"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.01"
                  },
                  {
                    "value":"Home -2.5",
                    "odd":"7.70"
                  },
                  {
                    "value":"Away -2.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Home -2.25",
                    "odd":"7.80"
                  },
                  {
                    "value":"Away -2.25",
                    "odd":"1.08"
                  },
                  {
                    "value":"Home -2.75",
                    "odd":"12.50"
                  },
                  {
                    "value":"Away -2.75",
                    "odd":"1.03"
                  },
                  {
                    "value":"Home -3.5",
                    "odd":"16.00"
                  },
                  {
                    "value":"Away -3.5",
                    "odd":"1.01"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.50"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.29"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.18"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"6.55"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.11"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.81"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.05"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"7.70"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"15.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.86"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"2.05"
                  },
                  {
                    "value":"Over 3.25",
                    "odd":"3.20"
                  },
                  {
                    "value":"Under 3.25",
                    "odd":"1.34"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.36"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.59"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"1.23"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"4.16"
                  },
                  {
                    "value":"Over 4.25",
                    "odd":"7.20"
                  },
                  {
                    "value":"Under 4.25",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"1.43"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"2.83"
                  },
                  {
                    "value":"Over 3.75",
                    "odd":"4.40"
                  },
                  {
                    "value":"Under 3.75",
                    "odd":"1.21"
                  },
                  {
                    "value":"Over 6.5",
                    "odd":"26.00"
                  },
                  {
                    "value":"Under 6.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.06"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"8.70"
                  },
                  {
                    "value":"Over 3.0",
                    "odd":"2.88"
                  },
                  {
                    "value":"Under 3.0",
                    "odd":"1.42"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.55"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.45"
                  },
                  {
                    "value":"Over 4.0",
                    "odd":"5.95"
                  },
                  {
                    "value":"Under 4.0",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"1.08"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"6.75"
                  },
                  {
                    "value":"Over 5.0",
                    "odd":"11.00"
                  },
                  {
                    "value":"Under 5.0",
                    "odd":"1.00"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"15.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.02"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.95"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.40"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"7.80"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.48"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.65"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"7.10"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"10.50"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.04"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"2.50"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"1.53"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"3.84"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"1.26"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.65"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.24"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"6.45"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"1.11"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"2.01"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.80"
                  }
                ]
              },
              {
                "label_id":26,
                "label_name":"Goals Over\/Under - Second Half",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"10.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.26"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.64"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"5.05"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.17"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.28"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"3.68"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"4.40"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"1.21"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"6.75"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 1.25",
                    "odd":"1.90"
                  },
                  {
                    "value":"Under 1.25",
                    "odd":"1.90"
                  },
                  {
                    "value":"Over 1.75",
                    "odd":"2.75"
                  },
                  {
                    "value":"Under 1.75",
                    "odd":"1.45"
                  },
                  {
                    "value":"Over 3.75",
                    "odd":"15.00"
                  },
                  {
                    "value":"Under 3.75",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 0.75",
                    "odd":"1.37"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"3.10"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"3.84"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"1.26"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"1.53"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"2.50"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"41.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"8.40"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"4.50"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.24"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"4.94"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"29.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"6.80"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.87"
                  },
                  {
                    "value":"No",
                    "odd":"1.89"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.78"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.81"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.76"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.29"
                  },
                  {
                    "value":"Away +1",
                    "odd":"8.90"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.60"
                  },
                  {
                    "value":"Home +2",
                    "odd":"1.06"
                  },
                  {
                    "value":"Draw +2",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away +2",
                    "odd":"23.00"
                  },
                  {
                    "value":"Home -2",
                    "odd":"7.70"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"6.20"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.25"
                  },
                  {
                    "value":"Home -3",
                    "odd":"21.00"
                  },
                  {
                    "value":"Draw -3",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.06"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"2:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"3:9",
                    "odd":"500.00"
                  },
                  {
                    "value":"1:0",
                    "odd":"7.00"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"8.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"18.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"15.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"26.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"46.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"36.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"71.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"126.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"151.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"7.70"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"14.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"71.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"11.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"18.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"46.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"176.00"
                  },
                  {
                    "value":"0:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"12.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"31.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"101.00"
                  },
                  {
                    "value":"1:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"41.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"126.00"
                  },
                  {
                    "value":"2:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"4:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"4:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"10:2",
                    "odd":"500.00"
                  },
                  {
                    "value":"10:3",
                    "odd":"500.00"
                  },
                  {
                    "value":"8:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"9:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"9:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"10:0",
                    "odd":"201.00"
                  },
                  {
                    "value":"9:2",
                    "odd":"201.00"
                  },
                  {
                    "value":"10:1",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:8",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:8",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:8",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"8:6",
                    "odd":"500.00"
                  },
                  {
                    "value":"8:7",
                    "odd":"500.00"
                  },
                  {
                    "value":"9:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"9:6",
                    "odd":"500.00"
                  },
                  {
                    "value":"9:7",
                    "odd":"500.00"
                  },
                  {
                    "value":"9:8",
                    "odd":"500.00"
                  },
                  {
                    "value":"10:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"10:6",
                    "odd":"500.00"
                  },
                  {
                    "value":"10:7",
                    "odd":"500.00"
                  },
                  {
                    "value":"10:8",
                    "odd":"500.00"
                  },
                  {
                    "value":"10:9",
                    "odd":"500.00"
                  },
                  {
                    "value":"6:6",
                    "odd":"500.00"
                  },
                  {
                    "value":"7:7",
                    "odd":"500.00"
                  },
                  {
                    "value":"8:8",
                    "odd":"500.00"
                  },
                  {
                    "value":"9:9",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:8",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:9",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:8",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:9",
                    "odd":"500.00"
                  },
                  {
                    "value":"6:8",
                    "odd":"500.00"
                  },
                  {
                    "value":"6:9",
                    "odd":"500.00"
                  },
                  {
                    "value":"7:8",
                    "odd":"500.00"
                  },
                  {
                    "value":"7:9",
                    "odd":"500.00"
                  },
                  {
                    "value":"8:9",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"6:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"7:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"8:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"9:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"10:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"10:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"6:5",
                    "odd":"201.00"
                  },
                  {
                    "value":"5:6",
                    "odd":"201.00"
                  },
                  {
                    "value":"6:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:3",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"8:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"9:3",
                    "odd":"500.00"
                  },
                  {
                    "value":"7:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"0:9",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:10",
                    "odd":"201.00"
                  },
                  {
                    "value":"1:9",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:8",
                    "odd":"201.00"
                  },
                  {
                    "value":"7:6",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:7",
                    "odd":"500.00"
                  },
                  {
                    "value":"6:7",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"3:10",
                    "odd":"500.00"
                  },
                  {
                    "value":"3:7",
                    "odd":"201.00"
                  },
                  {
                    "value":"9:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"1:10",
                    "odd":"201.00"
                  },
                  {
                    "value":"2:9",
                    "odd":"201.00"
                  }
                ]
              },
              {
                "label_id":11,
                "label_name":"Highest Scoring Half",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"3.54"
                  },
                  {
                    "value":"1st Half",
                    "odd":"3.16"
                  },
                  {
                    "value":"2nd Half",
                    "odd":"2.12"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"3.90"
                  },
                  {
                    "value":"2:0",
                    "odd":"11.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"21.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"36.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"51.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"81.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"71.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"81.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"91.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:3",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.65"
                  },
                  {
                    "value":"1:1",
                    "odd":"8.00"
                  },
                  {
                    "value":"2:2",
                    "odd":"51.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"5.80"
                  },
                  {
                    "value":"0:2",
                    "odd":"21.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"61.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"91.00"
                  },
                  {
                    "value":"0:5",
                    "odd":"91.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"29.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"71.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"91.00"
                  },
                  {
                    "value":"1:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"81.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"2:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"3:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:5",
                    "odd":"500.00"
                  }
                ]
              },
              {
                "label_id":62,
                "label_name":"Correct Score - Second Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.20"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.50"
                  },
                  {
                    "value":"2:1",
                    "odd":"15.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"26.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"41.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"66.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"61.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"71.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"81.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:3",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"3.68"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.50"
                  },
                  {
                    "value":"2:2",
                    "odd":"36.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"17.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"51.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"81.00"
                  },
                  {
                    "value":"0:5",
                    "odd":"91.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"21.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"56.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"81.00"
                  },
                  {
                    "value":"1:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"71.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"2:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"500.00"
                  },
                  {
                    "value":"3:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"4:5",
                    "odd":"500.00"
                  },
                  {
                    "value":"5:5",
                    "odd":"500.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.29"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.39"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.81"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.74"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.03"
                  },
                  {
                    "value":"Away",
                    "odd":"4.64"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"11.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.04"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.14"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.71"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.70"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.19"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.24"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"4.04"
                  },
                  {
                    "value":"Over 2",
                    "odd":"3.60"
                  },
                  {
                    "value":"Under 2",
                    "odd":"1.29"
                  },
                  {
                    "value":"Over 3",
                    "odd":"10.00"
                  },
                  {
                    "value":"Under 3",
                    "odd":"1.05"
                  },
                  {
                    "value":"Over 1",
                    "odd":"1.45"
                  },
                  {
                    "value":"Under 1",
                    "odd":"2.77"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"3.32"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"9.30"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.49"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.62"
                  },
                  {
                    "value":"Over 2",
                    "odd":"7.90"
                  },
                  {
                    "value":"Under 2",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 1",
                    "odd":"2.17"
                  },
                  {
                    "value":"Under 1",
                    "odd":"1.69"
                  }
                ]
              },
              {
                "label_id":18,
                "label_name":"Handicap Result - First Half",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"8.40"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.42"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.68"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.16"
                  },
                  {
                    "value":"Away +1",
                    "odd":"15.00"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.40"
                  },
                  {
                    "value":"Home -2",
                    "odd":"29.00"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"10.00"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.06"
                  }
                ]
              },
              {
                "label_id":19,
                "label_name":"Asian Handicap First Half",
                "values":[
                  {
                    "value":"Home +0",
                    "odd":"1.53"
                  },
                  {
                    "value":"Away +0",
                    "odd":"2.50"
                  },
                  {
                    "value":"Home -0.25",
                    "odd":"2.13"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"1.71"
                  },
                  {
                    "value":"Home +0.25",
                    "odd":"1.33"
                  },
                  {
                    "value":"Away +0.25",
                    "odd":"3.30"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"3.60"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.29"
                  },
                  {
                    "value":"Home -1",
                    "odd":"6.70"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.10"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.02"
                  },
                  {
                    "value":"Away +1",
                    "odd":"14.00"
                  },
                  {
                    "value":"Home -1.5",
                    "odd":"8.40"
                  },
                  {
                    "value":"Away -1.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Home +1.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Away +1.5",
                    "odd":"15.00"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.16"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.72"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.42"
                  }
                ]
              },
              {
                "label_id":33,
                "label_name":"Double Chance - Second Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.22"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.51"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.52"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"4.70"
                  },
                  {
                    "value":"No",
                    "odd":"1.19"
                  }
                ]
              },
              {
                "label_id":35,
                "label_name":"Both Teams To Score - Second Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"3.52"
                  },
                  {
                    "value":"No",
                    "odd":"1.30"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.98"
                  },
                  {
                    "value":"Even",
                    "odd":"1.83"
                  }
                ]
              },
              {
                "label_id":22,
                "label_name":"Odd\/Even - First Half",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.22"
                  },
                  {
                    "value":"Even",
                    "odd":"1.66"
                  }
                ]
              },
              {
                "label_id":23,
                "label_name":"Home Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.97"
                  },
                  {
                    "value":"Even",
                    "odd":"1.83"
                  }
                ]
              },
              {
                "label_id":60,
                "label_name":"Away Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"2.12"
                  },
                  {
                    "value":"Even",
                    "odd":"1.72"
                  }
                ]
              },
              {
                "label_id":55,
                "label_name":"Corners 1x2",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.45"
                  },
                  {
                    "value":"Draw",
                    "odd":"9.60"
                  },
                  {
                    "value":"Away",
                    "odd":"3.60"
                  }
                ]
              },
              {
                "label_id":45,
                "label_name":"Corners Over Under",
                "values":[
                  {
                    "value":"Over 9.5",
                    "odd":"1.39"
                  },
                  {
                    "value":"Under 9.5",
                    "odd":"2.84"
                  },
                  {
                    "value":"Over 10.5",
                    "odd":"1.64"
                  },
                  {
                    "value":"Under 10.5",
                    "odd":"2.26"
                  },
                  {
                    "value":"Over 7.5",
                    "odd":"1.12"
                  },
                  {
                    "value":"Under 7.5",
                    "odd":"5.70"
                  },
                  {
                    "value":"Over 11.5",
                    "odd":"2.06"
                  },
                  {
                    "value":"Under 11.5",
                    "odd":"1.71"
                  },
                  {
                    "value":"Over 8.5",
                    "odd":"1.24"
                  },
                  {
                    "value":"Under 8.5",
                    "odd":"3.80"
                  },
                  {
                    "value":"Over 13.5",
                    "odd":"3.62"
                  },
                  {
                    "value":"Under 13.5",
                    "odd":"1.26"
                  },
                  {
                    "value":"Over 12.5",
                    "odd":"2.70"
                  },
                  {
                    "value":"Under 12.5",
                    "odd":"1.43"
                  },
                  {
                    "value":"Over 15.5",
                    "odd":"7.20"
                  },
                  {
                    "value":"Under 15.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 14.5",
                    "odd":"5.05"
                  },
                  {
                    "value":"Under 14.5",
                    "odd":"1.15"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":12,
            "bookmaker_name":"BetFred",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.05"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.40"
                  },
                  {
                    "value":"Away",
                    "odd":"4.00"
                  }
                ]
              },
              {
                "label_id":2,
                "label_name":"Home\/Away",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.44"
                  },
                  {
                    "value":"Away",
                    "odd":"2.75"
                  }
                ]
              },
              {
                "label_id":3,
                "label_name":"Second Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.38"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.40"
                  },
                  {
                    "value":"Away",
                    "odd":"3.75"
                  }
                ]
              },
              {
                "label_id":4,
                "label_name":"Asian Handicap",
                "values":[
                  {
                    "value":"Home -0.25",
                    "odd":"1.73"
                  },
                  {
                    "value":"Away -0.25",
                    "odd":"2.20"
                  },
                  {
                    "value":"Home -0.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Away -0.5",
                    "odd":"1.85"
                  },
                  {
                    "value":"Home -0.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Away -0.75",
                    "odd":"1.62"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"3.50"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.30"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"1.36"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"7.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.10"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"2.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.80"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.07"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Over 5.5",
                    "odd":"13.00"
                  },
                  {
                    "value":"Under 5.5",
                    "odd":"1.03"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 1.5",
                    "odd":"3.20"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.36"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"7.50"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.08"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.44"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.75"
                  }
                ]
              },
              {
                "label_id":7,
                "label_name":"HT\/FT Double",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"41.00"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"9.00"
                  },
                  {
                    "value":"Draw\/Draw",
                    "odd":"5.00"
                  },
                  {
                    "value":"Home\/Home",
                    "odd":"3.25"
                  },
                  {
                    "value":"Draw\/Home",
                    "odd":"5.00"
                  },
                  {
                    "value":"Away\/Home",
                    "odd":"26.00"
                  },
                  {
                    "value":"Away\/Draw",
                    "odd":"15.00"
                  },
                  {
                    "value":"Away\/Away",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":8,
                "label_name":"Both Teams Score",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.80"
                  },
                  {
                    "value":"No",
                    "odd":"2.00"
                  }
                ]
              },
              {
                "label_id":9,
                "label_name":"Handicap Result",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"3.75"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.80"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.50"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.22"
                  },
                  {
                    "value":"Away +1",
                    "odd":"8.50"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"5.50"
                  },
                  {
                    "value":"Home -2",
                    "odd":"8.50"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"5.50"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.22"
                  },
                  {
                    "value":"Home -3",
                    "odd":"21.00"
                  },
                  {
                    "value":"Draw -3",
                    "odd":"11.00"
                  },
                  {
                    "value":"Away -3",
                    "odd":"1.07"
                  }
                ]
              },
              {
                "label_id":10,
                "label_name":"Exact Score",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"7.00"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.50"
                  },
                  {
                    "value":"2:1",
                    "odd":"8.50"
                  },
                  {
                    "value":"3:0",
                    "odd":"17.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"17.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"29.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"41.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"41.00"
                  },
                  {
                    "value":"4:2",
                    "odd":"67.00"
                  },
                  {
                    "value":"4:3",
                    "odd":"126.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"81.00"
                  },
                  {
                    "value":"5:1",
                    "odd":"81.00"
                  },
                  {
                    "value":"5:2",
                    "odd":"126.00"
                  },
                  {
                    "value":"5:3",
                    "odd":"251.00"
                  },
                  {
                    "value":"5:4",
                    "odd":"301.00"
                  },
                  {
                    "value":"6:0",
                    "odd":"251.00"
                  },
                  {
                    "value":"6:1",
                    "odd":"251.00"
                  },
                  {
                    "value":"6:2",
                    "odd":"301.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"9.50"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.50"
                  },
                  {
                    "value":"2:2",
                    "odd":"15.00"
                  },
                  {
                    "value":"3:3",
                    "odd":"67.00"
                  },
                  {
                    "value":"4:4",
                    "odd":"201.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"11.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"21.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"51.00"
                  },
                  {
                    "value":"0:4",
                    "odd":"126.00"
                  },
                  {
                    "value":"0:5",
                    "odd":"301.00"
                  },
                  {
                    "value":"0:6",
                    "odd":"351.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"13.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"34.00"
                  },
                  {
                    "value":"1:4",
                    "odd":"101.00"
                  },
                  {
                    "value":"1:5",
                    "odd":"251.00"
                  },
                  {
                    "value":"1:6",
                    "odd":"351.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"41.00"
                  },
                  {
                    "value":"2:4",
                    "odd":"101.00"
                  },
                  {
                    "value":"2:5",
                    "odd":"251.00"
                  },
                  {
                    "value":"2:6",
                    "odd":"351.00"
                  },
                  {
                    "value":"3:4",
                    "odd":"176.00"
                  },
                  {
                    "value":"3:5",
                    "odd":"301.00"
                  },
                  {
                    "value":"4:5",
                    "odd":"351.00"
                  },
                  {
                    "value":"0:7",
                    "odd":"401.00"
                  },
                  {
                    "value":"1:7",
                    "odd":"401.00"
                  },
                  {
                    "value":"7:0",
                    "odd":"351.00"
                  },
                  {
                    "value":"7:1",
                    "odd":"351.00"
                  },
                  {
                    "value":"8:0",
                    "odd":"401.00"
                  }
                ]
              },
              {
                "label_id":11,
                "label_name":"Highest Scoring Half",
                "values":[
                  {
                    "value":"Draw",
                    "odd":"3.20"
                  },
                  {
                    "value":"1st Half",
                    "odd":"3.10"
                  },
                  {
                    "value":"2nd Half",
                    "odd":"2.10"
                  }
                ]
              },
              {
                "label_id":31,
                "label_name":"Correct Score - First Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"3.75"
                  },
                  {
                    "value":"2:0",
                    "odd":"10.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"19.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"34.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"51.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"101.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"101.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"2.60"
                  },
                  {
                    "value":"1:1",
                    "odd":"7.50"
                  },
                  {
                    "value":"2:2",
                    "odd":"51.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"5.50"
                  },
                  {
                    "value":"0:2",
                    "odd":"19.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"67.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"26.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"81.00"
                  }
                ]
              },
              {
                "label_id":62,
                "label_name":"Correct Score - Second Half",
                "values":[
                  {
                    "value":"1:0",
                    "odd":"4.33"
                  },
                  {
                    "value":"2:0",
                    "odd":"9.00"
                  },
                  {
                    "value":"2:1",
                    "odd":"13.00"
                  },
                  {
                    "value":"3:0",
                    "odd":"23.00"
                  },
                  {
                    "value":"3:1",
                    "odd":"41.00"
                  },
                  {
                    "value":"3:2",
                    "odd":"81.00"
                  },
                  {
                    "value":"4:0",
                    "odd":"81.00"
                  },
                  {
                    "value":"4:1",
                    "odd":"126.00"
                  },
                  {
                    "value":"5:0",
                    "odd":"251.00"
                  },
                  {
                    "value":"0:0",
                    "odd":"3.60"
                  },
                  {
                    "value":"1:1",
                    "odd":"6.50"
                  },
                  {
                    "value":"2:2",
                    "odd":"34.00"
                  },
                  {
                    "value":"0:1",
                    "odd":"6.00"
                  },
                  {
                    "value":"0:2",
                    "odd":"17.00"
                  },
                  {
                    "value":"0:3",
                    "odd":"51.00"
                  },
                  {
                    "value":"1:2",
                    "odd":"17.00"
                  },
                  {
                    "value":"1:3",
                    "odd":"67.00"
                  },
                  {
                    "value":"2:3",
                    "odd":"101.00"
                  }
                ]
              },
              {
                "label_id":12,
                "label_name":"Double Chance",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.25"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.30"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.80"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.70"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.10"
                  },
                  {
                    "value":"Away",
                    "odd":"4.33"
                  }
                ]
              },
              {
                "label_id":14,
                "label_name":"Team To Score First",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.67"
                  },
                  {
                    "value":"Draw",
                    "odd":"9.00"
                  },
                  {
                    "value":"Away",
                    "odd":"2.50"
                  }
                ]
              },
              {
                "label_id":15,
                "label_name":"Team To Score Last",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.83"
                  },
                  {
                    "value":"Away",
                    "odd":"2.63"
                  },
                  {
                    "value":"No goal",
                    "odd":"11.00"
                  }
                ]
              },
              {
                "label_id":32,
                "label_name":"Win Both Halves",
                "values":[
                  {
                    "value":"Home",
                    "odd":"6.00"
                  },
                  {
                    "value":"Away",
                    "odd":"17.00"
                  }
                ]
              },
              {
                "label_id":16,
                "label_name":"Total - Home",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"11.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.03"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"2.10"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.67"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"34.00"
                  },
                  {
                    "value":"Under 4.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"4.50"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.17"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.22"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"4.00"
                  }
                ]
              },
              {
                "label_id":17,
                "label_name":"Total - Away",
                "values":[
                  {
                    "value":"Over 3.5",
                    "odd":"26.00"
                  },
                  {
                    "value":"Under 3.5",
                    "odd":"1.01"
                  },
                  {
                    "value":"Over 1.5",
                    "odd":"3.25"
                  },
                  {
                    "value":"Under 1.5",
                    "odd":"1.33"
                  },
                  {
                    "value":"Over 4.5",
                    "odd":"67.00"
                  },
                  {
                    "value":"Over 2.5",
                    "odd":"8.00"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.06"
                  },
                  {
                    "value":"Over 0.5",
                    "odd":"1.50"
                  },
                  {
                    "value":"Under 0.5",
                    "odd":"2.50"
                  }
                ]
              },
              {
                "label_id":18,
                "label_name":"Handicap Result - First Half",
                "values":[
                  {
                    "value":"Home -1",
                    "odd":"8.50"
                  },
                  {
                    "value":"Away -1",
                    "odd":"1.44"
                  },
                  {
                    "value":"Draw -1",
                    "odd":"3.40"
                  },
                  {
                    "value":"Home +1",
                    "odd":"1.20"
                  },
                  {
                    "value":"Away +1",
                    "odd":"15.00"
                  },
                  {
                    "value":"Draw +1",
                    "odd":"4.80"
                  },
                  {
                    "value":"Home -2",
                    "odd":"29.00"
                  },
                  {
                    "value":"Draw -2",
                    "odd":"9.50"
                  },
                  {
                    "value":"Away -2",
                    "odd":"1.07"
                  }
                ]
              },
              {
                "label_id":20,
                "label_name":"Double Chance - First Half",
                "values":[
                  {
                    "value":"Home\/Draw",
                    "odd":"1.20"
                  },
                  {
                    "value":"Home\/Away",
                    "odd":"1.67"
                  },
                  {
                    "value":"Draw\/Away",
                    "odd":"1.44"
                  }
                ]
              },
              {
                "label_id":34,
                "label_name":"Both Teams Score - First Half",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"5.00"
                  },
                  {
                    "value":"No",
                    "odd":"1.17"
                  }
                ]
              },
              {
                "label_id":36,
                "label_name":"Win To Nil",
                "values":[
                  {
                    "value":"Home",
                    "odd":"3.25"
                  },
                  {
                    "value":"Away",
                    "odd":"6.50"
                  }
                ]
              },
              {
                "label_id":21,
                "label_name":"Odd\/Even",
                "values":[
                  {
                    "value":"Odd",
                    "odd":"1.91"
                  },
                  {
                    "value":"Even",
                    "odd":"1.91"
                  }
                ]
              },
              {
                "label_id":39,
                "label_name":"To Win Either Half",
                "values":[
                  {
                    "value":"Home",
                    "odd":"1.53"
                  },
                  {
                    "value":"Away",
                    "odd":"2.38"
                  }
                ]
              },
              {
                "label_id":24,
                "label_name":"Results\/Both Teams Score",
                "values":[
                  {
                    "value":"Home\/Yes",
                    "odd":"4.33"
                  },
                  {
                    "value":"Draw\/Yes",
                    "odd":"4.50"
                  },
                  {
                    "value":"Away\/Yes",
                    "odd":"7.50"
                  },
                  {
                    "value":"Home\/No",
                    "odd":"3.25"
                  },
                  {
                    "value":"Draw\/No",
                    "odd":"9.50"
                  },
                  {
                    "value":"Away\/No",
                    "odd":"7.00"
                  }
                ]
              },
              {
                "label_id":25,
                "label_name":"Result\/Total Goals",
                "values":[
                  {
                    "value":"Home\/Over 3.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Draw\/Over 3.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away\/Over 3.5",
                    "odd":"13.00"
                  },
                  {
                    "value":"Home\/Under 3.5",
                    "odd":"2.60"
                  },
                  {
                    "value":"Draw\/Under 3.5",
                    "odd":"4.00"
                  },
                  {
                    "value":"Away\/Under 3.5",
                    "odd":"4.80"
                  },
                  {
                    "value":"Home\/Over 1.5",
                    "odd":"2.50"
                  },
                  {
                    "value":"Away\/Over 1.5",
                    "odd":"5.00"
                  },
                  {
                    "value":"Draw\/Over 1.5",
                    "odd":"4.50"
                  },
                  {
                    "value":"Draw\/Under 1.5",
                    "odd":"9.00"
                  },
                  {
                    "value":"Home\/Under 1.5",
                    "odd":"7.00"
                  },
                  {
                    "value":"Away\/Under 1.5",
                    "odd":"11.00"
                  },
                  {
                    "value":"Home\/Over 2.5",
                    "odd":"3.25"
                  },
                  {
                    "value":"Draw\/Over 2.5",
                    "odd":"12.00"
                  },
                  {
                    "value":"Away\/Over 2.5",
                    "odd":"6.50"
                  },
                  {
                    "value":"Home\/Under 2.5",
                    "odd":"4.20"
                  },
                  {
                    "value":"Draw\/Under 2.5",
                    "odd":"4.00"
                  },
                  {
                    "value":"Away\/Under 2.5",
                    "odd":"8.00"
                  }
                ]
              },
              {
                "label_id":43,
                "label_name":"Home Team Score a Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.22"
                  },
                  {
                    "value":"No",
                    "odd":"4.33"
                  }
                ]
              },
              {
                "label_id":44,
                "label_name":"Away Team Score a Goal",
                "values":[
                  {
                    "value":"Yes",
                    "odd":"1.50"
                  },
                  {
                    "value":"No",
                    "odd":"2.50"
                  }
                ]
              },
              {
                "label_id":46,
                "label_name":"Exact Goals Number - First Half",
                "values":[
                  {
                    "value":1,
                    "odd":"2.50"
                  },
                  {
                    "value":0,
                    "odd":"2.70"
                  },
                  {
                    "value":"more 2",
                    "odd":"2.90"
                  }
                ]
              }
            ]
          },
          {
            "bookmaker_id":9,
            "bookmaker_name":"Dafabet",
            "bets":[
              {
                "label_id":1,
                "label_name":"Match Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.08"
                  },
                  {
                    "value":"Draw",
                    "odd":"3.30"
                  },
                  {
                    "value":"Away",
                    "odd":"3.95"
                  }
                ]
              },
              {
                "label_id":5,
                "label_name":"Goals Over\/Under",
                "values":[
                  {
                    "value":"Over 2.5",
                    "odd":"2.11"
                  },
                  {
                    "value":"Under 2.5",
                    "odd":"1.82"
                  },
                  {
                    "value":"Over 2.25",
                    "odd":"1.86"
                  },
                  {
                    "value":"Under 2.25",
                    "odd":"2.06"
                  },
                  {
                    "value":"Over 2.75",
                    "odd":"2.44"
                  },
                  {
                    "value":"Under 2.75",
                    "odd":"1.61"
                  },
                  {
                    "value":"Over 2.0",
                    "odd":"1.59"
                  },
                  {
                    "value":"Under 2.0",
                    "odd":"2.49"
                  }
                ]
              },
              {
                "label_id":6,
                "label_name":"Goals Over\/Under First Half",
                "values":[
                  {
                    "value":"Over 0.75",
                    "odd":"1.62"
                  },
                  {
                    "value":"Under 0.75",
                    "odd":"2.38"
                  },
                  {
                    "value":"Over 1.0",
                    "odd":"2.02"
                  },
                  {
                    "value":"Under 1.0",
                    "odd":"1.88"
                  }
                ]
              },
              {
                "label_id":13,
                "label_name":"First Half Winner",
                "values":[
                  {
                    "value":"Home",
                    "odd":"2.75"
                  },
                  {
                    "value":"Draw",
                    "odd":"2.08"
                  },
                  {
                    "value":"Away",
                    "odd":"4.25"
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
};

exports.h2hByTeamIds = {
  "api": {
    "teams": [
      {
        "team_id": 62,
        "team_name": "Sheffield Utd",
        "team_logo": "https://media.api-football.com/teams/62.png",
        "statistics": {
          "played": {
            "home": 1,
            "away": 1,
            "total": 2
          },
          "wins": {
            "home": 1,
            "away": 0,
            "total": 1
          },
          "draws": {
            "home": 0,
            "away": 1,
            "total": 1
          },
          "loses": {
            "home": 0,
            "away": 0,
            "total": 0
          }
        }
      },
      {
        "team_id": 48,
        "team_name": "West Ham",
        "team_logo": "https://media.api-football.com/teams/48.png",
        "statistics": {
          "played": {
            "home": 1,
            "away": 1,
            "total": 2
          },
          "wins": {
            "home": 0,
            "away": 0,
            "total": 0
          },
          "draws": {
            "home": 1,
            "away": 0,
            "total": 1
          },
          "loses": {
            "home": 0,
            "away": 1,
            "total": 1
          }
        }
      }
    ],
    "results": 2,
    "fixtures": [
      {
        "fixture_id": 157114,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-10-26T14:00:00+00:00",
        "event_timestamp": 1572098400,
        "firstHalfStart": 1572098400,
        "secondHalfStart": 1572102000,
        "round": "Regular Season - 10",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "1-0",
          "fulltime": "1-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 1571142,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-11T14:00:00+00:00",
        "event_timestamp": 1572098400,
        "firstHalfStart": 1572098400,
        "secondHalfStart": 1572102000,
        "round": "Regular Season - 10",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "1-0",
          "fulltime": "2-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 1571152,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-15T14:00:00+00:00",
        "event_timestamp": 1572098400,
        "firstHalfStart": 1572098400,
        "secondHalfStart": 1572102000,
        "round": "Regular Season - 10",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 3,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "1-0",
          "fulltime": "3-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157232,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-10T20:00:00+00:00",
        "event_timestamp": 1578686400,
        "firstHalfStart": 1578686400,
        "secondHalfStart": 1578690000,
        "round": "Regular Season - 22",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane, Sheffield",
        "referee": "Michael Oliver, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-0",
          "extratime": null,
          "penalty": null
        }
      }
    ]
  }
};

exports.homeTeamLeagueGames = {
  "api": {
    "results": 38,
    "fixtures": [
      {
        "fixture_id": 157017,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-08-10T14:00:00+00:00",
        "event_timestamp": 1565445600,
        "firstHalfStart": 1565445600,
        "secondHalfStart": 1565449200,
        "round": "Regular Season - 1",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Vitality Stadium (Bournemouth, Dorset)",
        "referee": "Kevin Friend, England",
        "homeTeam": {
          "team_id": 35,
          "team_name": "Bournemouth",
          "logo": "https://media.api-football.com/teams/35.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157032,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-08-18T13:00:00+00:00",
        "event_timestamp": 1566133200,
        "firstHalfStart": 1566133200,
        "secondHalfStart": 1566136800,
        "round": "Regular Season - 2",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 52,
          "team_name": "Crystal Palace",
          "logo": "https://media.api-football.com/teams/52.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157041,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-08-24T14:00:00+00:00",
        "event_timestamp": 1566655200,
        "firstHalfStart": 1566655200,
        "secondHalfStart": 1566658800,
        "round": "Regular Season - 3",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "Andy Madley, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 46,
          "team_name": "Leicester",
          "logo": "https://media.api-football.com/teams/46.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 2,
        "score": {
          "halftime": "0-1",
          "fulltime": "1-2",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157047,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-08-31T14:00:00+00:00",
        "event_timestamp": 1567260000,
        "firstHalfStart": 1567260000,
        "secondHalfStart": 1567263600,
        "round": "Regular Season - 4",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Stamford Bridge (London)",
        "referee": "Stuart Attwell, England",
        "homeTeam": {
          "team_id": 49,
          "team_name": "Chelsea",
          "logo": "https://media.api-football.com/teams/49.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 2,
        "score": {
          "halftime": "2-0",
          "fulltime": "2-2",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157061,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-09-14T14:00:00+00:00",
        "event_timestamp": 1568469600,
        "firstHalfStart": 1568469600,
        "secondHalfStart": 1568473200,
        "round": "Regular Season - 5",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "Lee Mason, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 41,
          "team_name": "Southampton",
          "logo": "https://media.api-football.com/teams/41.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-0",
          "fulltime": "0-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157069,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-09-21T14:00:00+00:00",
        "event_timestamp": 1569074400,
        "firstHalfStart": 1569074400,
        "secondHalfStart": 1569078000,
        "round": "Regular Season - 6",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Goodison Park (Liverpool)",
        "referee": "Simon Hooper, England",
        "homeTeam": {
          "team_id": 45,
          "team_name": "Everton",
          "logo": "https://media.api-football.com/teams/45.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 2,
        "score": {
          "halftime": "0-1",
          "fulltime": "0-2",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157082,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-09-28T11:30:00+00:00",
        "event_timestamp": 1569670200,
        "firstHalfStart": 1569670200,
        "secondHalfStart": 1569673800,
        "round": "Regular Season - 7",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "Anthony Taylor, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 40,
          "team_name": "Liverpool",
          "logo": "https://media.api-football.com/teams/40.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-0",
          "fulltime": "0-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157093,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-10-05T14:00:00+00:00",
        "event_timestamp": 1570284000,
        "firstHalfStart": 1570284000,
        "secondHalfStart": 1570287600,
        "round": "Regular Season - 8",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Vicarage Road (Watford)",
        "referee": "Andre Marriner, England",
        "homeTeam": {
          "team_id": 38,
          "team_name": "Watford",
          "logo": "https://media.api-football.com/teams/38.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "0-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157102,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-10-21T19:00:00+00:00",
        "event_timestamp": 1571684400,
        "firstHalfStart": 1571684400,
        "secondHalfStart": 1571688000,
        "round": "Regular Season - 9",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "Mike Dean, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 42,
          "team_name": "Arsenal",
          "logo": "https://media.api-football.com/teams/42.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "1-0",
          "fulltime": "1-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157114,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-10-26T14:00:00+00:00",
        "event_timestamp": 1572098400,
        "firstHalfStart": 1572098400,
        "secondHalfStart": 1572102000,
        "round": "Regular Season - 10",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "1-0",
          "fulltime": "1-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157122,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-02T15:00:00+00:00",
        "event_timestamp": 1572706800,
        "firstHalfStart": 1572706800,
        "secondHalfStart": 1572710400,
        "round": "Regular Season - 11",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "Simon Hooper, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 44,
          "team_name": "Burnley",
          "logo": "https://media.api-football.com/teams/44.png"
        },
        "goalsHomeTeam": 3,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "3-0",
          "fulltime": "3-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157133,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-09T15:00:00+00:00",
        "event_timestamp": 1573311600,
        "firstHalfStart": 1573311600,
        "secondHalfStart": 1573315200,
        "round": "Regular Season - 12",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Tottenham Hotspur Stadium (London)",
        "referee": "Graham Scott, England",
        "homeTeam": {
          "team_id": 47,
          "team_name": "Tottenham",
          "logo": "https://media.api-football.com/teams/47.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157142,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-24T16:30:00+00:00",
        "event_timestamp": 1574613000,
        "firstHalfStart": 1574613000,
        "secondHalfStart": 1574616600,
        "round": "Regular Season - 13",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "Andre Marriner, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 33,
          "team_name": "Manchester United",
          "logo": "https://media.api-football.com/teams/33.png"
        },
        "goalsHomeTeam": 3,
        "goalsAwayTeam": 3,
        "score": {
          "halftime": "1-0",
          "fulltime": "3-3",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157154,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-01T14:00:00+00:00",
        "event_timestamp": 1575208800,
        "firstHalfStart": 1575208800,
        "secondHalfStart": 1575212400,
        "round": "Regular Season - 14",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Molineux Stadium (Wolverhampton, West Midlands)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 39,
          "team_name": "Wolves",
          "logo": "https://media.api-football.com/teams/39.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-1",
          "fulltime": "1-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157158,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-05T19:30:00+00:00",
        "event_timestamp": 1575574200,
        "firstHalfStart": 1575574200,
        "secondHalfStart": 1575577800,
        "round": "Regular Season - 15",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "Stuart Attwell, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 34,
          "team_name": "Newcastle",
          "logo": "https://media.api-football.com/teams/34.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 2,
        "score": {
          "halftime": "0-1",
          "fulltime": "0-2",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157171,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-08T14:00:00+00:00",
        "event_timestamp": 1575813600,
        "firstHalfStart": 1575813600,
        "secondHalfStart": 1575817200,
        "round": "Regular Season - 16",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Carrow Road (Norwich, Norfolk)",
        "referee": "Simon Hooper, England",
        "homeTeam": {
          "team_id": 71,
          "team_name": "Norwich",
          "logo": "https://media.api-football.com/teams/71.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 2,
        "score": {
          "halftime": "1-0",
          "fulltime": "1-2",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157182,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-14T15:00:00+00:00",
        "event_timestamp": 1576335600,
        "firstHalfStart": 1576335600,
        "secondHalfStart": 1576339200,
        "round": "Regular Season - 17",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "Peter Bankes, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 66,
          "team_name": "Aston Villa",
          "logo": "https://media.api-football.com/teams/66.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "2-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157187,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-21T15:00:00+00:00",
        "event_timestamp": 1576940400,
        "firstHalfStart": 1576940400,
        "secondHalfStart": 1576944000,
        "round": "Regular Season - 18",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "The American Express Community Stadium (Falmer, East Sussex)",
        "referee": "Robert Jones, England",
        "homeTeam": {
          "team_id": 51,
          "team_name": "Brighton",
          "logo": "https://media.api-football.com/teams/51.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-1",
          "fulltime": "0-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157202,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-26T15:00:00+00:00",
        "event_timestamp": 1577372400,
        "firstHalfStart": 1577372400,
        "secondHalfStart": 1577376000,
        "round": "Regular Season - 19",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane (Sheffield)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 38,
          "team_name": "Watford",
          "logo": "https://media.api-football.com/teams/38.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "1-1",
          "fulltime": "1-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157209,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-29T18:00:00+00:00",
        "event_timestamp": 1577642400,
        "firstHalfStart": 1577642400,
        "secondHalfStart": 1577646000,
        "round": "Regular Season - 20",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Etihad Stadium (Manchester)",
        "referee": "Chris Kavanagh, England",
        "homeTeam": {
          "team_id": 50,
          "team_name": "Manchester City",
          "logo": "https://media.api-football.com/teams/50.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "2-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157218,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-02T20:00:00+00:00",
        "event_timestamp": 1577995200,
        "firstHalfStart": 1577995200,
        "secondHalfStart": 1577998800,
        "round": "Regular Season - 21",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Anfield, Liverpool",
        "referee": "Paul Tierney, England",
        "homeTeam": {
          "team_id": 40,
          "team_name": "Liverpool",
          "logo": "https://media.api-football.com/teams/40.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "1-0",
          "fulltime": "2-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157232,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-10T20:00:00+00:00",
        "event_timestamp": 1578686400,
        "firstHalfStart": 1578686400,
        "secondHalfStart": 1578690000,
        "round": "Regular Season - 22",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane, Sheffield",
        "referee": "Michael Oliver, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157235,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-18T15:00:00+00:00",
        "event_timestamp": 1579359600,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 23",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Emirates Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 42,
          "team_name": "Arsenal",
          "logo": "https://media.api-football.com/teams/42.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157249,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-21T19:30:00+00:00",
        "event_timestamp": 1579635000,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 24",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Bramall Lane (Sheffield)",
        "referee": null,
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 50,
          "team_name": "Manchester City",
          "logo": "https://media.api-football.com/teams/50.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157257,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-02-01T15:00:00+00:00",
        "event_timestamp": 1580569200,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 25",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Selhurst Park (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 52,
          "team_name": "Crystal Palace",
          "logo": "https://media.api-football.com/teams/52.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157272,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-02-09T14:00:00+00:00",
        "event_timestamp": 1581256800,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 26",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Bramall Lane (Sheffield)",
        "referee": null,
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 35,
          "team_name": "Bournemouth",
          "logo": "https://media.api-football.com/teams/35.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157282,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-02-22T15:00:00+00:00",
        "event_timestamp": 1582383600,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 27",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Bramall Lane (Sheffield)",
        "referee": null,
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 51,
          "team_name": "Brighton",
          "logo": "https://media.api-football.com/teams/51.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157286,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-02-29T15:00:00+00:00",
        "event_timestamp": 1582988400,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 28",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Villa Park (Birmingham)",
        "referee": null,
        "homeTeam": {
          "team_id": 66,
          "team_name": "Aston Villa",
          "logo": "https://media.api-football.com/teams/66.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157302,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-03-07T15:00:00+00:00",
        "event_timestamp": 1583593200,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 29",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Bramall Lane (Sheffield)",
        "referee": null,
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 71,
          "team_name": "Norwich",
          "logo": "https://media.api-football.com/teams/71.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157310,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-03-14T15:00:00+00:00",
        "event_timestamp": 1584198000,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 30",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "St. James' Park (Newcastle upon Tyne)",
        "referee": null,
        "homeTeam": {
          "team_id": 34,
          "team_name": "Newcastle",
          "logo": "https://media.api-football.com/teams/34.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157319,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-03-21T15:00:00+00:00",
        "event_timestamp": 1584802800,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 31",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Old Trafford (Manchester)",
        "referee": null,
        "homeTeam": {
          "team_id": 33,
          "team_name": "Manchester United",
          "logo": "https://media.api-football.com/teams/33.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157332,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-04-04T14:00:00+00:00",
        "event_timestamp": 1586008800,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 32",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Bramall Lane (Sheffield)",
        "referee": null,
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 47,
          "team_name": "Tottenham",
          "logo": "https://media.api-football.com/teams/47.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157335,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-04-11T14:00:00+00:00",
        "event_timestamp": 1586613600,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 33",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Turf Moor (Burnley)",
        "referee": null,
        "homeTeam": {
          "team_id": 44,
          "team_name": "Burnley",
          "logo": "https://media.api-football.com/teams/44.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157352,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-04-18T14:00:00+00:00",
        "event_timestamp": 1587218400,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 34",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Bramall Lane (Sheffield)",
        "referee": null,
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 39,
          "team_name": "Wolves",
          "logo": "https://media.api-football.com/teams/39.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157361,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-04-25T14:00:00+00:00",
        "event_timestamp": 1587823200,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 35",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Bramall Lane (Sheffield)",
        "referee": null,
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 49,
          "team_name": "Chelsea",
          "logo": "https://media.api-football.com/teams/49.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157370,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-05-02T14:00:00+00:00",
        "event_timestamp": 1588428000,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 36",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "King Power Stadium (Leicester, Leicestershire)",
        "referee": null,
        "homeTeam": {
          "team_id": 46,
          "team_name": "Leicester",
          "logo": "https://media.api-football.com/teams/46.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157381,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-05-09T14:00:00+00:00",
        "event_timestamp": 1589032800,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 37",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Bramall Lane (Sheffield)",
        "referee": null,
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 45,
          "team_name": "Everton",
          "logo": "https://media.api-football.com/teams/45.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157393,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-05-17T14:00:00+00:00",
        "event_timestamp": 1589724000,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 38",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "St. Mary's Stadium (Southampton, Hampshire)",
        "referee": null,
        "homeTeam": {
          "team_id": 41,
          "team_name": "Southampton",
          "logo": "https://media.api-football.com/teams/41.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      }
    ]
  }
};

exports.awayTeamLeagueGames = {
  "api": {
    "results": 38,
    "fixtures": [
      {
        "fixture_id": 157016,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-08-10T11:30:00+00:00",
        "event_timestamp": 1565436600,
        "firstHalfStart": 1565436600,
        "secondHalfStart": 1565440200,
        "round": "Regular Season - 1",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "Mike Dean, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 50,
          "team_name": "Manchester City",
          "logo": "https://media.api-football.com/teams/50.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 5,
        "score": {
          "halftime": "0-1",
          "fulltime": "0-5",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157027,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-08-17T14:00:00+00:00",
        "event_timestamp": 1566050400,
        "firstHalfStart": 1566050400,
        "secondHalfStart": 1566054000,
        "round": "Regular Season - 2",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "The American Express Community Stadium (Falmer, East Sussex)",
        "referee": "Anthony Taylor, England",
        "homeTeam": {
          "team_id": 51,
          "team_name": "Brighton",
          "logo": "https://media.api-football.com/teams/51.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157043,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-08-24T14:00:00+00:00",
        "event_timestamp": 1566655200,
        "firstHalfStart": 1566655200,
        "secondHalfStart": 1566658800,
        "round": "Regular Season - 3",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Vicarage Road (Watford)",
        "referee": "Chris Kavanagh, England",
        "homeTeam": {
          "team_id": 38,
          "team_name": "Watford",
          "logo": "https://media.api-football.com/teams/38.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 3,
        "score": {
          "halftime": "1-1",
          "fulltime": "1-3",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157054,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-08-31T14:00:00+00:00",
        "event_timestamp": 1567260000,
        "firstHalfStart": 1567260000,
        "secondHalfStart": 1567263600,
        "round": "Regular Season - 4",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "Paul Tierney, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 71,
          "team_name": "Norwich",
          "logo": "https://media.api-football.com/teams/71.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "1-0",
          "fulltime": "2-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157056,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-09-16T19:00:00+00:00",
        "event_timestamp": 1568660400,
        "firstHalfStart": 1568660400,
        "secondHalfStart": 1568664000,
        "round": "Regular Season - 5",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Villa Park (Birmingham)",
        "referee": "Mike Dean, England",
        "homeTeam": {
          "team_id": 66,
          "team_name": "Aston Villa",
          "logo": "https://media.api-football.com/teams/66.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "0-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157074,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-09-22T13:00:00+00:00",
        "event_timestamp": 1569157200,
        "firstHalfStart": 1569157200,
        "secondHalfStart": 1569160800,
        "round": "Regular Season - 6",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "Anthony Taylor, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 33,
          "team_name": "Manchester United",
          "logo": "https://media.api-football.com/teams/33.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "1-0",
          "fulltime": "2-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157075,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-09-28T14:00:00+00:00",
        "event_timestamp": 1569679200,
        "firstHalfStart": 1569679200,
        "secondHalfStart": 1569682800,
        "round": "Regular Season - 7",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Vitality Stadium (Bournemouth, Dorset)",
        "referee": "Stuart Attwell, England",
        "homeTeam": {
          "team_id": 35,
          "team_name": "Bournemouth",
          "logo": "https://media.api-football.com/teams/35.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 2,
        "score": {
          "halftime": "1-1",
          "fulltime": "2-2",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157094,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-10-05T16:30:00+00:00",
        "event_timestamp": 1570293000,
        "firstHalfStart": 1570293000,
        "secondHalfStart": 1570296600,
        "round": "Regular Season - 8",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "Michael Oliver, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 52,
          "team_name": "Crystal Palace",
          "logo": "https://media.api-football.com/teams/52.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 2,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-2",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157099,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-10-19T11:30:00+00:00",
        "event_timestamp": 1571484600,
        "firstHalfStart": 1571484600,
        "secondHalfStart": 1571488200,
        "round": "Regular Season - 9",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Goodison Park (Liverpool)",
        "referee": "Paul Tierney, England",
        "homeTeam": {
          "team_id": 45,
          "team_name": "Everton",
          "logo": "https://media.api-football.com/teams/45.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "1-0",
          "fulltime": "2-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157114,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-10-26T14:00:00+00:00",
        "event_timestamp": 1572098400,
        "firstHalfStart": 1572098400,
        "secondHalfStart": 1572102000,
        "round": "Regular Season - 10",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "1-0",
          "fulltime": "1-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157124,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-02T15:00:00+00:00",
        "event_timestamp": 1572706800,
        "firstHalfStart": 1572706800,
        "secondHalfStart": 1572710400,
        "round": "Regular Season - 11",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "Stuart Attwell, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 34,
          "team_name": "Newcastle",
          "logo": "https://media.api-football.com/teams/34.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 3,
        "score": {
          "halftime": "0-2",
          "fulltime": "2-3",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157125,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-09T15:00:00+00:00",
        "event_timestamp": 1573311600,
        "firstHalfStart": 1573311600,
        "secondHalfStart": 1573315200,
        "round": "Regular Season - 12",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Turf Moor (Burnley)",
        "referee": "Kevin Friend, England",
        "homeTeam": {
          "team_id": 44,
          "team_name": "Burnley",
          "logo": "https://media.api-football.com/teams/44.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 3,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "2-0",
          "fulltime": "3-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157144,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-23T12:30:00+00:00",
        "event_timestamp": 1574512200,
        "firstHalfStart": 1574512200,
        "secondHalfStart": 1574515800,
        "round": "Regular Season - 13",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "Michael Oliver, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 47,
          "team_name": "Tottenham",
          "logo": "https://media.api-football.com/teams/47.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 3,
        "score": {
          "halftime": "0-2",
          "fulltime": "2-3",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157146,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-11-30T15:00:00+00:00",
        "event_timestamp": 1575126000,
        "firstHalfStart": 1575126000,
        "secondHalfStart": 1575129600,
        "round": "Regular Season - 14",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Stamford Bridge (London)",
        "referee": "Jonathan Moss, England",
        "homeTeam": {
          "team_id": 49,
          "team_name": "Chelsea",
          "logo": "https://media.api-football.com/teams/49.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-0",
          "fulltime": "0-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157159,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-04T19:30:00+00:00",
        "event_timestamp": 1575487800,
        "firstHalfStart": 1575487800,
        "secondHalfStart": 1575491400,
        "round": "Regular Season - 15",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Molineux Stadium (Wolverhampton, West Midlands)",
        "referee": "Andre Marriner, England",
        "homeTeam": {
          "team_id": 39,
          "team_name": "Wolves",
          "logo": "https://media.api-football.com/teams/39.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "1-0",
          "fulltime": "2-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157174,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-09T20:00:00+00:00",
        "event_timestamp": 1575921600,
        "firstHalfStart": 1575921600,
        "secondHalfStart": 1575925200,
        "round": "Regular Season - 16",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "Mike Dean, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 42,
          "team_name": "Arsenal",
          "logo": "https://media.api-football.com/teams/42.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 3,
        "score": {
          "halftime": "1-0",
          "fulltime": "1-3",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157183,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-14T17:30:00+00:00",
        "event_timestamp": 1576344600,
        "firstHalfStart": 1576344600,
        "secondHalfStart": 1576348200,
        "round": "Regular Season - 17",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "St. Mary's Stadium (Southampton, Hampshire)",
        "referee": "Martin Atkinson, England",
        "homeTeam": {
          "team_id": 41,
          "team_name": "Southampton",
          "logo": "https://media.api-football.com/teams/41.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 0,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-1",
          "fulltime": "0-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157198,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-26T15:00:00+00:00",
        "event_timestamp": 1577372400,
        "firstHalfStart": 1577372400,
        "secondHalfStart": 1577376000,
        "round": "Regular Season - 19",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Selhurst Park (London)",
        "referee": "Andre Marriner, England",
        "homeTeam": {
          "team_id": 52,
          "team_name": "Crystal Palace",
          "logo": "https://media.api-football.com/teams/52.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 2,
        "goalsAwayTeam": 1,
        "score": {
          "halftime": "0-0",
          "fulltime": "2-1",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157214,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2019-12-28T17:30:00+00:00",
        "event_timestamp": 1577554200,
        "firstHalfStart": 1577554200,
        "secondHalfStart": 1577557800,
        "round": "Regular Season - 20",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium (London)",
        "referee": "David Coote, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 46,
          "team_name": "Leicester",
          "logo": "https://media.api-football.com/teams/46.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 2,
        "score": {
          "halftime": "1-1",
          "fulltime": "1-2",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157224,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-01T17:30:00+00:00",
        "event_timestamp": 1577899800,
        "firstHalfStart": 1577899800,
        "secondHalfStart": 1577903400,
        "round": "Regular Season - 21",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "London Stadium, London",
        "referee": "Graham Scott, England",
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 35,
          "team_name": "Bournemouth",
          "logo": "https://media.api-football.com/teams/35.png"
        },
        "goalsHomeTeam": 4,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "3-0",
          "fulltime": "4-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157232,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-10T20:00:00+00:00",
        "event_timestamp": 1578686400,
        "firstHalfStart": 1578686400,
        "secondHalfStart": 1578690000,
        "round": "Regular Season - 22",
        "status": "Match Finished",
        "statusShort": "FT",
        "elapsed": 90,
        "venue": "Bramall Lane, Sheffield",
        "referee": "Michael Oliver, England",
        "homeTeam": {
          "team_id": 62,
          "team_name": "Sheffield Utd",
          "logo": "https://media.api-football.com/teams/62.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": 1,
        "goalsAwayTeam": 0,
        "score": {
          "halftime": "0-0",
          "fulltime": "1-0",
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157244,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-18T15:00:00+00:00",
        "event_timestamp": 1579359600,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 23",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 45,
          "team_name": "Everton",
          "logo": "https://media.api-football.com/teams/45.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157248,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-22T19:30:00+00:00",
        "event_timestamp": 1579721400,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 24",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "King Power Stadium (Leicester, Leicestershire)",
        "referee": null,
        "homeTeam": {
          "team_id": 46,
          "team_name": "Leicester",
          "logo": "https://media.api-football.com/teams/46.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157194,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-01-29T19:45:00+00:00",
        "event_timestamp": 1580327100,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 18",
        "status": "Match Postponed",
        "statusShort": "PST",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 40,
          "team_name": "Liverpool",
          "logo": "https://media.api-football.com/teams/40.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157264,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-02-01T15:00:00+00:00",
        "event_timestamp": 1580569200,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 25",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 51,
          "team_name": "Brighton",
          "logo": "https://media.api-football.com/teams/51.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157270,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-02-09T16:30:00+00:00",
        "event_timestamp": 1581265800,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 26",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Etihad Stadium (Manchester)",
        "referee": null,
        "homeTeam": {
          "team_id": 50,
          "team_name": "Manchester City",
          "logo": "https://media.api-football.com/teams/50.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157280,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-02-24T20:00:00+00:00",
        "event_timestamp": 1582574400,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 27",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Anfield (Liverpool)",
        "referee": null,
        "homeTeam": {
          "team_id": 40,
          "team_name": "Liverpool",
          "logo": "https://media.api-football.com/teams/40.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157294,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-02-29T15:00:00+00:00",
        "event_timestamp": 1582988400,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 28",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 41,
          "team_name": "Southampton",
          "logo": "https://media.api-football.com/teams/41.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157295,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-03-07T15:00:00+00:00",
        "event_timestamp": 1583593200,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 29",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Emirates Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 42,
          "team_name": "Arsenal",
          "logo": "https://media.api-football.com/teams/42.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157314,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-03-14T15:00:00+00:00",
        "event_timestamp": 1584198000,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 30",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 39,
          "team_name": "Wolves",
          "logo": "https://media.api-football.com/teams/39.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157323,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-03-21T15:00:00+00:00",
        "event_timestamp": 1584802800,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 31",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Tottenham Hotspur Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 47,
          "team_name": "Tottenham",
          "logo": "https://media.api-football.com/teams/47.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157334,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-04-04T14:00:00+00:00",
        "event_timestamp": 1586008800,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 32",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 49,
          "team_name": "Chelsea",
          "logo": "https://media.api-football.com/teams/49.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157340,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-04-11T14:00:00+00:00",
        "event_timestamp": 1586613600,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 33",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "St. James' Park (Newcastle upon Tyne)",
        "referee": null,
        "homeTeam": {
          "team_id": 34,
          "team_name": "Newcastle",
          "logo": "https://media.api-football.com/teams/34.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157354,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-04-18T14:00:00+00:00",
        "event_timestamp": 1587218400,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 34",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 44,
          "team_name": "Burnley",
          "logo": "https://media.api-football.com/teams/44.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157360,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-04-25T14:00:00+00:00",
        "event_timestamp": 1587823200,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 35",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Carrow Road (Norwich, Norfolk)",
        "referee": null,
        "homeTeam": {
          "team_id": 71,
          "team_name": "Norwich",
          "logo": "https://media.api-football.com/teams/71.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157374,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-05-02T14:00:00+00:00",
        "event_timestamp": 1588428000,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 36",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 38,
          "team_name": "Watford",
          "logo": "https://media.api-football.com/teams/38.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157379,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-05-09T14:00:00+00:00",
        "event_timestamp": 1589032800,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 37",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "Old Trafford (Manchester)",
        "referee": null,
        "homeTeam": {
          "team_id": 33,
          "team_name": "Manchester United",
          "logo": "https://media.api-football.com/teams/33.png"
        },
        "awayTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      },
      {
        "fixture_id": 157394,
        "league_id": 524,
        "league": {
          "name": "Premier League",
          "country": "England",
          "logo": "https://media.api-football.com/leagues/2.png",
          "flag": "https://media.api-football.com/flags/gb.svg"
        },
        "event_date": "2020-05-17T14:00:00+00:00",
        "event_timestamp": 1589724000,
        "firstHalfStart": null,
        "secondHalfStart": null,
        "round": "Regular Season - 38",
        "status": "Not Started",
        "statusShort": "NS",
        "elapsed": 0,
        "venue": "London Stadium (London)",
        "referee": null,
        "homeTeam": {
          "team_id": 48,
          "team_name": "West Ham",
          "logo": "https://media.api-football.com/teams/48.png"
        },
        "awayTeam": {
          "team_id": 66,
          "team_name": "Aston Villa",
          "logo": "https://media.api-football.com/teams/66.png"
        },
        "goalsHomeTeam": null,
        "goalsAwayTeam": null,
        "score": {
          "halftime": null,
          "fulltime": null,
          "extratime": null,
          "penalty": null
        }
      }
    ]
  }
};
