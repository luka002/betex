const puppeteer = require('puppeteer');
const leaguesPro = require('../constants/leagues-pro');

const ALL_PREDICTIONS_URL = 'https://www.forebet.com/en/football-predictions';

exports.fetchPredictionsForFixtures = async (fixtures) => {
  let browser;
  setTimeout(() => {
    if (browser) browser.close();
  }, 120000);

  try {
    browser = await puppeteer.launch({
      headless: false,
      args: ['--no-sandbox']
    });
    const page = await browser.newPage();
    await page.goto(ALL_PREDICTIONS_URL, {waitUntil: 'networkidle2'});
    await page.waitFor(2000);

    while (true) {
      try {
        await page.waitForSelector('tr#mrows > td > span', {visible: true, timeout: 5000});
        await page.waitFor(Math.floor(Math.random() * 1000) + 4000);
        await page.click('tr#mrows > td > span');
      } catch (e) {
        break;
      }
    }

    for (const game of fixtures) {
      if (game.forebetResult) continue;
      const forebetTeamHome = mapTeamFootballApiToForebet(game.homeTeamName, game.leagueCountry);
      const forebetAwayTeam = mapTeamFootballApiToForebet(game.awayTeamName, game.leagueCountry);
      if (!forebetTeamHome || !forebetAwayTeam) continue;
      const forebetGame = `${forebetTeamHome} vs ${forebetAwayTeam}`;

      try {
        game.forebetResult = await page.$eval(`tr > td > div > [content="${forebetGame}"]`, el => {
          const tr = el.closest('tr');
          const result = tr.querySelector('td:nth-child(6)');
          return result ? result.textContent : null;
        });
      } catch (ignorable) {}
    }

    await browser.close();
  } catch (e) {
    console.log(e);
    if (browser) {
      await browser.close();
    }
  }

  return fixtures;
};

function mapTeamFootballApiToForebet(footballApiTeam, teamCountry) {
  const team = leaguesPro
    .filter(league => league.country === teamCountry)
    .reduce((prevLeague, currLeague) => [...prevLeague, ...currLeague.teams], [])
    .find(team => team.footballApi === footballApiTeam);

  if (!team) {
    return null;
  }
  return team.forebet
}
