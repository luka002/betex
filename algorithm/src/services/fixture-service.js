const fs = require('fs');
const moment = require('moment');
const {chunkPromise, PromiseFlavor} = require('chunk-promise');
const footballApiService = require('./football-api-service');
const forebetScraperService = require('./forebet-scraper-service');
const fixtureDao = require('../DAO/fixture-dao');
const Fixture = require('../models/fixture');
const analysisService = require('./analysis-service');
const util = require('../util/util');

exports.findById = (fixtureId) => {
  return fixtureDao.findByFixtureId(fixtureId);
};

exports.findByDate = (dateStart, dateEnd) => {
  const dates = util.dateRangeToArray(dateStart, dateEnd);
  return fixtureDao.findByFilter({eventDate: {$in: dates}});
};

exports.findFinished = () => {
  return fixtureDao.findByFilter({status: 'FT'});
};

exports.findFinishedByDate = (dateStart, dateEnd) => {
  const dates = util.dateRangeToArray(dateStart, dateEnd);
  return fixtureDao.findByFilter({
    status: 'FT',
    eventDate: {
      $in: dates
    }
  });
};

exports.findNotFinished = () => {
  return fixtureDao.findByFilter({status: {$ne: 'FT'}})
};

exports.findNotFinishedByDate = (dateStart, dateEnd) => {
  const dates = util.dateRangeToArray(dateStart, dateEnd);
  return fixtureDao.findByFilter({
    status: {
      $ne: 'FT'
    },
    eventDate: {
      $in: dates
    }
  });
};

exports.analyzeFixtures = async (filter) => {
  const analyzedGames = await analysisService.analyzeFixtures(filter);
  fs.writeFileSync('result.json', JSON.stringify(analyzedGames, null, 2));
  return analyzedGames;
};

exports.predictFixtures = async (filter) => {
  const analyzedGames = await analysisService.predictFixtures(filter);
  fs.writeFileSync('result.json', JSON.stringify(analyzedGames, null, 2));
  return analyzedGames;
};

exports.updateOddsByDate = async (dateStart, dateEnd) => {
  const savedFixtures = await this.findByDate(dateStart, dateEnd);
  const betsToUpdate = savedFixtures.map(fixture => () => updateBetsByFixtureId(fixture.fixtureId));

  return chunkPromise(betsToUpdate, {
    concurrent: 20,
    promiseFlavor: PromiseFlavor.PromiseAllSettled,
    sleepMs: 100
  });
};

exports.updateForebetResultByDate = async (dateStart, dateEnd) => {
  const savedFixtures = await this.findByDate(dateStart, dateEnd);
  const fixturesDataForebet = await forebetScraperService.fetchPredictionsForFixtures(savedFixtures);
  const fixturesToSave = fixturesDataForebet.map(fixture => () => fixtureDao.updateForebet(fixture));
  return save(fixturesToSave);
};

exports.updateResultIfFinishedByDate = async (dateStart, dateEnd) => {
  const savedFixtures = await this.findNotFinishedByDate(dateStart, dateEnd);
  const fixtureIds = savedFixtures.map(fixture => fixture.fixtureId);
  const realTimeFixtures = await footballApiService.getFixturesByIds(fixtureIds);
  return updateFixturesResult(savedFixtures, realTimeFixtures.api.fixtures);
};

function updateFixturesResult(savedFixtures, realTimeFixtures) {
  const fixturesToUpdate = [];
  realTimeFixtures.forEach(fixture => {
    const currFixture = savedFixtures.find(f => f.fixtureId === fixture['fixture_id']);
    if (currFixture.status === fixture.status) {
      return;
    }
    currFixture.status = fixture.statusShort;
    if (fixture.statusShort === 'FT') {
      currFixture.homeTeamGoals = fixture.goalsHomeTeam;
      currFixture.awayTeamGoals = fixture.goalsAwayTeam;
      currFixture.goals = fixture.goalsHomeTeam + fixture.goalsAwayTeam;
      currFixture.isBts = fixture.goalsHomeTeam > 0 && fixture.goalsAwayTeam > 0;
    }
    fixturesToUpdate.push(() => fixtureDao.updateFixtureResult(currFixture));
  });

  return chunkPromise(fixturesToUpdate, {
    concurrent: 20,
    promiseFlavor: PromiseFlavor.PromiseAllSettled,
    sleepMs: 100
  });
}

exports.saveFixture = (fixture) => {
  return fixtureDao.saveFixture(fixture);
};

exports.saveFixturesByDate = async (date) => {
  const response = await footballApiService.getFixturesByDate(date);
  return saveFixtures(response.api.fixtures);
};

exports.saveFixturesBetweenDates = async (dateStart, dateEnd) => {
  const response = await footballApiService.getFixturesBetweenDates(dateStart, dateEnd);
  return saveFixtures(response.api.fixtures);
};

async function saveFixtures(fixtures) {
  const fixturesData = await getFixturesData({fixtures: fixtures});
  const fixturesDataForebet = await forebetScraperService.fetchPredictionsForFixtures(fixturesData);
  const fixturesToSave = fixturesDataForebet. map(fixture => () => fixtureDao.saveFixture(fixture));
  return save(fixturesToSave);
}

async function save(fixturesToSave) {
  const allChunks = await chunkPromise(fixturesToSave, {
    concurrent: 20,
    promiseFlavor: PromiseFlavor.PromiseAllSettled,
    sleepMs: 100
  });

  if (!allChunks) return;

  return allChunks.reduce((prevVal, currVal) => {
    const saved = currVal.status === 'fulfilled';
    return {
      saved: prevVal.saved + (saved ? 1 : 0),
      notSaved: prevVal.notSaved + (!saved ? 1 : 0)
    }
  }, {saved: 0, notSaved: 0});
}

async function getFixturesData ({
  fixtures,
  concurrentRequests = 20,
  sleepMs = 300,
  isProLeague = true,
  lastGamesNum = 13,
  logMe = true
}) {
  const fixturesToAnalyze = fixtures
    .map(fixture => new Fixture(fixture))
    .filter(fixture => isProLeague ? fixture.isProLeagueValid() : fixture.isLeagueValid())
    .map(fixture => () => getFixtureData(fixture, lastGamesNum));

  const totalIterations = fixturesToAnalyze.length / concurrentRequests;

  const allChunks = await chunkPromise(fixturesToAnalyze, {
    concurrent: concurrentRequests,
    promiseFlavor: PromiseFlavor.PromiseAllSettled,
    sleepMs: sleepMs,
    callback: async (chunkResults, index, allResults) => {
      if (logMe) {
        console.log(`${index}/${totalIterations}`);
      }
    },
  });

  if (!allChunks) return [];

  return allChunks
    .filter(analysis => analysis.status === 'fulfilled')
    .map(analysis => analysis.value);
}

async function getFixtureData(fixture, lastGamesNum) {
  const bets = await getBetsByFixtureId(fixture.fixtureId);
  if (!bets) {
    throw new Error('Bookies not available');
  }
  const h2hGames = await getH2HGames(fixture);
  if (!h2hGames) {
    throw new Error('H2H not available')
  }
  const homeTeamGames = await getLastLeagueGames(fixture, lastGamesNum, true);
  if (!homeTeamGames) {
    throw new Error('Home Team games not available')
  }
  const awayTeamGames = await getLastLeagueGames(fixture, lastGamesNum, false);
  if (!awayTeamGames) {
    throw new Error('Away team games not available')
  }

  return {
    ...fixture,
    bets: bets,
    h2hGames: h2hGames,
    homeTeamGames: homeTeamGames,
    awayTeamGames: awayTeamGames
  };
}

async function updateBetsByFixtureId(fixtureId) {
  const bets = await getBetsByFixtureId(fixtureId);
  if (!bets) throw new Error('Odds not found');
  return fixtureDao.updateBetsByFixtureId(fixtureId, bets);
}

async function getBetsByFixtureId(fixtureId) {
  const response = await footballApiService.getOddsByFixtureId(fixtureId);
  if (!response.api.odds[0]) return null;

  const bet365 = response.api.odds[0].bookmakers.find(bookmaker => bookmaker['bookmaker_id'] === 8);
  if (!bet365) return null;

  const betBTS = bet365.bets.find(bet => bet['label_id'] === 8);
  const betOverUnder = bet365.bets.find(bet => bet['label_id'] === 5);
  if (betBTS === null && betOverUnder === null) return null;

  const yesBts = betBTS && betBTS.values.find(value => value.value === 'Yes');
  const noBts = betBTS && betBTS.values.find(value => value.value === 'No');
  const over15 = betOverUnder && betOverUnder.values.find(value => value.value === 'Over 1.5');
  const under15 = betOverUnder && betOverUnder.values.find(value => value.value === 'Under 1.5');
  const over25 = betOverUnder && betOverUnder.values.find(value => value.value === 'Over 2.5');
  const under25 = betOverUnder && betOverUnder.values.find(value => value.value === 'Under 2.5');
  const over35 = betOverUnder && betOverUnder.values.find(value => value.value === 'Over 3.5');
  const under35 = betOverUnder && betOverUnder.values.find(value => value.value === 'Under 3.5');

  return {
    yesBts: yesBts && +yesBts.odd,
    noBts: noBts && +noBts.odd,
    over15: over15 && +over15.odd,
    under15: under15 && +under15.odd,
    over25: over25 && +over25.odd,
    under25: under25 && +under25.odd,
    over35: over35 && +over35.odd,
    under35: under35 && +under35.odd,
  };
}

async function getH2HGames(fixture) {
  const matchDate = moment(fixture.eventDateTime);
  const oldestDate = moment(matchDate).subtract(5, 'years');
  const response = await footballApiService.getH2HByTeamIds(fixture.homeTeamId, fixture.awayTeamId);
  const fixtures = response.api.fixtures;
  const games = [];

  for (let i = fixtures.length - 1; i >= 0; i--) {
    const fixtureDate = moment(fixtures[i]['event_date']);
    if (fixtureDate.isBetween(oldestDate, matchDate)) {
      games.push({
        homeTeamGoals: fixtures[i].goalsHomeTeam,
        awayTeamGoals: fixtures[i].goalsAwayTeam
      });
      if (games.length >= 5) break;
    }
  }

  if (games.length < 3 ) return null;
  return games;
}

async function getLastLeagueGames(fixture, numberOfLastGames, isHome) {
  const response = await footballApiService.getFixturesByTeamId(isHome ? fixture.homeTeamId : fixture.awayTeamId);
  const fixtures = response.api.fixtures;
  const games = [];

  for (let i = fixtures.length - 1; i >= 0; i--) {
    if (fixture.isGameAnalyzable(fixtures[i])) {
      games.push({
        homeTeamGoals: fixtures[i].goalsHomeTeam,
        awayTeamGoals: fixtures[i].goalsAwayTeam
      });
      if (games.length === numberOfLastGames) break;
    }
  }

  if (games.length !== numberOfLastGames) return null;
  return games;
}
