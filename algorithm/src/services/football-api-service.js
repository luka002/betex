const request = require('request');
const {chunkPromise, PromiseFlavor} = require('chunk-promise');
const moment = require('moment');

const baseUrl = 'http://v2.api-football.com';
const headers = {'X-RapidAPI-Key': 'e42adc496580a521b28c0fd8e1c56b44'};

exports.getFixturesByDate = (date) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}/fixtures/date/${date}?timezone=Europe/London`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

exports.getFixturesBetweenDates = async (dateStart, dateEnd) => {
  const start = moment(dateStart);
  const end = moment(dateEnd);
  const promises = [];

  for (const selectedDate = moment(start); selectedDate.diff(end, 'days') <= 0; selectedDate.add(1, 'days')) {
    const date = selectedDate.format('YYYY-MM-DD');
    promises.push(() => this.getFixturesByDate(date));
  }

  const result = await chunkPromise(promises, {
    concurrent: 15,
    promiseFlavor: PromiseFlavor.PromiseAllSettled,
    sleepMs: 100,
  });

  return result
    .filter(analysis => analysis.status === 'fulfilled')
    .map(analysis => analysis.value)
    .reduce((prevValue, currValue) => {
      return {
        api: {
          fixtures: [...prevValue.api.fixtures, ...currValue.api.fixtures]
        }
      };
    }, {api: {fixtures: []}});
};

exports.getOddsByFixtureId = (fixtureId) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}/odds/fixture/${fixtureId}`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};


exports.getH2HByTeamIds = (teamId1, teamId2) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}/fixtures/h2h/${teamId1}/${teamId2}`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

exports.getFixturesByTeamId = (teamId) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}/fixtures/team/${teamId}`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

exports.getFixturesByTeamId = (teamId) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}/fixtures/team/${teamId}`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

exports.leaguesAvailable = () => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}/leagues`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

exports.getTeamsFromLeagueId = (leagueId) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}/teams/league/${leagueId}`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

exports.getFixtureById = (fixtureId) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}/fixtures/id/${fixtureId}`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

exports.getFixturesByIds = async (fixtureIds) => {
  const promises = [];
  for (let i = 0; i < fixtureIds.length; i++) {
    promises.push(() => this.getFixtureById(fixtureIds[i]));
  }

  const result = await chunkPromise(promises, {
    concurrent: 15,
    promiseFlavor: PromiseFlavor.PromiseAllSettled,
    sleepMs: 100,
  });

  if (!result) {
    return {api: {fixtures: []}};
  }

  return result
    .filter(analysis => analysis.status === 'fulfilled')
    .map(analysis => analysis.value)
    .reduce((prevValue, currValue) => {
      return {
        api: {
          fixtures: [...prevValue.api.fixtures, ...currValue.api.fixtures]
        }
      };
    }, {api: {fixtures: []}});
};

exports.getTeamById = (teamId) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `${baseUrl}//teams/team/${teamId}`,
      headers: headers,
      timeout: 2500
    };

    request(options, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

