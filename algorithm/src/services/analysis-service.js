const fixtureService = require('../services/fixture-service');
const util = require('../util/util');

exports.analyzeFixtures = async (filter) => {
  const analyzedGames = await analyze(filter);
  return reduce({fixtures: analyzedGames, ...filter});
};

exports.predictFixtures = async (filter) => {
  const analyzedGames = await analyze(filter);
  return reduce({fixtures: analyzedGames, ...filter});
};

async function analyze({analysisType, startDate, endDate, lastH2H, lastHome, lastAway}) {
  const fixtures = await fixtureService.findByDate(startDate, endDate);
  const analyzedGames = [];

  fixtures.forEach(fixture => {
    if (!fixture.bets[analysisType]) return;
    const h2h = analyzePercentage(fixture.h2hGames, analysisType, lastH2H);
    const home = analyzePercentage(fixture.homeTeamGames, analysisType, lastHome);
    const away = analyzePercentage(fixture.awayTeamGames, analysisType, lastAway);

    analyzedGames.push({
      betType: analysisType,
      odds: {
        bookie: (1 / fixture.bets[analysisType]),
        analysis: (h2h + home + away) / 3
      },
      ...fixture
    });
  });

  return analyzedGames;
}

function analyzePercentage(fixtures, type, lastGames) {
  let success = 0;
  let gamesPlayed = 0;

  if (lastGames > fixtures.length) {
    lastGames = fixtures.length;
  }

  for (let i = 0; i < lastGames; i++) {
    if (util.fixtureSatisfiesBetTypeCondition(fixtures[i], type)) success++;
    gamesPlayed++;
  }

  if (success === 0) success++;
  return success / gamesPlayed;
}

function reduce({fixtures, minOdds, maxOdds, betType, isAnalysis, spread, forebet}) {
  let results = {
    under85: [],
    from85to90: [],
    from90to95: [],
    from95to0: [],
    from0to5: [],
    from5to10: [],
    from10to15: [],
    over15: []
  };
  if (spread !== null) {
    results = [];
  }

  fixtures.forEach(fixture => {
    if (isAnalysis && fixture.status !== 'FT') return;

    const bookie = fixture.odds.bookie;
    const analysis = fixture.odds.analysis;

    if (isAnalysis &&
        (!fixture.bets[betType.betType] ||
          fixture.bets[betType.betType] > betType.maxBetOdds ||
          fixture.bets[betType.betType] < betType.minBetOdds)) return;

    const result = {
      odds: fixture.bets[betType.betType],
      success: util.fixtureSatisfiesBetTypeCondition(fixture, betType.betType),
      game: {
        odds: fixture.bets[betType.betType],
        ...util.fixtureToFixtureTableData(fixture),
      }
    };

    const forebetTotal = util.getForebetTotalGoals(fixture.forebetResult);
    if (forebet && (!forebetTotal || (forebetTotal < forebet.min || forebetTotal > forebet.max))) return;

    if ((1 / bookie) >= minOdds && (1 / bookie) <= maxOdds) {
      if (spread !== null) {
        if (bookie + spread.bottom <= analysis && bookie + spread.top >= analysis) {
          results.push(result);
        }
      } else {
        if (bookie - 1 <= analysis && bookie - 0.15 >= analysis) results.under85.push(result);
        if (bookie - 0.15 <= analysis && bookie - 0.10 >= analysis) results.from85to90.push(result);
        if (bookie - 0.10 <= analysis && bookie - 0.05 >= analysis) results.from90to95.push(result);
        if (bookie - 0.05 <= analysis && bookie >= analysis) results.from95to0.push(result);
        if (bookie <= analysis && bookie + 0.05 >= analysis) results.from0to5.push(result);
        if (bookie + 0.05 <= analysis && bookie + 0.10 >= analysis) results.from5to10.push(result);
        if (bookie + 0.10 <= analysis && bookie + 0.15 >= analysis) results.from10to15.push(result);
        if (bookie + 0.15 <= analysis && bookie + 1 >= analysis) results.over15.push(result);
      }
    }
  });

  const stats = {};
  if (results.length) {
    stats[spread.bottom + 'to' + spread.top] = generateStatsFromResult(results, isAnalysis)
  } else {
    for (let key in results) {
      stats[key] = generateStatsFromResult(results[key], isAnalysis);
    }
  }

  if (isAnalysis) {
    const analysisArray = [];
    for (let key in stats) {
      analysisArray.push({spread: key, ...stats[key]});
    }
    return analysisArray;
  } else {
    const predictionArray = [];
    for (let key in stats) {
      for (let game of stats[key].games) {
        predictionArray.push({spread: key, ...game});
      }
    }
    return predictionArray;
  }
}

function generateStatsFromResult(result, isAnalysis = true, averageOddsOnlyWinners = false) {
  const allGames = [];

  const stats = result.reduce((prevValue, currValue) => {
    if (!isAnalysis) allGames.push(currValue.game);
    return {
      profit: currValue.success ? prevValue.profit + currValue.odds * 100 : prevValue.profit,
      totalGames: prevValue.totalGames + 1,
      gamesWon: currValue.success ? prevValue.gamesWon + 1 : prevValue.gamesWon,
      gamesLost: !currValue.success ? prevValue.gamesLost + 1 : prevValue.gamesLost,
    };
  }, {profit: 0, totalGames: 0, gamesWon: 0, gamesLost: 0});

  stats.profit -= stats.totalGames * 100;
  stats.winRate = ((stats.gamesWon / stats.totalGames) * 100).toFixed(2) + '%';
  stats.roi = ((100 / stats.totalGames) * stats.profit / 100).toFixed(2) + '%';
  if (averageOddsOnlyWinners) {
    stats.gamesWonAverageOdds = result.reduce((p, c) => c.success ? p + c.odds : p, 0) / stats.gamesWon;
  } else {
    stats.averageOdds = result.reduce((p, c) => p + c.odds, 0) / result.length;
  }
  if (!isAnalysis) stats.games = allGames;

  return stats;
}
