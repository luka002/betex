const moment = require('moment');

exports.dateRangeToArray = (dateStart, dateEnd) => {
  if (!dateEnd) dateEnd = dateStart;
  const datesArray = [];

  for (const selectedDate = moment(dateStart); selectedDate.diff(dateEnd, 'days') <= 0; selectedDate.add(1, 'days')) {
    const date = selectedDate.format('YYYY-MM-DD');
    datesArray.push(date);
  }
  return datesArray;
};

exports.mapFixtureStatus = (status) => {
  const statusMap = {
    'TBD': 'Time To Be Defined',
    'NS': 'Not Started',
    '1H': 'First Half, Kick Off',
    'HT': 'Halftime',
    '2H': 'Second Half, 2nd Half Started',
    'ET': 'Extra Time',
    'P' : 'Penalty In Progress',
    'FT': 'Match Finished',
    'AET': 'Match Finished After Extra Time',
    'PEN': 'Match Finished After Penalty',
    'BT': 'Break Time (in Extra Time)',
    'SUSP': 'Match Suspended',
    'INT': 'Match Interrupted',
    'PST': 'Match Postponed',
    'CANC': 'Match Cancelled',
    'ABD': 'Match Abandoned',
    'AWD': 'Technical Loss',
    'WO': 'WalkOver'
  };
  return statusMap[status];
};

exports.getForebetTotalGoals = (forebetResult) => {
  if (!forebetResult) return null;
  const result = forebetResult.split(' - ');
  return +result[0] + +result[1];
};


exports.fixtureToFixtureTableData = (fixture) => {
  const gameFinished = fixture.goals !== undefined && fixture.goals !== null;
  return {
    fixtureId: fixture.fixtureId,
    homeTeamName: fixture.homeTeamName,
    awayTeamName: fixture.awayTeamName,
    score: gameFinished ? (fixture.homeTeamGoals + ' - ' + fixture.awayTeamGoals) : '-',
    eventDateTime: fixture.eventDateTime,
    leagueName: fixture.leagueName,
    leagueCountry: fixture.leagueCountry,
    analyzedBeforeStart: fixture.analyzedBeforeStart,
    status: this.mapFixtureStatus(fixture.status),
    forebetResult: fixture.forebetResult,
    forebetTotalGoals: this.getForebetTotalGoals(fixture.forebetResult),
    bets: fixture.bets
  }
};

exports.fixtureSatisfiesBetTypeCondition = (fixture, type) => {
  const totalGoals = fixture.homeTeamGoals + fixture.awayTeamGoals;
  const isBts = fixture.homeTeamGoals > 0 && fixture.awayTeamGoals > 0;

  switch (type) {
    case 'over15': return totalGoals > 1.5;
    case 'under15': return totalGoals < 1.5;
    case 'over25': return totalGoals > 2.5;
    case 'under25': return totalGoals < 2.5;
    case 'over35': return totalGoals > 3.5;
    case 'under35': return totalGoals < 3.5;
    case 'yesBts': return isBts;
    case 'noBts': return !isBts;
    default: return false;
  }
};
