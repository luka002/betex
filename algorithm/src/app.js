const express = require('express');
const db = require('./DAO/db-connection');
const fixturesController = require('./controllers/fixtures-controller');

const app = express();
app.use(express.json());
app.use('/fixtures', fixturesController);

db.mongoConnect(() => {
  const server = app.listen(3000);
  server.setTimeout(900000);
});
