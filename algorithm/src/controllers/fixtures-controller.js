const express = require('express');
const fixtureService = require('../services/fixture-service');
const util = require('../util/util');

const router = express.Router();

router.get('/:startDate/:endDate', async (req, res) => {
  const fixtures = await fixtureService.findByDate(req.params.startDate, req.params.endDate);
  const mappedFixtures = fixtures.map(fixture => util.fixtureToFixtureTableData(fixture));
  return res.status(200).json(mappedFixtures);
});

router.post('/:startDate/:endDate', async (req, res) => {
  const result = await fixtureService.saveFixturesBetweenDates(req.params.startDate, req.params.endDate);
  return res.status(200).json(result);
});

router.put('/:startDate/:endDate', async (req, res) => {
  const result = await fixtureService.updateResultIfFinishedByDate(req.params.startDate, req.params.endDate);
  return res.status(200).json(result);
});

router.put('/forebet/:startDate/:endDate', async (req, res) => {
  const result = await fixtureService.updateForebetResultByDate(req.params.startDate, req.params.endDate);
  return res.status(200).json(result);
});

router.put('/odds/:startDate/:endDate', async (req, res) => {
  const result = await fixtureService.updateOddsByDate(req.params.startDate, req.params.endDate);
  return res.status(200).json(result);
});

router.post('/analyze', async (req, res) => {
  const result = await fixtureService.analyzeFixtures(req.body);
  return res.status(200).json(result);
});

router.post('/predict', async (req, res) => {
  const result = await fixtureService.predictFixtures(req.body);
  return res.status(200).json(result);
});

module.exports = router;
